-- 13/03/2019
ALTER TABLE `retiroefectivos` ADD idregistrocaja INT DEFAULT 0;
ALTER TABLE `recibositems` ADD idregistrocaja INT DEFAULT 0;
ALTER TABLE `recibositems` ADD idregistrocaja INT DEFAULT 0;


CREATE PROCEDURE `reg_arqueo` (IN `p_fecarqueo` DATETIME, IN `p_efectivoarqueo` DECIMAL(10,2), IN `p_fecultmodif` DATETIME, OUT `p_oresult` INT)  NO SQL
BEGIN 
	SET p_oresult = -1;
	
    SELECT id INTO p_oresult 
    FROM registroscaja
    WHERE fecarqueo = CONVERT(0, DATETIME)
    ORDER BY fecinicio
    LIMIT 1;
    
	UPDATE registroscaja SET
    	fecarqueo = p_fecarqueo, 
        efectivoarqueo = p_efectivoarqueo,
        fecultmodif = p_fecultmodif
    WHERE id = p_oresult;
    
    UPDATE gastositems SET
    	idregistrocaja = p_oresult
    WHERE idregistrocaja = 0;
    
    UPDATE recibositems SET
    	idregistrocaja = p_oresult
    WHERE idregistrocaja = 0;
    
    UPDATE retiroefectivos SET
    	idregistrocaja = p_oresult
    WHERE idregistrocaja = 0;
END$$

CREATE PROCEDURE `reg_inicio_dia` (IN `p_fecinicio` DATETIME, IN `p_efectivoinicio` DECIMAL(10,2), IN `p_fecultmodif` DATETIME, OUT `p_oresult` INT)  NO SQL
BEGIN 

	SET p_oresult = -1;
	INSERT INTO registroscaja 
    	(fecinicio, efectivoinicio, fecultmodif)
	VALUES
		(p_fecinicio, p_efectivoinicio, p_fecultmodif);
	SET p_oresult = LAST_INSERT_ID();
END$$