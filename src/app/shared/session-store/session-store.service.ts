import { Injectable } from '@angular/core';

@Injectable()
export class SessionStore {
    readonly PREFIX = 'tware';
    readonly SEPARATOR = '|';
    module: string;

    constructor() {
        this.module = '';
    }

    init(module: string) {
        this.module = module;
    }

    getKey(key: string) {
        const lkey = localStorage.getItem(this.PREFIX + this.SEPARATOR + this.module + this.SEPARATOR + key);
        return (lkey !== null) ? lkey : '';
    }

    setKey(key: string, value: string) {
        const lkey = this.PREFIX + this.SEPARATOR + this.module + this.SEPARATOR + key;
        localStorage.setItem(lkey, value);
    }

    getNumberKey(key: string): number {
        const lkey = localStorage.getItem(this.PREFIX + this.SEPARATOR + this.module + this.SEPARATOR + key);
        return (lkey !== null) ? +lkey : 0;
    }

    setNumberKey(key: string, value: number) {
        const lkey = this.PREFIX + this.SEPARATOR + this.module + this.SEPARATOR + key;
        localStorage.setItem(lkey, value.toString());
    }

    clear() {
        localStorage.clear();
    }
}
