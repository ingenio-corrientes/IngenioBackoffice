import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public loggedUser;
    public loggedId;

    constructor(private translate: TranslateService, public router: Router) {
        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedUser = jsonData.usuario;
        this.loggedId = jsonData.id;
        // console.log(this.loggedUser);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('push-right');
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('userLog');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    gotoPerfil() {
        this.router.navigate(['/perfil', this.loggedId]);
    }
}
