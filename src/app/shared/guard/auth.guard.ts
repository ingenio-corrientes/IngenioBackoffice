import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthGuard implements CanActivate {
    readonly MINUTES_UNITL_AUTO_LOGOUT = 60; // in mins
    readonly CHECK_INTERVAL = (environment.production) ? 1000 : 10000; // in ms
    readonly STORE_KEY = 'lastAction';

    constructor(private router: Router) {
        this.check();
        this.initListener();
        this.initInterval();
     }

    canActivate() {
        if (localStorage.getItem('isLoggedin')) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }

    get lastAction() {
        return parseInt(localStorage.getItem(this.STORE_KEY), 10);
    }

    set lastAction(value) {
        localStorage.setItem(this.STORE_KEY, value + '');
    }

    initListener() {
        document.body.addEventListener('click', () => this.reset());
    }

    reset() {
        this.lastAction = Date.now();
    }

    initInterval() {
        setInterval(() => {
            this.check();
      }, this.CHECK_INTERVAL);
    }

    check() {
      const now = Date.now();
      const timeleft = this.lastAction + this.MINUTES_UNITL_AUTO_LOGOUT * 60 * this.CHECK_INTERVAL;
      const diff = timeleft - now;
      const isTimeout = diff < 0;

      if (isTimeout && localStorage.getItem('isLoggedin')) {
        this.router.navigate(['/login']);
      }
    }
}
