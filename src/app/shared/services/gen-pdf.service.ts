import { Injectable } from '@angular/core';
import { jsPDF, jsPDFOptions } from 'jspdf';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { ImagesBase64 } from './images-base64.service'
import { Presupuesto, PresupuestoItem } from '.';

@Injectable()
export class GenPdfService {
    private lineHeight: number;
    private line: number;
    private leftMargin: number;
    private rightMargin: number;
    private imgB64: ImagesBase64
    private logo;
    private _imgSafe: SafeUrl;

    genFormHeader() {
        const options: jsPDFOptions = {
            orientation: 'p',
            unit: 'cm',
            format: 'a4'
        };
        const doc = new jsPDF(options);

        this.lineHeight = 3;
        this.line = 8;
        this.leftMargin = 8;
        this.rightMargin = 200;

        doc.addImage(ImagesBase64.icono, 'PNG', 8, 5, 18, 14);
        doc.setFont('Cantarell', 'normal').setFontSize(7).setTextColor(150);
        doc.text('AV. MAIPU 3256 - (3400) CORRIENTES', this.leftMargin, this.line * this.lineHeight);
        this.line++;
        doc.text('TEL. FAX: (0379) 4476396', this.leftMargin, this.line * this.lineHeight);
        doc.text('e-mail: info@azife.com', this.rightMargin, this.line * this.lineHeight, {'align': 'right'});
        doc.line(this.leftMargin, this.line * this.lineHeight + 1, this.rightMargin, this.line * this.lineHeight + 1);
        doc.line(this.leftMargin, this.line * this.lineHeight + 2, this.rightMargin, this.line * this.lineHeight + 2);
        return doc;
    }

    public genPresupuesto(presupuesto: Presupuesto, dataItems: PresupuestoItem[]) {
        let text = '';
        const doc = this.genFormHeader();
        this.line++;
        doc.setFont('Cantarell', 'bolditalic').setFontSize(9).setTextColor(0, 0, 0);

        this.line++;
        doc.text('Presupuesto Nº ', this.leftMargin, this.line * this.lineHeight);
        text = presupuesto.id + '';
        doc.text(text, this.rightMargin, this.line * this.lineHeight, {'align': 'right'});

        const fileName = 'Presupuesto-' + presupuesto.id + '.pdf';
        doc.save(fileName);
    }

    stringDate(strindDate: any) {
        const datestring = strindDate.split('-');
        const newDate = new Date(+datestring[0], (+datestring[1]) - 1, +datestring[2], 0, 0, 0, 0);
        const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
        'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        if (newDate.toString() !== 'Invalid Date' && strindDate !== '0000-00-00') {
            return 'Corrientes ' + newDate.getDate() + ' de ' + months[newDate.getMonth()] + ' de ' + newDate.getFullYear();
        } else {
            return ''
        };
    }

    // testHoja() {
    //     const doc = new jsPDF().setProperties({
    //         title: 'Presupuesto', orientation: 'p',
    //         unit: 'cm',
    //         format: 'a4'
    //     });
    //     this.line = 5;
    //     this.leftMargin = 8;
    //     this.lineHeight = 3;
    //     let text = '1';

    //     doc.setFont('Cantarell').setFontType('normal').setFontSize(8).setTextColor(150);
    //     do {
    //         this.line++;
    //         text = (this.line - 5) + '';
    //         doc.text(this.leftMargin, this.line * this.lineHeight, text);
    //     } while (this.line < 90);
    // }
}
