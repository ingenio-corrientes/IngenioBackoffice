
export class TWUtils {
    static readonly MESSAGE_STAYHERE = 0;
    static readonly MESSAGE_GOTO = 1;
    static readonly MESSAGE_RELOAD = 2;

    static arrayDateToString(arrDate: any): any {
        if (arrDate['year'] !== undefined) {
            const selDate = arrDate['year'] + '-' + arrDate['month'] + '-' + arrDate['day'];
            return selDate;
        } else { return ''; }
    }

    static arrayDateToNumber(arrDate: any): any {
        if (arrDate['year'] !== undefined) {
            const selDate = arrDate['year'] + '' + arrDate['month'] + '' + arrDate['day'];
            return +selDate;
        } else { return 0; }
    }

    static arrayDateToDateTimeString(arrDate: any): any {
        if (arrDate['year'] !== undefined) {
            const dateTime = new Date();
            const selDate = arrDate['year'] + '-' + arrDate['month'] + '-' + arrDate['day'];
            const selTime = dateTime.getHours() + ':' + dateTime.getMinutes() + ':' + dateTime.getSeconds();
            return selDate + ' ' + selTime;
        } else { return ''; }
    }

    static getTodayJson(): any {
        const date = new Date();
        const selDate = {
            'year': date.getFullYear(),
            'month': (date.getMonth() + 1),
            'day': date.getDate()
        };
        return selDate;
    }

    static stringDateToJson(strindDate: any): any {
        if (strindDate === '') {
            return {};
        }
        const splitDateTime = strindDate.replace('T', ' ').split(' ');
        const datestring = splitDateTime[0].split('-');
        const newDate = new Date(+datestring[0], (+datestring[1]) - 1, +datestring[2], 0, 0, 0, 0);
        return {
             'year': newDate.getFullYear(),
             'month': (newDate.getMonth() + 1),
             'day': newDate.getDate() };
    }

    static padLeft(text: string, padChar: string, size: number): string {
        return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
    }

    static isNullDate(arrDate: any) {
        if (arrDate === undefined || arrDate === '' || arrDate === '0000-00-00' || arrDate === '0000-00-00 00:00:00') {
            return true;
        }
        return false;
    }

    static getEmptyIfNullOrUndefined(data: any) {
        if (data === null) {
            return '';
        }
        if (data + '' === 'undefined') {
            return '';
        }
        return data;
    }
}
