import { jsPDF, jsPDFOptions } from 'jspdf';
import 'jspdf-autotable';
import { ImagesBase64 } from '../images-base64.service'

import { CurrencyAR } from '../../pipes/currency-ar';

import { Presupuesto, PresupuestoItem, PresupuestoItemAds,
        AppSettings,
        TWUtils} from '../../../shared'

export class GenPresupuesto {
    private lineHeight: number;
    private line: number;
    private leftMargin: number;
    private rightMargin: number;
    private itemsCarteleria: PresupuestoItem[];
    private itemsDiseno: PresupuestoItem[];
    private itemsImpresionLaser: PresupuestoItem[];
    private itemsPapeleria: PresupuestoItem[];
    private itemsCarteleriaAds: PresupuestoItemAds[];
    private itemsPapeleriaAds: PresupuestoItemAds[];
    private itemsImpresionLaserAds: PresupuestoItemAds[];

    private mDoc;
    constructor(private cp: CurrencyAR) {
    }

    setTextCentered(text: string, start: number, end: number) {
        const init = (end - start) / 2 - text.length;
        this.mDoc.text(text, init, this.line * this.lineHeight);
        return init;
    }

    public genPresupuesto(
        comprobante: Presupuesto,
        items: PresupuestoItem[],
        itemsads: PresupuestoItemAds[]
        ) {

        this.itemsCarteleria = items.filter(item => +item.idtiposervicio === AppSettings.TiposServicios.caterleria);
        this.itemsDiseno = items.filter(item => +item.idtiposervicio === AppSettings.TiposServicios.diseno);
        this.itemsImpresionLaser = items.filter(item => +item.idtiposervicio === AppSettings.TiposServicios.impresionlaser);
        this.itemsPapeleria = items.filter(item => +item.idtiposervicio === AppSettings.TiposServicios.papeleria);
        this.itemsCarteleriaAds = itemsads.filter(item => +item.idtiposervicio === AppSettings.TiposServicios.caterleria);
        this.itemsPapeleriaAds = itemsads.filter(item => +item.idtiposervicio === AppSettings.TiposServicios.papeleria);
        this.itemsImpresionLaserAds = itemsads.filter(item => +item.idtiposervicio === AppSettings.TiposServicios.impresionlaser);

        const options: jsPDFOptions = {
            orientation: 'portrait',
            unit: 'mm',
            format: 'a4'
        };
        this.mDoc = new jsPDF(options);

        const fecha = (!TWUtils.isNullDate(comprobante.fecemision))
            ? (comprobante.fecemision.split(' ')[0].split('-'))
            : ('0000-00-00'.split('-'));

        this.lineHeight = 3;
        this.line = 4;
        this.leftMargin = 8;
        this.rightMargin = 200;

        this.mDoc.addImage(ImagesBase64.icono, 'PNG', 20, 2, 20, 18);
        this.mDoc.setFont('helvetica', 'bold').setFontSize(12).setTextColor(0);
        const position = this.setTextCentered('PRESUPUESTO ' + comprobante.id, this.leftMargin, this.rightMargin);

        this.line++;
        this.mDoc.setDrawColor(0);
        this.mDoc.roundedRect(position + 78, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'DF');
        this.mDoc.roundedRect(position + 89, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'DF');
        this.mDoc.roundedRect(position + 100, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'DF');
        this.mDoc.setFont('helvetica', 'bold').setFontSize(9).setTextColor(150);
        this.mDoc.text(fecha[2], position + 82, this.line * this.lineHeight);
        this.mDoc.text(fecha[1], position + 93, this.line * this.lineHeight);
        this.mDoc.text(fecha[0], position + 102, this.line * this.lineHeight);

        this.mDoc.setFont('helvetica', 'bolditalic').setFontSize(7).setTextColor(0);
        this.mDoc.text('NO VÁLIDO COMO FACTURA', position, this.line * this.lineHeight, {'flags': {'autoencoding': true}});
        this.mDoc.setLineWidth(0.5);
        this.mDoc.line(position, this.line * this.lineHeight + 3, this.rightMargin, this.line * this.lineHeight + 3);

        this.line++;
        this.line++;
        let text = 'Salta 1111 - Teléfono 0379 15-471-1468';
        this.mDoc.setFont('helvetica', 'italic').setFontSize(7).setTextColor(0);
        this.mDoc.text(text, this.leftMargin, this.line * this.lineHeight + 2, {'autoencoding': true});

        this.mDoc.setFont('helvetica', 'italic').setFontSize(7).setTextColor(0);
        this.mDoc.text('CUIT 30-71190198-8', position, this.line * this.lineHeight);
        this.mDoc.text('INICIO DE ACTIVIDAD 17/05/2011', this.rightMargin, this.line * this.lineHeight, {align: 'right'});

        this.line++;
        this.line++;
        text = 'Razón Social: ' + comprobante.razonsocial;
        this.mDoc.text(text, position, this.line * this.lineHeight);
        this.mDoc.setLineWidth(0.001);
        this.mDoc.line(position + 15, this.line * this.lineHeight + 0.5, this.rightMargin, this.line * this.lineHeight + 0.5);
        this.line++;

        this.line++;
        this.line++;
        let pos = this.line * this.lineHeight + 2;
        if (this.itemsCarteleria.length > 0) {
            this.mDoc.setFont('helvetica', 'bold').setFontSize(11).setTextColor(0);
            this.mDoc.text('Cartelería', this.leftMargin, pos);
            pos += 0.381459804 * this.lineHeight;
            this.genAutoTableCarteleria(pos);
            pos = this.mDoc.lastAutoTable.finalY + this.lineHeight + 2; // pos en mm
        }

        if (this.itemsDiseno.length > 0) {
            this.mDoc.setFont('helvetica', 'bold').setFontSize(11).setTextColor(0);
            this.mDoc.text('Diseño', this.leftMargin, pos);
            pos += 0.381459804 * this.lineHeight;
            this.genAutoTableDiseno(pos);
            pos = this.mDoc.lastAutoTable.finalY + this.lineHeight + 2; // pos en mm
        }

        if (this.itemsImpresionLaser.length > 0) {
            this.mDoc.setFont('helvetica', 'bold').setFontSize(11).setTextColor(0);
            this.mDoc.text('Impresión Laser', this.leftMargin, pos);
            pos += 0.381459804 * this.lineHeight;
            this.genAutoTableImpresionLaser(pos);
            pos = this.mDoc.lastAutoTable.finalY + this.lineHeight + 2; // pos en mm
        }

        if (this.itemsPapeleria.length > 0) {
            this.mDoc.setFont('helvetica', 'bold').setFontSize(11).setTextColor(0);
            this.mDoc.text('Papelería', this.leftMargin, pos);
            pos += 0.381459804 * this.lineHeight;
            this.genAutoTablePapeleria(pos);
            pos = this.mDoc.lastAutoTable.finalY + this.lineHeight + 2; // pos en mm
        }

        this.line = pos;
        this.line++;
        this.line++;

        let total = 0;
        items.forEach(item => {
            total += +item.subtotal;
        })
        itemsads.forEach(item => {
            total += +item.subtotal;
        })
        text = 'Total: ' + this.cp.transform(+parseFloat(total.toString()).toFixed(2));
        this.mDoc.text(text, position + 60, pos + 5);

        this.line++;
        this.line++;

        text = 'Observaciones: ';
        this.mDoc.setFont('helvetica', 'bold').setFontSize(12).setTextColor(0);
        this.mDoc.text(text, position + -65, pos + 15);

        text = comprobante.observacion;
        this.mDoc.setFont('helvetica', 'normal').setFontSize(12).setTextColor(0);
        this.mDoc.text(text, position + -65, pos + 19);
        this.mDoc.setLineWidth(0.5);
        this.line++;

        this.mDoc.save('Presupuesto-' + comprobante.id + '.pdf');
    }

    protected genAutoTableCarteleria(startPos): number {
        const columns = [['Descripción', 'Cantidad', 'Precio', 'Total']];
        const rows = [];

        this.itemsCarteleria.forEach(item => {
            rows.push([
                item.descripcionpdf,
                item.cantidad,
                this.cp.transform(+item.precio),
                this.cp.transform(+item.subtotal)
            ]);

            const itemsItAdCarteleriaFilt = this.itemsCarteleriaAds.filter(adicional =>
                adicional.idservicio === item.id
            );

            itemsItAdCarteleriaFilt.forEach(itemAd => {
                rows.push([
                    itemAd.descripcion,
                    itemAd.cantidad,
                    this.cp.transform(+itemAd.precio),
                    this.cp.transform(+itemAd.subtotal)
                ]);
            })
        });
        return this.mDoc.autoTable({
            startY : startPos,
            theme: 'grid',
            margin: {top: 40},
            headStyles: {
               fillColor: [236, 104, 28]
            },
            columnStyles: {
                0: { halign: 'left'},
                1: { halign: 'right'},
                2: { halign: 'right'},
                3: { halign: 'right'}
            },
            head: columns,
            body: rows,
        });
    }

    protected genAutoTableDiseno(startPos) {
        const columns = [['Descripción', 'Cantidad', 'Precio', 'Total']];
        const rows = [];

        this.itemsDiseno.forEach(item => {
            rows.push([
                item.descripcionpdf,
                item.cantidad,
                this.cp.transform(+item.precio),
                this.cp.transform(+item.subtotal)
            ]);
        });

        return this.mDoc.autoTable({
            startY: startPos,
            theme: 'grid',
            margin: {top: 40},
            headStyles: {
               fillColor: [236, 104, 28]
            },
            columnStyles: {
                0: { halign: 'left'},
                1: { halign: 'right'},
                2: { halign: 'right'},
                3: { halign: 'right'}
            },
            head: columns,
            body: rows
        });
    }

    protected genAutoTableImpresionLaser(startPos) {
        const columns = [['Descripción', 'Cantidad', 'Precio', 'Total']];
        const rows = [];

        this.itemsImpresionLaser.forEach(item => {
            rows.push([
                item.descripcionpdf,
                item.cantidad,
                this.cp.transform(+item.precio),
                this.cp.transform(+item.subtotal)
            ]);

            const itemsItAdImpresionLaserFilt = this.itemsImpresionLaserAds.filter(adicional =>
                adicional.idservicio === item.id
            );

            itemsItAdImpresionLaserFilt.forEach(itemAd => {
                rows.push([
                    itemAd.descripcion,
                    itemAd.cantidad,
                    this.cp.transform(+itemAd.precio),
                    this.cp.transform(+itemAd.subtotal)
                ]);
            })
        });
        return this.mDoc.autoTable({
            startY: startPos,
            theme: 'grid',
            margin: {top: 40},
            headStyles: {
               fillColor: [236, 104, 28]
            },
            columnStyles: {
                0: { halign: 'left'},
                1: { halign: 'right'},
                2: { halign: 'right'},
                3: { halign: 'right'}
            },
            head: columns,
            body: rows
        });
    }

    protected genAutoTablePapeleria(startPos) {
        const columns = [['Descripción', 'Cantidad', 'Precio', 'Total']];
        const rows = [];

        this.itemsPapeleria.forEach(item => {
            rows.push([
                item.descripcionpdf,
                (item.unidades === undefined) ? item.cantidad : item.unidades,
                this.cp.transform(+item.precio),
                this.cp.transform(+item.subtotal)
            ]);

            const itemsItAdPapeleriaFilt = this.itemsPapeleriaAds.filter(adicional =>
                adicional.idservicio === item.id
            );

            itemsItAdPapeleriaFilt.forEach(itemAd => {
                rows.push([
                    itemAd.descripcion,
                    itemAd.cantidad,
                    this.cp.transform(+itemAd.precio),
                    this.cp.transform(+itemAd.subtotal)
                ]);
            })
        });
        return this.mDoc.autoTable({
            startY: startPos,
            theme: 'grid',
            margin: {top: 40},
            headStyles: {
               fillColor: [236, 104, 28]
            },
            columnStyles: {
                0: { halign: 'left'},
                1: { halign: 'right'},
                2: { halign: 'right'},
                3: { halign: 'right'}
            },
            head: columns,
            body: rows
        });
    }
}
