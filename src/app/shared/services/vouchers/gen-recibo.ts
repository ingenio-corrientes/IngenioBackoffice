import { jsPDF, jsPDFOptions } from 'jspdf';
import 'jspdf-autotable';
import { ImagesBase64 } from '../images-base64.service'

import { CurrencyAR } from '../../pipes/currency-ar';
import { Recibo, ReciboItem, AppSettings, TWUtils } from '../../../shared';

export class GenRecibo {
    private lineHeight: number;
    private line: number;
    private leftMargin: number;
    private rightMargin: number;
    private itemsRecibo: ReciboItem[];

    private mDoc;
    constructor(private cp: CurrencyAR) {
    }

    setTextCentered(text: string, start: number, end: number) {
        const init = (end - start) / 2 - text.length;
        this.mDoc.text(init, this.line * this.lineHeight, text);
        return init;
    }

    public genRecibo(
        comprobante: Recibo,
        itemsrecibo: ReciboItem[]) {

        const options: jsPDFOptions = {
            orientation: 'portrait',
            unit: 'mm',
            format: 'a4'
        };

        this.mDoc = new jsPDF(options);

        this.itemsRecibo = itemsrecibo;

        const fecha = (!TWUtils.isNullDate(comprobante.fecemision))
            ? (comprobante.fecemision.split(' ')[0].split('-'))
            : ('0000-00-00'.split('-'));

        this.lineHeight = 3;
        this.line = 4;
        this.leftMargin = 8;
        this.rightMargin = 200;

        this.mDoc.addImage(ImagesBase64.icono, 'PNG', 20, 2, 20, 18);
        this.mDoc.setFont('helvetica', 'bold').setFontSize(12).setTextColor(0);
        const position = this.setTextCentered('RECIBO ' + comprobante.id, this.leftMargin, this.rightMargin);

        this.line++;
        this.mDoc.setDrawColor(0);
        this.mDoc.roundedRect(position + 78, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'DF');
        this.mDoc.roundedRect(position + 89, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'DF');
        this.mDoc.roundedRect(position + 100, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'DF');
        this.mDoc.setFont('helvetica', 'bold').setFontSize(9).setTextColor(150);
        this.mDoc.text(fecha[2], position + 82, this.line * this.lineHeight);
        this.mDoc.text(fecha[1], position + 93, this.line * this.lineHeight);
        this.mDoc.text(fecha[0], position + 102, this.line * this.lineHeight);

        this.mDoc.setFont('helvetica', 'bolditalic').setFontSize(7).setTextColor(0);
        this.mDoc.text('NO VÁLIDO COMO FACTURA', position + 17, this.line * this.lineHeight, {'flags': {'autoencoding': true}});
        this.mDoc.setLineWidth(0.5);
        this.mDoc.line(position, this.line * this.lineHeight + 3, this.rightMargin, this.line * this.lineHeight + 3);

        this.line++;
        this.line++;
        let text = 'Salta 1111 - Teléfono 0379 15-471-1468';
        this.mDoc.setFont('helvetica', 'italic').setFontSize(7).setTextColor(0);
        this.mDoc.text(text, this.leftMargin, this.line * this.lineHeight + 2);

        this.mDoc.setFont('helvetica', 'italic').setFontSize(7).setTextColor(0);
        this.mDoc.text('CUIT 30-71190198-8', position, this.line * this.lineHeight);
        this.mDoc.text('INICIO DE ACTIVIDAD 17/05/2011', this.rightMargin, this.line * this.lineHeight, null, null, 'right');

        this.line++;
        this.line++;
        text = 'Razón Social: ' + comprobante.razonsocial;
        this.mDoc.text(text, position, this.line * this.lineHeight);
        this.mDoc.setLineWidth(0.001);
        this.mDoc.line(position + 15, this.line * this.lineHeight + 0.5, this.rightMargin, this.line * this.lineHeight + 0.5);

        this.line++;
        this.line++;
        text = 'Presupuesto Nº: ' + comprobante.idpresupuesto;
        this.mDoc.setFont('helvetica', 'bold').setFontSize(10).setTextColor(0);
        this.mDoc.text(text, this.leftMargin, this.line * this.lineHeight + 2);
        text = 'Monto: ' + TWUtils.padLeft(this.cp.transform(comprobante.montopresupuesto), ' ', 16);
        this.mDoc.text(text, this.rightMargin, this.line * this.lineHeight + 2, null, null, 'right');

        this.line++;
        text = 'Saldo: ' + TWUtils.padLeft(this.cp.transform(comprobante.saldopresupuesto), ' ', 16);
        this.mDoc.text(text, this.rightMargin, this.line * this.lineHeight + 2, null, null, 'right');

        // this.line++;
        this.line++;
        this.line++;
        let pos = this.line * this.lineHeight + 2;
        text = 'Formas de Pago';
        this.mDoc.setFont('helvetica', 'bold').setFontSize(12).setTextColor(0);
        this.mDoc.text(text, this.leftMargin, pos);
        this.mDoc.setLineWidth(0.8);
        this.mDoc.line(position - 80, pos + 3, this.rightMargin, pos + 3);
        this.line++;
        this.line++;
        pos = this.line * this.lineHeight + 2;
        this.genAutoTable(pos);

        pos = this.mDoc.lastAutoTable.finalY + this.lineHeight + 2; // pos en mm
        text = 'Total Cobro: ' + this.cp.transform(+parseFloat(comprobante.monto.toString()).toFixed(2));
        this.mDoc.text(text, position, pos + 15);
        this.mDoc.setLineWidth(0.5);
        this.mDoc.line(position + 30, pos + 17, this.rightMargin, pos + 17);
        this.line++;

        this.mDoc.save('Recibo-' + comprobante.id + '.pdf');
    }

    protected genAutoTable(startPos): number {
        const columns = [['Forma', 'Número', 'Monto']];
        const rows = [];

        this.itemsRecibo.forEach(item => {
            let forma = 'Efectivo';
            if (item.idformacobro === AppSettings.FormaCobro.cheque) {
                forma = 'Cheque ' + item.banco;
            }
            rows.push([
                item.formacobro,
                item.numero,
                this.cp.transform(+item.monto)]);
        });

        return this.mDoc.autoTable({
            startY : startPos,
            theme: 'grid',
            margin: {top: 40},
            headStyles: {
               fillColor: [236, 104, 28]
            },
            columnStyles: {
                0: { halign: 'left'},
                1: { halign: 'center'},
                2: { halign: 'right'}
            },
            head: columns,
            body: rows,
        });
    }
}
