import { Injectable } from '@angular/core';
import { jsPDF, jsPDFOptions } from 'jspdf';
import 'jspdf-autotable';
import { ImagesBase64 } from '../images-base64.service'

import { CurrencyAR } from '../../pipes/currency-ar';

import { Gasto, GastoDeuda, GastoItem, TWUtils } from '../../../shared'

export class GenGasto {
    private lineHeight: number;
    private line: number;
    private leftMargin: number;
    private rightMargin: number;

    private mDoc;
    constructor(private cp: CurrencyAR) {
    }

    setTextCentered(text: string, start: number, end: number) {
        const init = (end - start) / 2 - text.length;
        this.mDoc.text(init, this.line * this.lineHeight, text);
        return init;
    }

    public genGasto(
        comprobante: Gasto,
        itemsdeuda: GastoDeuda[],
        itemsFormaPago: GastoItem[]) {

        const options: jsPDFOptions = {
            orientation: 'portrait',
            unit: 'mm',
            format: 'a4'
        };
        this.mDoc = new jsPDF(options);

        // const fecha = comprobante.fecha.split(' ')[0].split('-');
        const fecha = (!TWUtils.isNullDate(comprobante.fecha))
            ? (comprobante.fecha.split(' ')[0].split('-'))
            : ('0000-00-00'.split('-'));

        this.lineHeight = 3;
        this.line = 4;
        this.leftMargin = 8;
        this.rightMargin = 200;

        this.mDoc.addImage(ImagesBase64.icono, 'PNG', 20, 2, 20, 20);
        this.mDoc.setFont('helvetica', 'bold').setFontSize(12).setTextColor(0);
        const position = this.setTextCentered('GASTO ' + comprobante.id, this.leftMargin, this.rightMargin);

        this.line++;
        this.mDoc.setDrawColor(0);
        this.mDoc.roundedRect(position + 78, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'DF');
        this.mDoc.roundedRect(position + 89, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'DF');
        this.mDoc.roundedRect(position + 100, this.line * this.lineHeight - 4, 11, 6, 2, 2, 'DF');
        this.mDoc.setFont('helvetica', 'bold').setFontSize(9).setTextColor(150);
        this.mDoc.text(position + 82, this.line * this.lineHeight, fecha[2]);
        this.mDoc.text(position + 93, this.line * this.lineHeight, fecha[1]);
        this.mDoc.text(position + 102, this.line * this.lineHeight, fecha[0]);

        this.mDoc.setFont('helvetica', 'bolditalic').setFontSize(7).setTextColor(0);
        this.mDoc.text('NO VÁLIDO COMO FACTURA', position, this.line * this.lineHeight, {'flags': {'autoencoding': true}});
        this.mDoc.setLineWidth(0.5);
        this.mDoc.line(position, this.line * this.lineHeight + 3, this.rightMargin, this.line * this.lineHeight + 3);

        this.line++;
        this.line++;
        this.line++;
        let text = 'Salta 1111 - Teléfono 0379 15-471-1468';
        this.mDoc.setFont('helvetica', 'italic').setFontSize(7).setTextColor(0);
        this.mDoc.text(text, this.leftMargin, this.line * this.lineHeight + 2, {'autoencoding': true});

        this.mDoc.setFont('helvetica', 'italic').setFontSize(7).setTextColor(0);
        this.mDoc.text('CUIT 30-71190198-8', position, this.line * this.lineHeight);
        this.mDoc.text('INICIO DE ACTIVIDAD 17/05/2011', this.rightMargin, this.line * this.lineHeight, {align: 'right'});

        this.line++;
        this.line++;

        this.line++;
        this.line++;
        text = 'Deudas';
        this.mDoc.setFont('helvetica', 'bold').setFontSize(14).setTextColor(0);
        this.mDoc.text(text, this.leftMargin, this.line * this.lineHeight + 2);
        this.mDoc.setLineWidth(0.5);
        this.mDoc.line(position - 80, this.line * this.lineHeight + 3, this.rightMargin, this.line * this.lineHeight + 3);
        this.genAutoTableDeudas(this.line * this.lineHeight + 4, itemsdeuda);

        let pos = this.mDoc.lastAutoTable.finalY + this.lineHeight;
        this.line += pos;
        text = 'Forma de Pago';
        this.mDoc.setFont('helvetica', 'bold').setFontSize(14).setTextColor(0);
        this.mDoc.text(text, this.leftMargin, this.line);
        this.mDoc.setLineWidth(0.5);
        this.mDoc.line(position - 80, this.line + 1, this.rightMargin, this.line + 1);
        this.genAutoTableFormasCobro(this.line + 3, itemsFormaPago);

        pos = this.mDoc.lastAutoTable.finalY + this.lineHeight + 2;
        text = 'Total Deuda: ' + this.cp.transform(comprobante.totaldeuda);
        this.mDoc.text(text, position, pos + 5);
        this.mDoc.setLineWidth(0.5);
        this.mDoc.line(position + 30, pos + 7, this.rightMargin, pos + 7);
        this.line++;

        this.line++;
        this.line++;
        text = 'Total a Pagar: ' + this.cp.transform(comprobante.monto);
        this.mDoc.text(text, position, pos + 15);
        this.mDoc.setLineWidth(0.5);
        this.mDoc.line(position + 30, pos + 17, this.rightMargin, pos + 17);
        this.line++;

        this.mDoc.save('Gasto-' + comprobante.id + '.pdf');
    }

    protected genAutoTableDeudas(startPos, itemsdeuda: GastoDeuda[]): number {
        const columns = [['Fecha', 'Factura', 'Monto', 'Importe']];
        const rows = [];

        itemsdeuda.forEach(item => {
            rows.push([
                item.fecemision,
                item.tipocomprobante,
                this.cp.transform(+item.total),
                item.montoacancelar]);
        });

        return this.mDoc.autoTable({
            startY : startPos,
            theme: 'grid',
            margin: {top: 40},
            headStyles: {
               fillColor: [236, 104, 28]
            },
            columnStyles: {
                0: { halign: 'left'},
                1: { halign: 'left'},
                2: { halign: 'right'},
                3: { halign: 'right'}
            },
            head: columns,
            body: rows,
        });
    }

    protected genAutoTableFormasCobro(startPos, itemsFormaPago: GastoItem[]) {
        const columns = [['Destino', 'Forma de Pago', 'Cheque Nº', 'Importe']];
        const rows = [];

        itemsFormaPago.forEach(item => {
            rows.push([
                item.origendestino,
                item.formapago,
                item.numero,
                this.cp.transform(+item.monto)]);
        });

        return this.mDoc.autoTable({
            startY : startPos,
            theme: 'grid',
            margin: {top: 40},
            headStyles: {
               fillColor: [236, 104, 28]
            },
            columnStyles: {
                0: { halign: 'left'},
                1: { halign: 'right'},
                2: { halign: 'right'}
            },
            head: columns,
            body: rows,
        });
    }

}
