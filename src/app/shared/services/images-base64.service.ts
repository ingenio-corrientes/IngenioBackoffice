import { Injectable } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Injectable()
export class ImagesBase64 {
    public static icono =
            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADgCAMAAAAt85rTAAAA0lBMVEX4nB7////4mxz5pDT0mh3ymR34mhX6tmjulx3tlh3mkhzqlBzShhjWiBnnkxzhjxvcjBv2lwDMfADeiADOhBf4lQDNgADKegD//fn82LP5vn/99u7tz7DotHvQfwDSkkPqjQDx2sL769v96db94cXy4M3/+vLlwJj80qf+8ubjjADpyqv959Hjt4f6s2D7yJH5unberHTZn1r81q/5rVH5qUPYoF76wofRjTP8zZvtrmbXmk3juo3TjCrcqG7ru4ffmD/smy/in0/pnj/4pDrUlkrGylbsAAAPl0lEQVR4nO2daXvavBKGCTEEAlnM4pCYEKABAmQjJTRptr5d/v9fOpa8YMB6ZryF0sPz5fTtdWp8W9JoNBppMrv/tu4z2X9a+b3Mv60t4KZrC7jp2gJuuraAm64t4KZrC7jp2gJuuraAm64t4KZrC7jp2gJuuraAm64t4KbrUwGz2bwlPUDi77PZNH4zfcCsjVSv65ndx+nt3tm35+erq6+nrr5eXT3fne3dTn9r4v9ksWa1BH8+NUCby3rj37f3d1enl1/MfmMHq9E3ry+vzvZ2JWcmkwhn8oA2mJ6bnj2fXg8pqGB1L59vNQsyq2lxKZMEtNCsb/+4d/c1KtiChpffHi1ILR5kMoASrf54dnWZBJlP5um9Xo/FGBcwa3XIev7x/up6mCiZT9dnWox2jAMomi1ze3eacKsF6PK2LREjMEYEFO2mT+8uU2u2ZQ2/tfVcFMQogFnLkpydfhqbo8Zzu53LhUaMAJjdve5/MpyD+M0iDIsYpQX157XwWRretgshESONwfrdugh3TkcFiZgu4DoJzV+jYiFEI0a0ovW19dKdnfNRKUQjRgTU6lfrI7yqlopswhCAed0HqLXXSNisHpYKTEI+YH56VfcB5tZLuM8l5ANak8NzXXMdJkF4uj7CV4uwyCLkA9avdwSh4y9ZgGslfOe2YYgWFM+9axecmXbdhD+OeIRswPztjk1YcqYhi7DQvlwb4LDKI2QDuv7Zx8i2YGsnHDuESQGKISh17vT+tRP+OD46pA0NvwW9FYRHqK2XsNviEHIB89P5k5cIr9XvkK4mrWO6k3IBdb97/ZcQmlYT7lNNyAWsL/TE738H4Tmjk7IBzYVHLxF2w71Yw1G/ES9cZbYORCdNAjCbWXp2eMLGsNscDyYPfy7eyuWWLaP8dvEyuxl3htGCIO90EzIB8/fLz/7JJ+x2BjcX5Yolw1LZ1YEjQ7KW3we90IA9ugmZgPrq0mGJMDDIZnYG708SbAXr4ODYkfOfgvJhEDJW93RANSETsP5l9eELhLni0qtZbC81P9oClKWjuTzU41braWCu/pRSY9mECQDqQU9fJBzNCbvjd+NEsNU8tAWqfUeHQs6fPVKrHfl9tV8RTYjmQh6gf5onCLvjmXFiGLUlNgfMAioJFaUKQvJP4u8Eqw3Z+slGfDCIJuQB6mfBj/+xQHjY7ZwftNyGc9mcJjt0uApivRUkiWphSsrqA7OjNq0+eoTcGSagauHnEmZsSzOqWiZjEc5Gk2Q2iKaSjSkgLcZqdcwCFFOhNDPxAOuqWaCx0IaF0v6RsIpLbA7aAs3SDyxQFopWOx5Vv7O8gD8HuI+yALN55W813pYJXTtyOGdTYa3IgxSMoyPOnDEwcB9lAeYf1T+w1Ibi47twPjbGjyxASsTSiOEg9Spl2EdZgPo38AuN4/1Dn6UpFN3xFp7NzygROYRGGfZRHuBX9Av9t6PDkh1pdk1FLjKbj1Egjmhj+mL30ViAQX6MT4Oq5y4prUg0xELhFwkoB6G6j/IA8U+8LTqECcC5D8rl2h8UYKdSPpDr3uiA2V34C6blLpHLskgSjUiuxMwTOAg5gKtrpZVPSDiEkWUhkp1UWBn1IOQAElvWE/kLyJuIIU0jw3YzA31fDmAd/8Sb5Xoeg1EQT1oWTMJSNwb6eRYg9CjMigRMqQXFXisR02qKIaKMrjEAs3n4/I4FmNoYtKTl9zBgr4KsDANQsRh0dWPU4CiPrzr2us0K+n0OIDaifyQgjhvEk3KxZqtRRj2IARgQcPI/vlKrOWMgcTL3HW8h4M4TsgEMQGxEeyfp2hghHffRB+NA7ctwAKERHVecIZgmIJ6n5ESsMqOcLgq/38SxMcwhKE8WhD0/oOPEqkELGAEakPBEa94QJPdaRdpz5nE6ne7qdV3P8r1ywo6PK+AT04B4iDdOLECGH5PV6799+bP96+ffOj+JNzAs66mJ5gkaEPeP7glrmtczdysjefgtn2cm8SqDXlK9mIBwlvBsDHpPXfGMPjfDFVuZLlrO0IDYFbxhTPP1e6WZ6v5qczIHcS8aVsBExQCEs8QFaWOydeiInLcZ6Z/YmTJjAqJnN2qUjcnniSX51YhOjsRLpn4swJW93QUNHRujBMzmyehtc0QmR+IFTcMCVLoyJCCeJTqUjSECclJXdGIddjaMstobpgFh93eNqOr16jCi6uqcJNRhP4gFiAMyE2xEqXWAqzcqsQ5PhN5LRAKENvBFPFttY+rMXb4vLYIQ9/RaOQYgXiwZ0oiqbIxq33RV78c4dRC/RTxA1Psb2FHjNqBljYmEF9yPYgHqKEXH8USVbiCXb2dn1rLXlArC9ACzGWSgO9CIEvHUBfVEYp3a4cMOcSzA3+jJA2hE1fvCAcL+QnqAeK3puNqKGYg5R9iawJSeFAHhOHpAswR+qWV1YD4IflaceRDuXu+82bNE8GtxvLS5zApy2lMEpJ+sBAyXDAr32lMERM5k33O1A/9pKD6ZlKXso/A1GpU4gMjUD9FaAuWeBEnugil9IjQPxgKEA8me5xX9Ksw0LzQw1AYZf2czFiBy4zsVAMh3RG010S4Y9BmGsQCRK9qsgGkQ299VdVD4Fn7nWIDQFR2jeZ6It6sAg91aCNhF35kERLZeeGrKySsqYPCUgzpSLxYgeqeJAVaD0QAVcyrsSJ0YgDim9m6AeF2EMaj2iuB3loGhiFE1vLU0M9BHD21F1W4f/s5wpBCAeLZ+McCwCT8PqgHxawzQSEkRMLwnowaEH2uCRgoFCNd0f8oo6hvaF1X3dzyeH1IDvICA9XAHrlC6DQ7O/kF5JBQgjGsTgOHWgyh+hWNOTygNggDEptABjLSGWxbc5YB7lA2YapUiYMiYDIpfQUfGhBtAKQKGi6rBbA24R+ls4UXLssCAzjShDIWFiIt2ULYGdjfgoi1VwFCRbRTe4exRRsx0woAzDBhmb+KkhowofIsBWrTFA5wQgHm2O/pgoCGI7fEEbuHFAnzFCe9UhsZcPacBVQFIOJhnyBUlJ3o4jAYUIHuH17GhqvU8/E7WP46eEIvfECaJCWm8yf7dqHnJGkFGFM83OGc8FmAPRRlsQiplXuhVdFDQgHhdAn28mMulIXnmRdNwfoTQ4KQGG5BwiHtwno+3ooc5VC6hRvjcry5fxI2JJpzn48VkdtBCzAUkrraaOXzH6jQL3M3n02AUQGLVKlwZIhlWEH5XTvi9muHwHanvScP+wgTO8/HioiLMQJ0oEMccR6NB4GPMd9l8kg/kWOCPfIHTAUlA+PXgfoKPsDgafazEprvvFcPPpzziiCfTWg2EG2PuTTi5tkTGvTx+fjiqHn90uk5DNszOzduJfR/EgbQvRXXOKI4gm8SxhljbZ9ThRB9hURz9r7Za5aeLi4s3o1LxLoQ4tvnUHwk7ajhVh9FF8bkhfDjRR5grlg7lRRzy1hXvAhZxZcL+YREn4hGjBOc7xtuj3xm3WGcjvTsE7BtV5hfmHFl4RMJvVoNv8Io26DmAeAtlCA8nLhI61yS4F8e4F0JQKdvEsnmGp8GYeTLi5BfzXI93R0XJvSNHXC1TtO+6RP+a6ENPRNI4CUgE4AesQegiSkbnjhznZhnyZAj2YxoVImmczFXDI8Dy5Q/4B5R998bwL4SAW4NzIxr5ogA80zPuHAqE5F8IQXQhMiueBiQWdL1KuqcHCSv3Sp28iZmzbenigHGFYnQRSaeUEWUcK6AiY4zb6eKIyHiTwQ548oYExIcTLT2wLjKNKGIImico31E+gD5eRwGa5O10MUQMwR7hibIOZ5HXDjVTbMLsLrTiA/KWAkYL0qdzzjl30UZUPoMIiZM3GdYZXnwRgtRDNT1DCgmfyFsKGKewCV9G6qWanp3JakrCPj55I8Q5R8/ZYGDfPR9emlZQEfboI9IcQNYRuUHChNm8+ydNTcg4Is25jYS3g9I9HpXCFGOBP6nX81NRv0PLOPGAYMJ3+og0604n5l77x2jEL1WC4PTp85ednTPdd6VjKZCQtjE8QO5eu3nebsdqRAl3d+180DPdW1LlCkGEzjUMMLTOunaMv9c+/Gi3NXoRuypZBU+/fb7295aztrviD25Dho1h3owXJh/k9LYdrppeVlbBm94FVFM7Hzkhm+BxSPsxTECeHfVknn7PS0iiimfWLjm5u/esrIJ3PvJuvswVVghnjGsYeBeohsyMtNT9ei8vVdGXb4+RF8rIKqF6Znr2fEl4ur66Aau9FGYuhAEkAj8qNYaXV3d708fdjKznatdm3f09lWVdvzDLMgLCIcPGcK+hDpm9u0za6PdNS/0ot/erCZucG924N6V/djlFn5SEE4aNiX6Z/ydKRehM83itza43scYmVBCaFc5tS+yaL6GSP1MldGeLDhXUDgWohTkzni6h67XxLlXklwbjrHs/h9DppRdlzqWKbEBNX6edCSDst2S+MRUqCdGC6+2kK7200xKbqOSliiEAGVlZn0aYK/U/qnJ/n4qp8wEzWva/1Ite8wn/+yW3iRMrLCUANTqM/3mEdmGE5EqD2YT6WmfDZcJDmcBAXckWAjCj5dq//p5eKnfCyRhQOEAtp/33F1kaexscv3MowHWXVFwi1DjRn3CANuH530NIR35CAgrC4mi/s07Cj1Ax9LCAkrA0+hmyIF9iMjsfP0PtRoYGtLMjR9X3Tzc2/d5g9lZptQ6IO9jiAnr5n99DnfCMpUZvPHlzK8W5gDzCCIBOduR+tfqjGa0uYhjJCn8nIr/UzcE8Th0w4zoSR9XqrJMaWr87vvnjr/Dn5WDugwToRADt1EE7/7PaehgnPBwbZm9882L4mm2hCp6bgpkmoD/F9dhifJs0k4BsDHvNwexCovmbbc7mz8FMF9CX4mqXDGwZP1+b3WieaqNrgd28iFTuwJKTR3O2Ii8HMwFAN//Trqbn1H60bPjDZNDpmX2StNEwu53O+GYyexMtVjGWyPxofjZeDmYigL4aZR6jU6zUAn26eJhNBoPxuGmpY0n873g8GFhIolSt0bKorHZfrX96vIy2XAUv1DvGApxn8XqlH/11Wa2XtwhaPll/YSyUqvVVdV0ofrqMluO6nkkD+hk9yHlVVlLLRV33/WQx0ZICnDOKMp5OsdL9xfqzCnl1ap2SriVFMc0475YIYMafju0UKy15FWiPAuQrVFvyyroWVsuExn+xpACFtDllzq1AW1LLK1TrlXXVkiRzlCSg0PwVg4vRBkhbULKvkzygIy2kUnkJqZQAPa0Fyq+0AdeuLeCmawu46doCbrq2gJuuLeCmawu46doCbrq2gJuuLeCm6/8BMPtPywLc/bd1/z9xP+Tcer3+VgAAAABJRU5ErkJggg==';

    public icono2;

    constructor () {
        // this.loadImage('assets/images/logo.png');
    }

    public loadImage(imagePath) {
        const img = new Image();
        img.src = imagePath;
        img.addEventListener('load', () => {
            const canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            const context = canvas.getContext('2d');
            context.drawImage(img, 0, 0);
            const dataURL = canvas.toDataURL('image/jpeg');
            ImagesBase64.icono = dataURL;
        });
    }

    // getBase64ImageFromURL(url: string) {
    //     return new Promise((resolve) => {
    //         const img = new Image();
    //         img.src = url;
    //         img.addEventListener('load', () => {
    //             const canvas = document.createElement('canvas');
    //             canvas.width = img.width;
    //             canvas.height = img.height;
    //             const context = canvas.getContext('2d');
    //             context.drawImage(img, 0, 0);
    //             const dataURL = canvas.toDataURL('image/jpeg');
    //             defer.resolve(dataURL);
    //         }
    //     });
    // }

    // getBase64Image(img: HTMLImageElement) {
    //     const canvas = document.createElement('canvas');
    //     canvas.width = img.width;
    //     canvas.height = img.height;
    //     const ctx = canvas.getContext('2d');
    //     ctx.drawImage(img, 0, 0);
    //     const dataURL = canvas.toDataURL('image/png');
    //     return dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
    // }
}
