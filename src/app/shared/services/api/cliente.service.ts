import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class Cliente {
  public id: number;
  public idtipodecliente: number;
  public razonsocial: string;
  public telefono: string;
  public email: string;
  public direccion: string;
  public tipodecliente: number;
  public clientetipo: string;
  public cuit: number;
  public emitido: number;
  public produccion: number;
  public borrador: string;
  constructor() {
    this.cuit = 0;
  }
}

export class CuentaCorriente {
  public id: number;
  public razonsocial: string;
  public idpresupuesto: number;
  public fecentrega: string;
  public saldo: number;
  public monto: number;
}

@Injectable()
export class ClienteService extends BasicService {
  readonly GET_CUENTACORRIENTE = 'cuentacorriente';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'cliente';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Cliente[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Cliente[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getCuentaCorriente(idcliente: number): Promise<CuentaCorriente[]> {
    const customUrl = `${this.apiUrl}/${this.GET_CUENTACORRIENTE}/${idcliente}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as CuentaCorriente[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Cliente> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Cliente;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Cliente): Promise<Cliente> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Cliente): Promise<Cliente> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Cliente): Promise<Cliente> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Cliente): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response =>  {
        if (+response.status === this.ENTITY_DELETED) {
          return this.ENTITY_DELETED;
        } else if (+response.status === this.ERROR_FOREING_KEY) {
          return this.ERROR_FOREING_KEY;
        } else {
          return this.ERROR_UNKNOWN;
        }
      })
      .catch(this.handleError);
}

  // private handleError(error: any): Promise<any> {
  //   console.error('An error occurred', error);
  //   return Promise.reject(error.message || error);
  // }
}
