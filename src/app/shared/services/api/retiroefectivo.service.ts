import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class RetiroEfectivo {
  public id: number;
  public idresponsable: number;
  public responsable: string;
  public fecha: string;
  public concepto: string;
  public monto: string;
}

@Injectable()
export class RetiroEfectivoService extends BasicService {
  private apiUrl = '';
  private headers;
  constructor(private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'retiroefectivo';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<RetiroEfectivo[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as RetiroEfectivo[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<RetiroEfectivo> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as RetiroEfectivo;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: RetiroEfectivo): Promise<RetiroEfectivo> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: RetiroEfectivo): Promise<RetiroEfectivo> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        return body.status === 'success';
      }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: RetiroEfectivo): Promise<RetiroEfectivo> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        return body.status === 'success';
      }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: RetiroEfectivo): Promise<Response> {
    const customUrl = `${this.apiUrl}/${entity.id}`;

    return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        if (+response.status === this.ENTITY_DELETED) {
          return this.ENTITY_DELETED;
        } else if (+response.status === this.ERROR_FOREING_KEY) {
          return this.ERROR_FOREING_KEY;
        } else {
          return this.ERROR_UNKNOWN;
        }
      })
      .catch(this.handleError);
  }
}
