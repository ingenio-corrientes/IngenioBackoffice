import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class ReciboItem {
  public id: number;
  public idrecibo: number;
  public idformacobro: number;
  public formacobro: string;
  public idbanco: number;
  public banco: string;
  public origendestino: string;
  public librador: string;
  public numero: string;
  public fecha: string;
  public fecvencimiento: string;
  public monto: number;
  public fecultmodif: string;

  constructor() {
    this.id = -1;
    this.idformacobro = 0;
    this.monto = 0;
    this.librador = '';
  }
}

@Injectable()
export class ReciboItemService {
  readonly GET_LISTBYIDRECIBO = 'byidrecibo';
  private apiUrl = '';
  private headers;

  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'reciboitem';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<ReciboItem[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as ReciboItem[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<ReciboItem> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as ReciboItem;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getListByIDRecibo(id: number): Promise<ReciboItem[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDRECIBO}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as ReciboItem[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: ReciboItem): Promise<ReciboItem> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: ReciboItem): Promise<ReciboItem> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: ReciboItem): Promise<ReciboItem> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: ReciboItem): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
