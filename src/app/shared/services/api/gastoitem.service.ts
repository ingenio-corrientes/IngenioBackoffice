import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class GastoItem {
  public id: number;
  public idgasto: number;
  public idformapago: number;
  public formapago: string;
  public idbanco: number;
  public idcheque: string;
  public origendestino: string;
  public numero: string;
  public fecha: string;
  public fecvencimiento: string;
  public monto: number;
  public propio: number;
  public fecultmodif: string;

  constructor() {
    this.id = -1;
    this.propio = 1;
    this.idformapago = -1;
    this.idcheque = '0';
    this.monto = 0;
  }
}

@Injectable()
export class GastoItemService {
  readonly GET_LISTBYIDGASTO = 'byidgasto';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'gastoitem';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<GastoItem[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as GastoItem[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<GastoItem> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as GastoItem;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getListByIDGasto(id: number): Promise<GastoItem[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDGASTO}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as GastoItem[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: GastoItem): Promise<GastoItem> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: GastoItem): Promise<GastoItem> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: GastoItem): Promise<GastoItem> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: GastoItem): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
