import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class PresupuestoItemCarteleria {
  public id: number;
  public idpresupuesto: number;
  public idsustrato: number;
  public idsubtiposus: number;
  public ancho: number;
  public alto: number;
  public descripcion: string;
  public cantidad: number;
  public precio: number;
  public fecultmodif: string;

  constructor() {
    this.idsustrato = 0;
    this.cantidad = 0;
  }
}

@Injectable()
export class PresupuestoItemCarteleriaService extends BasicService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'presupuestoitemcarteleria';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<PresupuestoItemCarteleria[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as PresupuestoItemCarteleria[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<PresupuestoItemCarteleria> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as PresupuestoItemCarteleria;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: PresupuestoItemCarteleria): Promise<any> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: PresupuestoItemCarteleria): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (response.status === this.SYNCRO_OK) {
            return body.message;
          } else if (response.status === this.ERROR_NOT_MODIFIED) {
            return this.ERROR_NOT_MODIFIED;
          } else {
            return this.ERROR_UNKNOWN;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: PresupuestoItemCarteleria): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (response.status === 200) {
            return body.message;
          } else if (response.status === 304) {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: PresupuestoItemCarteleria): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response =>  {
        if (+response.status === this.ENTITY_DELETED) {
          return this.ENTITY_DELETED;
        } else if (+response.status === this.ERROR_FOREING_KEY) {
          return this.ERROR_FOREING_KEY;
        } else {
          return this.ERROR_UNKNOWN;
        }
      })
      .catch(this.handleError);
  }
}
