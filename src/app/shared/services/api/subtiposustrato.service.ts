import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class SubtipoSustrato {
  public id: number;
  public idsustrato: number;
  public sustrato: string;
  public subtiposustrato: string;
  public anchomin: number;
  public altomin: number;
  public anchomax: number;
  public altomax: number;
  constructor() {
    this.id = -1;
  }
}

@Injectable()
export class SubtipoSustratoService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'subtiposustrato';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<SubtipoSustrato[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as SubtipoSustrato[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<SubtipoSustrato> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as SubtipoSustrato;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: SubtipoSustrato): Promise<SubtipoSustrato> {
    if (entity.id > -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: SubtipoSustrato): Promise<SubtipoSustrato> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.message;
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: SubtipoSustrato): Promise<SubtipoSustrato> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: SubtipoSustrato): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
