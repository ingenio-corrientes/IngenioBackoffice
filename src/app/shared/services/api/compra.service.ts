import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class Compra {
    public id: string;
    public idproveedor: number;
    public proveedor: string;
    public numero: string;
    public fecemision: string;
    public idtipocomprobante: number;
    public observacion: string;
    public subtotal: number;
    public total: number;
    public saldo: number;
    public tope: number;
    public bloqueado: number;

  constructor() {
    this.id = '';
    this.observacion = '';
  }
}

@Injectable()
export class CompraService extends BasicService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'compra';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Compra[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Compra[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<Compra> {
    const url = `${this.apiUrl}/${id}`;
    return this.http
      .get(url, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Compra;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Compra): Promise<string> {
    if (entity.id && entity.id.length > 0) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Compra): Promise<string> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (response.status === this.SYNCRO_OK) {
            return body.message;
          } else if (body.message === this.ERROR_DUPLICATED) {
            return this.ERROR_NOT_MODIFIED;
          } else {
            return this.ERROR_UNKNOWN;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Compra): Promise<string> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (response.status === 200) {
            return body.message;
          } else if (response.status === 304) {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Compra): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
            if (+response.status === this.ENTITY_DELETED) {
              return this.ENTITY_DELETED;
            } else if (+response.status === this.ERROR_FOREING_KEY) {
              return this.ERROR_FOREING_KEY;
            } else {
              return this.ERROR_UNKNOWN;
            }
          })
          .catch(this.handleError);
  }
}
