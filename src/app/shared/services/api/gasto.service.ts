import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class Gasto {
  public id: number;
  public idproveedor: number;
  public razonsocial: string;
  public fecha: string;
  public concepto: string;
  public monto: number;
  public borrador: string;
  public dirty: number;
  public totaldeuda: number;
  public idcompraapagar: string;
  public fecemision: string;

  constructor() {
    this.id = -1;
    this.fecemision = '';
    this.dirty = 1;
  }
}

@Injectable()
export class GastoService extends BasicService {;
  private readonly GET_FILTEREDBYDATES = 'filteredbydates';
  private readonly PUT_EMITIR = 'emitir';
  private readonly PUT_PAGAR = 'pagar';
  public readonly ESTADO_OK = 200;
  public readonly ESTADO_SINDEUDA = 1;
  public readonly ESTADO_SINPAGOS = 2;
  public readonly ESTADO_ERRORSALDO = 3;

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'gasto';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Gasto[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Gasto[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getFilteredByDate(fecinicio: number, fecfin: number): Promise<Gasto[]> {
    const customUrl = `${this.apiUrl}/${this.GET_FILTEREDBYDATES}/${fecinicio}/${fecfin}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Gasto[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Gasto> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Gasto;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Gasto): Promise<Gasto> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Gasto): Promise<Gasto> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message
          } else {
            return false;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Gasto): Promise<Gasto> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.message;
        }
      )
      .catch(e => this.handleError(e));
  }

  public emitir(entity: Gasto): Promise<any> {
    const url = `${this.apiUrl}/${this.PUT_EMITIR}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'error') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  public pagar(entity: Gasto): Promise<any> {
    const url = `${this.apiUrl}/${this.PUT_PAGAR}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'error') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Gasto): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
            if (+response.status === this.ENTITY_DELETED) {
              return this.ENTITY_DELETED;
            } else if (+response.status === this.ERROR_FOREING_KEY) {
              return this.ERROR_FOREING_KEY;
            } else {
              return this.ERROR_UNKNOWN;
            }
          })
          .catch(this.handleError);
  }

  // private handleError(error: any): Promise<any> {
  //   console.error('An error occurred', error);
  //   return Promise.reject(error.message || error);
  // }
}
