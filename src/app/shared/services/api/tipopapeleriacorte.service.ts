import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class TipoPapeleriaCorte {
  public id: number;
  public tipocorte: string;
}

@Injectable()
export class TipoPapeleriaCorteService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'tipopapeleriacorte';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<TipoPapeleriaCorte[]> {
    const customUrl = `${this.apiUrl}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as TipoPapeleriaCorte[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<TipoPapeleriaCorte> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as TipoPapeleriaCorte;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: TipoPapeleriaCorte): Promise<TipoPapeleriaCorte> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: TipoPapeleriaCorte): Promise<TipoPapeleriaCorte> {
    return this.http
    .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
    .toPromise()
    .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message
        } else {
          return false;
        }
      }
    )
    .catch(e => this.handleError(e));
  }

  private put(entity: TipoPapeleriaCorte): Promise<TipoPapeleriaCorte> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: TipoPapeleriaCorte): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
