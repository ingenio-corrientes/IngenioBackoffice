import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Precio {
  public id: number;
  public precio: string;
  public idtiposerv: number;
  public servicio: string;
  public tipoclient: string;
  public desyreca: number;
  public monto: number;
  public idlistabase: number;
  public descripcion: string;
}

@Injectable()
export class PrecioService {;

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'precio';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Precio[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Precio[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }



  getByID(id: number): Promise<Precio> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Precio;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Precio): Promise<Precio> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Precio): Promise<Precio> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Precio): Promise<Precio> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
