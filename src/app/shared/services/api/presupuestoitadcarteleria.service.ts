import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class PresupuestoItAdCarteleria {
  public id: number;
  public idpresitcarteleria: number;
  public idtipoadicional: number;
  public tipoadicional: string;
  public ancho: number;
  public alto: number;
  public unidades: number;
  public precio: number;
  public fecultmodif: string;
  constructor() {
    this.id = -1;
    this.ancho = 0;
    this.alto = 0;
    this.unidades = 0;
    this.precio = 0.0;
  }
}

@Injectable()
export class PresupuestoItAdCarteleriaService {
  readonly GET_BYIDPRESITCARTELERIA = 'byidpresitcarteleria';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'presupuestoitadcarteleria';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<PresupuestoItAdCarteleria[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as PresupuestoItAdCarteleria[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<PresupuestoItAdCarteleria> {
    const customUrl = `${this.apiUrl}/${id}`
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as PresupuestoItAdCarteleria;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByIDIPresItCarteleria(id: number): Promise<PresupuestoItAdCarteleria[]> {
    const customUrl = `${this.apiUrl}/${this.GET_BYIDPRESITCARTELERIA}/${id}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as PresupuestoItAdCarteleria[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: PresupuestoItAdCarteleria): Promise<any> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: PresupuestoItAdCarteleria): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: PresupuestoItAdCarteleria): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: PresupuestoItAdCarteleria): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
