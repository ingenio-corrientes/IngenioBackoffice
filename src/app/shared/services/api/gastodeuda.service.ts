import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/toPromise';

export class GastoDeuda {
    public id: number;
    public idgasto: number;
    public idcompra: string;
    public montoacancelar: number;
    public idtipocomprobante: number;
    public tipocomprobante: string;
    public numero: number;
    public fecemision: string;
    public subtotal: number;
    public total: number;
    public saldo: number;
}

@Injectable()
export class GastoDeudaService {
    readonly GET_LISTBYIDGASTO = 'listbyidgasto';
    readonly PUT_RELOAD_DEUDAS = 'reloaddeudas';
    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'gastodeuda';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
    }

    getList(): Promise<GastoDeuda[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as GastoDeuda[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getByID(id: string): Promise<GastoDeuda> {
        const url = `${this.apiUrl}/${id}`;
        return this.http
            .get(url, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as GastoDeuda;
                return entity;
            }
            )
            .catch(e => this.handleError(e));
    }

    getDeudasByIdGasto(idgasto: number): Promise<GastoDeuda[]> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDGASTO}/${idgasto}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as GastoDeuda[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    reloadDeudas(idgasto: number): Promise<GastoDeuda[]> {
        const customUrl = `${this.apiUrl}/${this.PUT_RELOAD_DEUDAS}/${idgasto}`;

        return this.http
            .put(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return true;
                }
            }
            )
            .catch(e => this.handleError(e));
    }

    save(entity: GastoDeuda): Promise<any> {
        if (entity.id) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: GastoDeuda): Promise<any> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            }
            )
            .catch(e => this.handleError(e));
    }

    private put(entity: GastoDeuda): Promise<any> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity))
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            }
            )
            .catch(e => this.handleError(e));
    }

    delete(entity: GastoDeuda): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.ok;
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
