import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class LPrecioAdicional {
  public id: number;
  public idlistaprecio: number;
  public idtipoadicional: number;
  public tipoadicional: string;
  public precio: number;
  public fecultmodif;
  constructor() {
    this.id = -1;
  }
}

@Injectable()
export class LPrecioAdicionalService {
  readonly GET_LISTBYIDLISTA = 'byidlista';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'lprecioadicional';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<LPrecioAdicional[]> {
    const customUrl = `${this.apiUrl}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as LPrecioAdicional[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<LPrecioAdicional> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as LPrecioAdicional;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getListByIDLista(id: number): Promise<LPrecioAdicional[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDLISTA}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as LPrecioAdicional[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: LPrecioAdicional): Promise<any> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: LPrecioAdicional): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: LPrecioAdicional): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else {
          return -1;
        }
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: LPrecioAdicional): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;
      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
