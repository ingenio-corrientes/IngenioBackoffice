import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class LPrecioPapeleria {
    public id: number;
    public idlistaprecio: number;
    public idtipopapeleriatrabajo: number;
    public tipotrabajopapeleria: string;
    public idgramaje: number;
    public gramaje: string;
    public idtipopapelcom: number;
    public tipopapeleriacomercial: string;
    public descripcion: string;
    public idsubtipopapelcom: number;
    public subtipopapeleriacomercial: string;
    public idtamano: number;
    public tamano: string;
    public idtipocorte: number;
    public tipocorte: string;
    public comercial: number;
    public concorte: number;
    public doblefaz: number;
    public color: number;
    public precio: number;
    public fecultmodif: string;
    constructor() {
        this.id = -1;
    }
}

@Injectable()
export class LPrecioPapeleriaService {
    readonly GET_LISTBYIDLISTA = 'byidlista';
    readonly PUT_UPDATELISTA = 'updatelista';
    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'lpreciopapeleria';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        // this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<LPrecioPapeleria[]> {
        const customUrl = `${this.apiUrl}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as LPrecioPapeleria[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<LPrecioPapeleria> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as LPrecioPapeleria;
                return entity;
            }
            )
            .catch(e => this.handleError(e));
    }

    getListByIDLista(id: number): Promise<LPrecioPapeleria[]> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDLISTA}/${id}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as LPrecioPapeleria[];
                return entity;
            }
            )
            .catch(e => this.handleError(e));
    }

    save(entity: LPrecioPapeleria): Promise<any> {
        if (entity.id && entity.id !== -1) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: LPrecioPapeleria): Promise<any> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            }
            )
            .catch(e => this.handleError(e));
    }

    private put(entity: LPrecioPapeleria): Promise<any> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else {
                    return -1;
                }
            }
            )
            .catch(e => this.handleError(e));
    }

    delete(entity: LPrecioPapeleria): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;
        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.ok;
            })
            .catch(this.handleError);
    }

    updateLista(id: number): Promise<any> {
        const url = `${this.apiUrl}/${this.PUT_UPDATELISTA}/${id}`;

        return this.http
            .put(url, '', { headers: this.headers })
            .toPromise()
            .then(response => {
                return response.status === 200;
                // const body = response.json();
                // return body.status === 'success';
            }
            )
            .catch(e => this.handleError(e));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
