import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class Presupuesto {
    readonly DEFAULT_FORMAPAGO = 1;

    public id: number;
    public idusuario: number;
    public usuario: string;
    public idcliente: number;
    public razonsocial: string;
    public idformacobro: number;
    public formacobro: string;
    public fecemision: string;
    public observacion: string;
    public descuyrecar: number;
    public fecentrega: string;
    public fecaproduccion: string;
    public saldo: number;
    public actacte: number;
    public dirty: number;
    public estado: string;
    public total: number;

    constructor() {
        this.idcliente = null;
        this.idformacobro = this.DEFAULT_FORMAPAGO;
        this.fecemision = '';
        this.observacion = '';
        this.descuyrecar = 0.00;
        this.fecentrega = '';
        this.fecaproduccion = '';
        this.saldo = 0.00;
        this.total = 0.00;
        this.actacte = 0;
        this.dirty = 1;
    }
}

export class PresupuestoItem {
    public id: number;
    public idpresupuesto: number;
    public idtiposervicio: number;
    public tiposervicio: string;
    public iditem: number; // representa cualquiera de los tipos de servicios
    public descripcion: string;
    public descripcionpdf: string;
    public cantidad: number;
    public precio: number;
    public unidades: number;
    public subtotal: number;
}

export class PresupuestoItemAds {
    public id: number;
    public idpresupuesto: number;
    public idtiposervicio: number;
    public idservicio: number;
    public tiposervicio: string;
    public iditem: number; // representa cualquiera de los tipos de servicios
    public descripcion: string;
    public cantidad: number;
    public precio: number;
    public subtotal: number;
}

@Injectable()
export class PresupuestoService extends BasicService {
    readonly GET_PENDIENTESBYIDCLIENTE = 'pendbyidcliente';
    readonly GET_ITEMSBYID = 'itemsbyid';
    readonly GET_ITEMSADSBYID = 'itemsadsbyid';
    readonly PUT_EMITIR = 'emitir';
    readonly PUT_APRODUCCION = 'aproduccion';
    readonly PUT_APLICARDESREC = 'aplicardescrec';
    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        super();
        this.apiUrl = environment.apiUrl + 'presupuesto';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<Presupuesto[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Presupuesto[];
                return entities;
            })
            .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<Presupuesto> {
        return this.http
            .get(this.apiUrl + '/' + id, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Presupuesto;
                return entity;
            })
            .catch(e => this.handleError(e));
    }

    getPendientesByIdCliente(idcliente: number): Promise<Presupuesto[]> {
        const customUrl = `${this.apiUrl}/${this.GET_PENDIENTESBYIDCLIENTE}/${idcliente}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Presupuesto[];
                return entities;
            })
            .catch(e => this.handleError(e));
    }

    getItemsById(id: number): Promise<PresupuestoItem[]> {
        const customUrl = `${this.apiUrl}/${this.GET_ITEMSBYID}/${id}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as PresupuestoItem[];
                return entities;
            })
            .catch(e => this.handleError(e));
    }

    getItemsAdsById(id: number): Promise<PresupuestoItemAds[]> {
        const customUrl = `${this.apiUrl}/${this.GET_ITEMSADSBYID}/${id}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as PresupuestoItemAds[];
                return entities;
            })
            .catch(e => this.handleError(e));
    }

    save(entity: Presupuesto): Promise<Presupuesto> {
        if (entity.id) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: Presupuesto): Promise<Presupuesto> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message
                } else {
                    return false;
                }
            })
            .catch(e => this.handleError(e));
    }

    private put(entity: Presupuesto): Promise<Presupuesto> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else {
                    return -1;
                }
            })
            .catch(e => this.handleError(e));
    }

    emitir(entity: Presupuesto): Promise<Presupuesto> {
        const url = `${this.apiUrl}/${this.PUT_EMITIR}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity))
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            })
            .catch(e => this.handleError(e));
    }

    aProduccion(entity: Presupuesto): Promise<Presupuesto> {
        const url = `${this.apiUrl}/${this.PUT_APRODUCCION}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity))
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            })
            .catch(e => this.handleError(e));
    }

    public aplicarDescRec(entity: Presupuesto): Promise<Presupuesto> {
        const url = `${this.apiUrl}/${this.PUT_APLICARDESREC}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity))
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            })
            .catch(e => this.handleError(e));
    }

    delete(entity: Presupuesto): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                if (+response.status === this.ENTITY_DELETED) {
                    return this.ENTITY_DELETED;
                } else if (+response.status === this.ERROR_FOREING_KEY) {
                    return this.ERROR_FOREING_KEY;
                } else {
                    return this.ERROR_UNKNOWN;
                }
            })
            .catch(this.handleError);
    }
}
