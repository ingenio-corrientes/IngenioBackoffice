import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Usuario {
    public id: number;
    public usuario: string;
    public apellido: string;
    public nombre: string;
    public contrasena: string;
    public idrol: number;
    constructor() {
        this.id = -1;
        this.contrasena = environment.DEFAULT_PASS;
    }
}

export class ResetPass {
    public id: number;
    public contrasena = environment.DEFAULT_PASS;
    public reset = 1;
}

export class ChangePass {
    public id: number;
    public contrasena: string;
    public newcontrasena: string;
    public repitcontrasena: string;
    public change = 1;
}

@Injectable()
export class UsuarioService {
    private apiUrl = '';
    private headers;

    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'usuario';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
    }

    getList(): Promise<Usuario[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Usuario[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<Usuario> {
        return this.http
            .get(this.apiUrl + '/' + id)
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Usuario;
                return entity;
            }
            )
            .catch(e => this.handleError(e));
    }

    getUsuarios(): Promise<Usuario[]> {


        return this.http
            .get(this.apiUrl)
            .toPromise()
            .then(response => {
                const body = response.json();
                const usuarios = body || {} as Usuario[];
                return usuarios;
            }
            )
            .catch(e => this.handleError(e));
    }

    autenticate(usuarioName: string, clave: string): Promise<Boolean> {

        const json_req = JSON.stringify({
            'auth':
            {
                'usuario': usuarioName,
                'contrasena': clave
            }
        });

        return this.http
            .post(this.apiUrl, json_req)
            .toPromise()
            .then(res => {
                const body = res.json();

                const fecha = new Date;
                const startSession = JSON.stringify({
                    'usuario': usuarioName,
                    'id': body.message,
                    'initsession': {
                        'year': fecha.getFullYear(),
                        'month': fecha.getMonth(),
                        'day': fecha.getDay(),
                        'hour': fecha.getHours(),
                        'minute': fecha.getMinutes()
                    }
                });
                localStorage.setItem('userLog', startSession);
                return body.status === 'success';
            })
            .catch(this.handleError);
    }

    getUsuario(id: number): Promise<Usuario> {
        return this.getUsuarios()
            .then(usuarios => usuarios.find(usuario => usuario.id === id));
    }

    getUsuarioByNombre(nombre: string): Promise<Usuario> {
        return this.getUsuarios()
            .then(usuarios => usuarios.find(usuario => usuario.usuario === nombre));
    }

    save(usuario: Usuario): Promise<Usuario> {
        if (usuario.id && usuario.id > -1) {
            return this.put(usuario);
        }
        return this.post(usuario);
    }

    delete(usuario: Usuario): Promise<Response> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const url = `${this.apiUrl}/${usuario.id}`;

        return this.http
            .delete(url, { headers: headers })
            .toPromise()
            .catch(this.handleError);
    }

    private post(entity: Usuario): Promise<Usuario> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            }).catch(e =>
                this.handleError(e));
    }

    public resetPassword(entity: ResetPass): Promise<ResetPass> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(customUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            })
            .catch(this.handleError);
    }

    public changePass(entity: ChangePass): Promise<ResetPass> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(customUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            })
            .catch(this.handleError);
    }

    private put(entity: Usuario): Promise<Usuario> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(customUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                return body.status === 'success';
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
