import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class Resumen {
    public enproduccion: number;
    public noaprobados: number;
    public caja: number;
    public clientectacte: number;
}

@Injectable()
export class ReporteService extends BasicService {
    readonly GET_RESUMEN = 'resumen';
    private apiUrl = '';
    private headers;
    constructor (private http: Http) {
        super();
        this.apiUrl = environment.apiUrl +  'reporte';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        // this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<Resumen> {
        const customUrl = `${this.apiUrl}/${this.GET_RESUMEN}`;

        return this.http
        .get(customUrl, { headers: this.headers })
        .toPromise()
        .then(response => {
            const body = response.json();
            const entity = body[0] as Resumen;
            return entity;
        })
        .catch(e => this.handleError(e));
    }
}
