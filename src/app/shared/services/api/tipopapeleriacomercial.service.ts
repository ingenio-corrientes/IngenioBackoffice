import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class TipoPapeleriaComercial {
  public id: number;
  public tipopapeleriacomercial: string;
}

@Injectable()
export class TipoPapeleriaComercialService extends BasicService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'tipopapeleriacomercial';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<TipoPapeleriaComercial[]> {
    const customUrl = `${this.apiUrl}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as TipoPapeleriaComercial[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<TipoPapeleriaComercial> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as TipoPapeleriaComercial;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: TipoPapeleriaComercial): Promise<TipoPapeleriaComercial> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: TipoPapeleriaComercial): Promise<TipoPapeleriaComercial> {
    return this.http
    .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
    .toPromise()
    .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message
        } else {
          return false;
        }
      }
    )
    .catch(e => this.handleError(e));
  }

  private put(entity: TipoPapeleriaComercial): Promise<TipoPapeleriaComercial> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: TipoPapeleriaComercial): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response =>  {
        if (+response.status === this.ENTITY_DELETED) {
          return this.ENTITY_DELETED;
        } else if (+response.status === this.ERROR_FOREING_KEY) {
          return this.ERROR_FOREING_KEY;
        } else {
          return this.ERROR_UNKNOWN;
        }
      })
      .catch(this.handleError);
  }
}
