import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class CompraItem {
  public id: string;
  public idcompra: string;
  public descripcion: string;
  public precio: number;
  public cantidad: number;

  constructor() {
    this.id = '-';
  }
}

@Injectable()
export class CompraItemService extends BasicService {
  readonly GET_LISTBYIDCOMPRA = 'itemsbyidcompra';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'compraitem';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<CompraItem[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as CompraItem[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<CompraItem> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as CompraItem;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getListByIDGasto(id: string): Promise<CompraItem[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDCOMPRA}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as CompraItem[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: CompraItem): Promise<CompraItem> {
    if (entity.id && entity.id !== '-') {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: CompraItem): Promise<CompraItem> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: CompraItem): Promise<CompraItem> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: CompraItem): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }
}
