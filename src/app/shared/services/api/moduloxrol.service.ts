import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class ModuloXRol {
  public id: number;
  public idrol: number;
  public rol: string;
  public idmodulo: number;
  public modulo: string;
  public ver: number;
  public editar: number;
  public eliminar: number;

  constructor() {
    this.id = -1;
  }
}

@Injectable()
export class ModuloXRolService {
  readonly GET_LISTBYIDROL = 'byidrol';
  readonly GET_LISTBYIDUSUARIOIDMODULO = 'byidusuarioidmodulo';

  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'moduloxrol';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<ModuloXRol[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as ModuloXRol[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<ModuloXRol> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as ModuloXRol;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getListByIDRol(id: number): Promise<ModuloXRol[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDROL}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as ModuloXRol[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getListByIDUsuarioIDModulo(idusuario: number, idmodulo: number): Promise<ModuloXRol> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDUSUARIOIDMODULO}/${idusuario}/${idmodulo}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as ModuloXRol;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: ModuloXRol): Promise<ModuloXRol> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: ModuloXRol): Promise<ModuloXRol> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: ModuloXRol): Promise<ModuloXRol> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: ModuloXRol): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
