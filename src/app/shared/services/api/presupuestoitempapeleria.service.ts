import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class PresupuestoItemPapeleria {
  public id: number;
  public idpresupuesto: number;
  public idtipotrabajopapeleria: number;
  public idgramaje: number;
  public idtamano: number;
  public idtipopapelcom: number;
  public idsubpapelcom: number;
  public idtipocorte: number;
  public unidades: number;
  public descripcion: string;
  public pliegos: number;
  public doblefaz: number;
  public precio: number;
  public color: number;
  public fecultmodif: string;
  constructor() {
    this.id = -1;
    this.idtipotrabajopapeleria = -1;
    this.idgramaje = -1;
    this.idtipopapelcom = -1;
    this.idsubpapelcom = -1;
    this.idtipocorte = -1;
    this.unidades = 0;
    this.descripcion = '';
    this.pliegos = 0;
    this.color = 1;
  }
}

@Injectable()
export class PresupuestoItemPapeleriaService extends BasicService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'presupuestoitempapeleria';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<PresupuestoItemPapeleria[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as PresupuestoItemPapeleria[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<PresupuestoItemPapeleria> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as PresupuestoItemPapeleria;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: PresupuestoItemPapeleria): Promise<any> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: PresupuestoItemPapeleria): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: PresupuestoItemPapeleria): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: PresupuestoItemPapeleria): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response =>  {
        if (+response.status === this.ENTITY_DELETED) {
          return this.ENTITY_DELETED;
        } else if (+response.status === this.ERROR_FOREING_KEY) {
          return this.ERROR_FOREING_KEY;
        } else {
          return this.ERROR_UNKNOWN;
        }
      })
      .catch(this.handleError);
  }
}
