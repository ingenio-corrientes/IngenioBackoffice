import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class Proveedor {
  public id: number;
  public razonsocial: string;
  public cuit: number;
  public telefono: string;
  public email: string;
  public direccion: string;
  constructor() {
    this.cuit = 0;
  }
}

export class CuentaCorrienteProveedor {
  public id: number;
  public razonsocial: string;
  public idcompra: number;
  public numero: number;
  public idtipocomprobante: number;
  public tipocomprobante: string;
  public fecemision: string;
  public saldo: number;
  public monto: number;
}

@Injectable()
export class ProveedorService extends BasicService {
  readonly GET_CUENTACORRIENTEPROVEEDOR = 'cuentacorrienteproveedor';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'proveedor';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Proveedor[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Proveedor[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getCuentaCorrienteProvedor($idproveedor: number): Promise<CuentaCorrienteProveedor[]> {
    const customUrl = `${this.apiUrl}/${this.GET_CUENTACORRIENTEPROVEEDOR}/${$idproveedor}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as CuentaCorrienteProveedor[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Proveedor> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Proveedor;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Proveedor): Promise<Proveedor> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Proveedor): Promise<Proveedor> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Proveedor): Promise<Proveedor> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Proveedor): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response =>  {
        if (+response.status === this.ENTITY_DELETED) {
          return this.ENTITY_DELETED;
        } else if (+response.status === this.ERROR_FOREING_KEY) {
          return this.ERROR_FOREING_KEY;
        } else {
          return this.ERROR_UNKNOWN;
        }
      })
      .catch(this.handleError);
}

  // private handleError(error: any): Promise<any> {
  //   console.error('An error occurred', error);
  //   return Promise.reject(error.message || error);
  // }
}
