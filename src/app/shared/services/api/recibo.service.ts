import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';
import { AnimationKeyframesSequenceMetadata } from '@angular/animations';

export class Recibo {
    public id: number;
    public idcliente: number;
    public razonsocial: string;
    public fecha: string;
    public fecemision: string;
    public idpresupuesto: number;
    public monto: number;
    public montopresupuesto: number;
    public saldopresupuesto: number;
    public borrador: string;

    constructor() {
        this.id = -1;
        this.fecemision = '';
    }
}

@Injectable()
export class ReciboService extends BasicService {
    private readonly GET_FILTEREDBYDATES = 'filteredbydates';
    readonly PUT_EMITIR = 'emitir';
    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        super();
        this.apiUrl = environment.apiUrl + 'recibo';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        // this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<Recibo[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Recibo[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getFilteredByDate(fecinicio: number, fecfin: number): Promise<Recibo[]> {
        const customUrl = `${this.apiUrl}/${this.GET_FILTEREDBYDATES}/${fecinicio}/${fecfin}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Recibo[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<Recibo> {
        return this.http
            .get(this.apiUrl + '/' + id, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Recibo;
                return entity;
            }
            )
            .catch(e => this.handleError(e));
    }

    save(entity: Recibo): Promise<Recibo> {
        if (entity.id && entity.id !== -1) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: Recibo): Promise<Recibo> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message
                } else {
                    return false;
                }
            }
            )
            .catch(e => this.handleError(e));
    }

    private put(entity: Recibo): Promise<Recibo> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else {
                    return -1;
                }
            }
            )
            .catch(e => this.handleError(e));
    }

    emitir(entity: Recibo): Promise<AnimationKeyframesSequenceMetadata> {
        const url = `${this.apiUrl}/${this.PUT_EMITIR}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity))
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else {
                    return -1;
                }
            })
            .catch(e => this.handleError(e));
    }

    delete(entity: Recibo): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                if (+response.status === this.ENTITY_DELETED) {
                    return this.ENTITY_DELETED;
                } else if (+response.status === this.ERROR_FOREING_KEY) {
                    return this.ERROR_FOREING_KEY;
                } else {
                    return this.ERROR_UNKNOWN;
                }
            })
            .catch(this.handleError);
    }
}
