import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class ListaPrecio {
  public id: number;
  public idtipocliente: number;
  public idtiposervicio: number;
  public tiposervicio: string;
  public idlistabase: number;
  public listaprecio: string;
  public desyreca: number;
  public monto: number;
  public doblefaz: number;
  public color: number;
  constructor() {
    this.id = -1;
    this.idtipocliente = -1;
    this.idlistabase = -1;
    this.desyreca = 0;
    this.monto = 0;
    this.doblefaz = 0;
    this.color = 0;
  }
}

@Injectable()
export class ListaPrecioService extends BasicService {
  public static readonly APLICAR_PORCENTAJE = 1;
  public static readonly APLICAR_MONTO = 1;

  readonly GET_LISTASBASE = 'getlistasbase';
  readonly GET_BYIDPRESUPUESTO = 'getbyidpresupuesto';
  readonly PUT_APPLY = 'apply';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'listaprecio';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<ListaPrecio[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as ListaPrecio[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<ListaPrecio> {
    const customUrl = `${this.apiUrl}/${id}`
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as ListaPrecio;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getListasBases(tiposervicio: number): Promise<ListaPrecio[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTASBASE}/${tiposervicio}`
    return this.http
    .get(customUrl, { headers: this.headers })
    .toPromise()
    .then(response => {
      const body = response.json();
      const entities = body || {} as ListaPrecio[];
      return entities;
      }
    )
    .catch(e => this.handleError(e));
  }

  getByIDPrsupuesto(idpresupuesto: number, tiposervicio: number): Promise<ListaPrecio> {
    const customUrl = `${this.apiUrl}/${this.GET_BYIDPRESUPUESTO}/${idpresupuesto}/${tiposervicio}`
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as ListaPrecio;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: ListaPrecio): Promise<any> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: ListaPrecio): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return false;
          }
        }
      )
      .catch(e =>
        this.handleError(e));
  }

  private put(entity: ListaPrecio): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else {
            return false;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  public apply(entity: ListaPrecio, tipo: number): Promise<any> {
    const customUrl = `${this.apiUrl}/${this.PUT_APPLY}/${entity.id}/${tipo}`;

    return this.http
      .put(customUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else {
            return false;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: ListaPrecio): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response =>  {
        if (+response.status === this.ENTITY_DELETED) {
          return this.ENTITY_DELETED;
        } else if (+response.status === this.ERROR_FOREING_KEY) {
          return this.ERROR_FOREING_KEY;
        } else {
          return this.ERROR_UNKNOWN;
        }
      })
      .catch(this.handleError);
}

  // private handleError(error: any): Promise<any> {
  //   console.error('An error occurred', error);
  //   return Promise.reject(error.message || error);
  // }
}
