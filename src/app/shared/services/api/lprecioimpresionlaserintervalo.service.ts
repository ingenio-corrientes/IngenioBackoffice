import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class LPrecioImpresionLaserIntervalo {
  public id: number;
  public idlprecioimpresionlaser: number;
  public maximo: number;
  public porcentaje: number;
  public redondear: number;
  public fecultmodif: string;
  constructor() {
    this.id = -1;
  }
}

@Injectable()
export class LPrecioImpresionLaserIntervaloService {
  readonly GET_LISTBYIDLISTA = 'byidlista';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'lprecioimpresionlaserintervalo';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<LPrecioImpresionLaserIntervalo[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as LPrecioImpresionLaserIntervalo[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<LPrecioImpresionLaserIntervalo> {
    return this.http
      .get(this.apiUrl + '/' + id, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as LPrecioImpresionLaserIntervalo;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getListByIDLista(id: number): Promise<LPrecioImpresionLaserIntervalo[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTBYIDLISTA}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as LPrecioImpresionLaserIntervalo[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: LPrecioImpresionLaserIntervalo): Promise<any> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: LPrecioImpresionLaserIntervalo): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: LPrecioImpresionLaserIntervalo): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: LPrecioImpresionLaserIntervalo): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;
      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
