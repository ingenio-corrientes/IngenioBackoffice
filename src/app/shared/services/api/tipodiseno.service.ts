import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class TipoDiseno {
  public id: number;
  public tipodiseno: string;
  constructor() {
    this.id = -1;
  }
}

@Injectable()
export class TipoDisenoService extends BasicService {
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'tipodiseno';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<TipoDiseno[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as TipoDiseno[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<TipoDiseno> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as TipoDiseno;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: TipoDiseno): Promise<any> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: TipoDiseno): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: TipoDiseno): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: TipoDiseno): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response =>  {
        if (+response.status === this.ENTITY_DELETED) {
          return this.ENTITY_DELETED;
        } else if (+response.status === this.ERROR_FOREING_KEY) {
          return this.ERROR_FOREING_KEY;
        } else {
          return this.ERROR_UNKNOWN;
        }
      })
      .catch(this.handleError);
}

  // private handleError(error: any): Promise<any> {
  //   console.error('An error occurred', error);
  //   return Promise.reject(error.message || error);
  // }
}
