import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { BasicService } from '../basic.service';

import 'rxjs/add/operator/toPromise';

export class Cheque {
  public id: number;
  public origen: number;
  public idbanco: number;
  public banco: string;
  public tenedororiginal: string;
  public emisor: string;
  public numero: string;
  public fecemision: number;
  public fecvencimiento: number;
  public monto: number;
  public depositado: number;
  public cobrado: number;
  public origenstr: number;
  public entrega: string;
  public tenedoractual: string;
}

@Injectable()
export class ChequeService extends BasicService  {
  public readonly ORIGEN_RECIBO = 0;
  public readonly ORIGEN_GASTO = 1;
  readonly GET_ENCARTERA = 'encartera';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    super();
    this.apiUrl = environment.apiUrl + 'cheque';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Cheque[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Cheque[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Cheque> {
    const customUrl = encodeURI(`${this.apiUrl}/${id}`);
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Cheque;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getEnCartera(): Promise<Cheque[]> {
    const customUrl = encodeURI(`${this.apiUrl}/${this.GET_ENCARTERA}`);
   return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Cheque[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Cheque): Promise<Cheque> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Cheque): Promise<Cheque> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Cheque): Promise<Cheque> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity))
      .toPromise()
      .then(response => {
          const body = response.json();
          return body.status === 'success';
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Cheque): Promise<Response> {
    const customUrl = `${this.apiUrl}/${entity.id}`;

    return this.http
        .delete(customUrl, { headers: this.headers })
        .toPromise()
        .then(response =>  {
          if (+response.status === this.ENTITY_DELETED) {
            return this.ENTITY_DELETED;
          } else if (+response.status === this.ERROR_FOREING_KEY) {
            return this.ERROR_FOREING_KEY;
          } else {
            return this.ERROR_UNKNOWN;
          }
        })
        .catch(this.handleError);
  }
}
