import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class RegistroCaja {
  public id: number;
  public fecinicio: string;
  public efectivoanterior: number;
  public efectivoinicio: number;
  public fecarqueo: string;
  public efectivoarqueo: number;

  constructor() {
    this.id = -1;
    this.efectivoanterior = 0;
    this.efectivoinicio = 0;
    this.efectivoarqueo = 0;
    this.fecarqueo = '';
    this.fecinicio = '';
  }
}

export class ChequeEnCartera {
  public id: number;
  public idbanco: number;
  public banco: string;
  public numero: number;
  public fecven: string;
  public monto: number;
  public depositado: number;
  public fecultmodif: string;
}

export class Totales {
  public efectivo: number;
  public total: number;
  public totalretiros: number;
}

export class FormaCobroPago {
  public forma: string;
  public total: number;
  public tipo: number;
}

export class RetirosEfectivo {
  public id: number;
  public responsable: string;
  public concepto: string;
  public monto: number;
}

export class InformeGeneralCaja {
  public totalingreso: number;
  public totalegreso: number;
}

@Injectable()
export class RegistroCajaService {
  readonly GET_REGISTRO = 'registro';
  readonly GET_CHEQUESENCARTERA = 'chequesencartera';
  readonly GET_TOTALES = 'totales';
  readonly GET_FORMASCOBROPAGO = 'formascobropago';
  readonly GET_RETIROSEFECTIVO = 'retirosefectivos';
  readonly API_ACTION = 'registrocaja';
  readonly GET_INFORMEGENERAL = 'informegeneral';
  readonly GET_INFORMECOBROPAGO = 'informecobropago';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'registrocaja';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    // this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<RegistroCaja[]> {
    const customUrl = `${this.apiUrl}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as RegistroCaja[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<RegistroCaja> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as RegistroCaja;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getRegistro(): Promise<RegistroCaja> {
    const customUrl = `${this.apiUrl}/${this.GET_REGISTRO}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as RegistroCaja;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getChequesEnCartera(): Promise<ChequeEnCartera[]> {
    const customUrl = `${this.apiUrl}/${this.GET_CHEQUESENCARTERA}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as ChequeEnCartera[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getTotales(): Promise<Totales> {
    const customUrl = `${this.apiUrl}/${this.GET_TOTALES}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Totales;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getFormasCobroPagos(): Promise<FormaCobroPago[]> {
    const customUrl = `${this.apiUrl}/${this.GET_FORMASCOBROPAGO}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as FormaCobroPago[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getRetirosEfectivos(): Promise<RetirosEfectivo[]> {
    const customUrl = `${this.apiUrl}/${this.GET_RETIROSEFECTIVO}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as RetirosEfectivo[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getInformeGeneral(fecDesde: string, fecHasta: string): Promise<InformeGeneralCaja> {
    const customUrl = `${this.apiUrl}/${this.GET_INFORMEGENERAL}/${fecDesde}/${fecHasta}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as InformeGeneralCaja;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getInformeCobroPagos(fecDesde: string, fecHasta: string): Promise<FormaCobroPago[]> {
    const customUrl = `${this.apiUrl}/${this.GET_INFORMECOBROPAGO}/${fecDesde}/${fecHasta}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body || {} as FormaCobroPago[];
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: RegistroCaja): Promise<any> {
    if (entity.id && entity.id !== -1) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: RegistroCaja): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: RegistroCaja): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else {
          return -1;
        }
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: RegistroCaja): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;
      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              return response.ok;
          })
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
