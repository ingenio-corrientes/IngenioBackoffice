import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ModalMessageComponent } from './modal-message.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations: [ModalMessageComponent],
    exports: [ModalMessageComponent]
})
export class ModalMessageModule { }
