export class AppSettings {
    public static TiposServicios = {
        caterleria: 1,
        diseno: 2,
        impresionlaser: 3,
        papeleria: 4
    };
    public static EstadosPresupuesto = {
        nuevo: -1,
        borrador: 0,
        emitido: 1,
        en_produccion: 2,
        entegado: 3
    };
    public static EstadosRecibo = {
        nuevo: -1,
        borrador: 0,
        emitido: 1
    };
    public static EventType = {
        new: 0,
        update: 1,
        delete: -1,
        cancel: 2
    };
    public static FormaCobro = {
        cheque: 0,
        efectivo: 1,
    };
}
