import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { environment } from '../../environments/environment';
// import { VERSION } from 'environments';
import { UsuarioService } from '../shared/services';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()],
    providers: [UsuarioService]
})
export class LoginComponent implements OnInit {
    public error = false;
    loading = false;
    public appVersion;

    constructor(public router: Router,
        private usuarioService: UsuarioService) {
        this.appVersion = environment.version;
    }

    ngOnInit() {
        localStorage.clear();
    }

    onLoggedin(user, pass) {
        this.loading = true;
        this.usuarioService.autenticate(user, pass).then(
            response => {
                if (response) {
                    localStorage.setItem('isLoggedin', 'true');
                    if (pass === environment.DEFAULT_PASS) {
                        const jsonData = JSON.parse(localStorage.getItem('userLog'));
                        localStorage.setItem('isLoggedDefault', 'true');
                        this.router.navigate(['/perfil', jsonData.id]);
                    } else {
                        localStorage.setItem('isLoggedDefault', 'false');
                        this.router.navigate(['/dashboard']);
                        localStorage.setItem('isLoggedDefault', 'false');
                    }
                    this.error = false;
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch( e =>
            this.handleError(e));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
