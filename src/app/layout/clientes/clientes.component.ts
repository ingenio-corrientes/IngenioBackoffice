import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Cliente, ClienteService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './clientes.component.html',
    styleUrls: ['./clientes.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        ClienteService,
    ]
})

export class ClientesComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            clientetipo: { title: 'Tipo de Cliente', filter: false, },
            razonsocial: { title: 'Razón Social', filter: false, },
            cuit: { title: 'CUIT', filter: false, },
            telefono: { title: 'Celular', filter: false, },
            emitido: { title: 'Emitidos', filter: false, },
            produccion: { title: 'En Producción', filter: false, },
            borrador: { title: 'Borrador', filter: false, },
            fecultmodif: {
                title: 'Último Movimiento', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public modalTitle = 'Clientes';
    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Cliente;
    public data: Array<any> = Array<any>();

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private datePipe: DatePipe,
        private clienteService: ClienteService) {
        this.isLoading = true;
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.clienteService.getList()
        .then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.source.setSort([{ field: 'razonsocial', direction: 'asc' }]);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de clientes!!', false);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/cliente', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'razonsocial', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/cliente', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/clientes']);
                        } else {
                            this.router.navigate(['/clientes']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
