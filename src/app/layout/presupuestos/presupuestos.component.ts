import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { SessionStore } from '../../shared';
import { Presupuesto, PresupuestoService, TWUtils } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './presupuestos.component.html',
    styleUrls: ['./presupuestos.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        SessionStore,
        PresupuestoService,
    ]
})

export class PresupuestosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Número', filter: false, },
            razonsocial: { title: 'Cliente', filter: false, },
            estado: { title: 'Estado', filter: false, },
            fecemision: {
                title: 'Fecha de Emisión', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return (!TWUtils.isNullDate(value))
                        ? '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>'
                        : '<div class="cell_center"> No Emitido </div>';
                }
            },
            fecentrega: {
                title: 'Fecha de Entrega', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return (!TWUtils.isNullDate(value))
                        ? '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>'
                        : '<div class="cell_center"> No Entregado </div>';
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'Presupuestos';

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Presupuesto;
    public data: Array<any> = Array<any>();
    searchkey: string;
    showEntregados = false;

    isLoading = false;

    public constructor(public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private datePipe: DatePipe,
        private sessionStore: SessionStore,
        private presupuestoService: PresupuestoService) {
            this.isLoading = true;
            this.sessionStore.init('presupuestos');
            this.searchkey = this.sessionStore.getKey('searchkey');
            this.source = new LocalDataSource(this.data);
            this.showEntregados = true;
    }

    public ngOnInit(): void {
        this.presupuestoService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.onCheck();
                    this.source.setSort([{ field: 'fecultmodif', direction: 'asc' }]);
                    if (this.searchkey.length > 0) {
                        this.onSearch(this.searchkey);
                    }
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de presupuestos!!', true);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    onCheck() {
        this.showEntregados = !this.showEntregados;
        if (this.showEntregados) {
            this.source = new LocalDataSource(this.data);
        } else {
            const dataFilt = this.data.filter(item =>
                TWUtils.isNullDate(item.fecentrega) || item.saldo > 0);
            this.source = new LocalDataSource(dataFilt);
        }
    }

    nuevo() {
        this.router.navigate(['/presupuesto', -1]);
    }

    onSearch(query: string) {
        this.searchkey = query;
        this.sessionStore.setKey('searchkey', this.searchkey);
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query, },
            { field: 'razonsocial', search: query, },
            { field: 'estado', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/presupuesto', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuestos']);
                        } else {
                            this.router.navigate(['/presupuestos']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
