import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Usuario, ChangePass, UsuarioService,
    TWUtils } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './perfil.component.html',
    styleUrls: ['./perfil.component.scss'],
    animations: [routerTransition()],
    providers: [UsuarioService]
})

export class PerfilComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Perfil';

    public selectedID: number;
    public selectedEntity: Usuario;
    public changePass: ChangePass;

    defaultLog = false;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private usuarioService: UsuarioService) {
        this.isLoading = true;
        this.selectedEntity = new Usuario();
        this.defaultLog = (localStorage.getItem('isLoggedDefault') === 'true');
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
            this.changePass = new ChangePass();
        });
    }

    public getEntityData(): any {
        if (this.selectedID > -1) {
            this.usuarioService.getByID(this.selectedID)
            .then(
                response => {
                    this.selectedEntity = response as Usuario;
                    this.changePass.id = this.selectedEntity.id;
                    this.changePass.contrasena = this.selectedEntity.contrasena;
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        if (this.changePass.newcontrasena.length === 0 || this.changePass.repitcontrasena.length === 0) {
            this.showMessage('La contraseña no puede ser vacía', false);
            return;
        }
        if (this.changePass.newcontrasena !== this.changePass.repitcontrasena) {
            this.showMessage('La contraseñas no pueden ser distintas', false);
            return;
        }
        if (this.changePass.newcontrasena === this.changePass.contrasena) {
            this.showMessage('La contraseñas no pueden ser igual a la anterior', false);
            return;
        }
        if (this.changePass.newcontrasena === environment.DEFAULT_PASS ||
            this.changePass.newcontrasena.toLocaleLowerCase() === environment.DEFAULT_PASS.toLocaleLowerCase()) {
            this.showMessage('La contraseña no puede ser igual a la contraseña por defecto', true);
            return;
        }

        this.usuarioService.changePass(this.changePass).then(
            response => {
                if (response) {
                    localStorage.setItem('isLoggedDefault', 'false');
                    if (this.selectedEntity.contrasena === environment.DEFAULT_PASS ||
                        this.selectedEntity.contrasena.toLocaleLowerCase() === environment.DEFAULT_PASS.toLocaleLowerCase()) {
                            this.showMessage('El contraseña se cambió correctamente', true);
                    } else {
                        this.showMessage('El contraseña se cambió correctamente', false);
                    }
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/usuarios']);
                        } else {
                            this.router.navigate(['/usuarios', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
