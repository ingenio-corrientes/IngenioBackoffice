import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubtipoPapeleriaComercialComponent } from './subtipopapeleriacomercial.component';

const routes: Routes = [
    { path: '', component: SubtipoPapeleriaComercialComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SubtipoPapeleriaComercialRoutingModule { }
