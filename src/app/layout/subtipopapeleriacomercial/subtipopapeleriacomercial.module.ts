import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../shared';

import { SubtipoPapeleriaComercialRoutingModule } from './subtipopapeleriacomercial-routing.module';
import { SubtipoPapeleriaComercialComponent } from './subtipopapeleriacomercial.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        SubtipoPapeleriaComercialRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [SubtipoPapeleriaComercialComponent]
})
export class SubtipoPapeleriaComercialModule { }
