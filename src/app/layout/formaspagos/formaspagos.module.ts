import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { FormasPagosRoutingModule } from './formaspagos-routing.module';
import { FormasPagosComponent } from './formaspagos.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        FormasPagosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule,
        Ng2SmartTableModule
    ],
    declarations: [FormasPagosComponent]
})
export class FormasPagosModule { }
