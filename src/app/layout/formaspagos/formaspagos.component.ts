import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { FormaPago, FormaPagoService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './formaspagos.component.html',
    styleUrls: ['./formaspagos.component.scss'],
    animations: [routerTransition()],
    providers: [
        FormaPagoService,
    ]
})

export class FormasPagosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Código', filter: false, },
            formapago: { title: 'Forma de Pago', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'Presupuesto';

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: FormaPago;
    public data: Array<any> = Array<any>();

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private formaPagoService: FormaPagoService) {
            this.source = new LocalDataSource(this.data);
            this.isLoading = true;
    }

    public ngOnInit(): void {
        this.formaPagoService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de formas de pagos!!', false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/formapago', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query, },
            { field: 'formapago', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/formapago', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/formaspagos']);
                        } else {
                            this.router.navigate(['/formaspagos']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
