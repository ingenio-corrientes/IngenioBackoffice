import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { ClienteService, CurrencyAR } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './cuentascorrientes.component.html',
    styleUrls: ['./cuentascorrientes.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        ClienteService,
        CurrencyAR
    ]
})

export class CuentasCorrientesComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            razonsocial: { title: 'Cliente', filter: false, },
            idpresupuesto: { title: 'Presupuesto', filter: false, },
            fecentrega: {
                title: 'Fecha de Entrega', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    if (value) {
                        return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                    } else {
                        return '';
                    }
                }
            },
            saldo: {
                title: 'Saldo',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_right">' + this.cp.transform(value) + '</div>';
                }
            },
            monto: {
                title: 'Monto',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return  '<div class="cell_right">' + this.cp.transform(value) + '</div>';
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'CuentasCorrientes';

    public dataClientes: Array<any> = Array<any>();
    public idCliente: number;
    public data: Array<any> = Array<any>();

    totalcuenta = '';
    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private datePipe: DatePipe,
        private cp: CurrencyAR,
        private clienteService:  ClienteService) {
        this.isLoading = true;
        this.source = new LocalDataSource(this.data);
        this.idCliente = 0;
    }

    public ngOnInit(): void {
        this.getClientes();
    }

    private getClientes() {
        this.clienteService.getList().then(
            response => {
                if (response) {
                    this.dataClientes = response;
                    this.isLoading = false;
                } else {
                    alert('No se han podido cargar datos de clientes!!');
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onGenerar() {
        this.isLoading = true;
        this.clienteService.getCuentaCorriente(this.idCliente)
        .then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.totalcuenta = this.cp.transform(0);
                    let monto = 0;
                    this.data.forEach(presup => {
                        monto += +presup.saldo;
                    });
                    this.totalcuenta = this.cp.transform(monto);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de cuentascorrientes!!', false);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'razonsocial', search: query, },
            { field: 'idpresupuesto', search: query, },
            { field: 'fecentrega', search: query, },
            { field: 'saldo', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/presupuesto', event.data.idpresupuesto]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/cuentascorrientes']);
                        } else {
                            this.router.navigate(['/cuentascorrientes']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
