import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { CuentasCorrientesRoutingModule } from './cuentascorrientes-routing.module';
import { CuentasCorrientesComponent } from './cuentascorrientes.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        CuentasCorrientesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule,
        Ng2SmartTableModule
    ],
    declarations: [CuentasCorrientesComponent]
})
export class CuentasCorrientesModule { }
