import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuentasCorrientesComponent } from './cuentascorrientes.component';

const routes: Routes = [
    { path: '', component: CuentasCorrientesComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CuentasCorrientesRoutingModule { }
