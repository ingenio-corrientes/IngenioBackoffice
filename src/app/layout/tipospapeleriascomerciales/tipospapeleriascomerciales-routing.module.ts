import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TiposPapeleriasComercialesComponent } from './tipospapeleriascomerciales.component';

const routes: Routes = [
    { path: '', component: TiposPapeleriasComercialesComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TiposPapeleriasComercialesRoutingModule { }
