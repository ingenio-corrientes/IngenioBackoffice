import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { TiposPapeleriasComercialesRoutingModule } from './tipospapeleriascomerciales-routing.module';
import { TiposPapeleriasComercialesComponent } from './tipospapeleriascomerciales.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        TiposPapeleriasComercialesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [TiposPapeleriasComercialesComponent]
})
export class TiposPapeleriasComercialesModule { }
