import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReciboComponent } from './recibo.component';

const routes: Routes = [
    { path: '', component: ReciboComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ReciboRoutingModule { }
