import { Component, OnInit, Input, SimpleChange, ViewContainerRef, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { CurrencyAR, ReciboItem, ReciboItemService } from '../../../../shared/';

@Component({
    selector: 'app-reciboitems',
    templateUrl: './reciboitems.component.html',
    styleUrls: ['./reciboitems.component.scss'],
    providers: [
        ReciboItemService,
        CurrencyAR
    ]
})

export class ReciboItemsComponent implements OnInit {
    @Output() reciboUpdated = new EventEmitter();
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            formacobro: { title: 'Forma de cobro', filter: false, },
            monto: {
                title: 'Monto',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    modalTitle = 'Item';

    public data: Array<any> = Array<any>();
    public selectedItem: ReciboItem = null;
    public selectedIDRecibo: number;
    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private cp: CurrencyAR,
        private reciboItemService: ReciboItemService) {
        this.isLoading = true;
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDRecibo = params['id'];
            this.getData();
        });
    }

    handleItemUpdated(actualizar) {
        // if (!actualizar) {
        //     this.selectedItem = new ReciboItem();
        // }
        this.getData();
        this.reciboUpdated.emit();
        this.selectedItem = null;
    }

    public getData() {
        if (this.selectedIDRecibo !== -1) {
            this.reciboItemService.getListByIDRecibo(this.selectedIDRecibo).then(
                response => {
                    if (response) {
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                    } else {
                        this.showMessage('No se ha podido guardar el ítem!!', false);
                    }
                    this.isLoading = false;
                }
            ).catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onCellClick(data: any): any {
        this.selectedItem = new ReciboItem();
        this.selectedItem.id = 0;
        this.selectedItem = data.row as ReciboItem;
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'formacobro', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data as ReciboItem;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
