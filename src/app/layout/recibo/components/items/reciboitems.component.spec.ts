import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ReciboItemsComponent } from './reciboitems.component';

describe('ReciboItemsComponent', () => {
  let component: ReciboItemsComponent;
  let fixture: ComponentFixture<ReciboItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot()
    ],
      declarations: [ ReciboItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReciboItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
