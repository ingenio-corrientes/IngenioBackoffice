import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Recibo, ReciboService,
    ReciboItemService, ClienteService,
    PresupuestoService,
    CurrencyAR,
    TWUtils,
    Presupuesto,
    Cliente} from '../../../../shared';

@Component({
    selector: 'app-recibodata',
    templateUrl: './recibo-data.component.html',
    styleUrls: ['./recibo-data.component.scss'],
    providers: [
        ReciboService,
        ReciboItemService,
        ClienteService,
        PresupuestoService,
        CurrencyAR ]
})

export class ReciboDataComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Recibo';

    private dataClientes: Array<any> = Array<any>();
    private dataPresupuestos: Array<any> = Array<any>();
    private dataReciboItems: Array<any> = Array<any>();

    public predefinedIDPresupuesto: number;
    public selectedEntity: Recibo;
    public selectedID: number;
    public fechaRecibo: {};

    isLoading = false;
    montopresupuesto: string;
    saldopresupuesto: string;
    fromPresupuesto: boolean;
    montoRecibo: string;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private cp: CurrencyAR,
        private reciboService: ReciboService,
        private reciboItemService: ReciboItemService,
        private clienteService: ClienteService,
        private presupuestoService: PresupuestoService) {
        this.isLoading = true;
        this.fromPresupuesto = false;
        this.selectedEntity = new Recibo();
        this.selectedEntity.fecha = (new Date()).toISOString();
        this.fechaRecibo = TWUtils.stringDateToJson(this.selectedEntity.fecha);
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            if (this.selectedID > -1) {
                this.loadRecibo();
                return;
            }

            if (params['idpresupuesto']) {
                this.fromPresupuesto = true;
                this.predefinedIDPresupuesto = +params['idpresupuesto'];
                this.getDataFP();
            } else {
                // queda
                this.getClientes(false);
            }
        });
    }

    // queda
    loadRecibo() {
        this.reciboService.getByID(this.selectedID).then(
            response => {
                if (response) {
                    this.selectedEntity = response as Recibo;
                    this.fechaRecibo = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                    this.getClientes(this.selectedEntity.idcliente);
                    this.getPresupuesto(this.selectedEntity.idpresupuesto);
                    this.montoRecibo = this.cp.transform(this.selectedEntity.monto);
                    this.isLoading = false;
                }
            }
        )
    }

    // queda
    getPresupuesto(idPresupuesto) {
        this.presupuestoService.getByID(idPresupuesto).then(
                response => {
                    if (response) {
                        const presupuesto = response as Presupuesto;
                        this.dataPresupuestos.push(presupuesto);
                        this.montopresupuesto = this.cp.transform(this.dataPresupuestos[0].total);
                        this.saldopresupuesto = this.cp.transform(this.dataPresupuestos[0].saldo);
                    } else {
                        this.showMessage('No se han podido obtener datos de clientes!!', false, false);
                    }
                }
            ).catch(e => this.handleError(e));
    }

    // queda
    public getDataFP() {
        this.presupuestoService.getByID(this.predefinedIDPresupuesto).then(
                response => {
                    if (response) {
                        const presupuesto = response as Presupuesto;
                        this.dataPresupuestos.push(presupuesto);
                        this.montopresupuesto = this.cp.transform(this.dataPresupuestos[0].total);
                        this.saldopresupuesto = this.cp.transform(this.dataPresupuestos[0].saldo);
                        this.fechaRecibo = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                        if (!this.isEmited()) {
                            this.selectedEntity.idcliente = presupuesto.idcliente;
                            this.selectedEntity.idpresupuesto = this.predefinedIDPresupuesto;
                        }
                        // obtiene los datos del cliente del presupuesto
                        this.getClientes(this.selectedEntity.idcliente);
                    } else {
                        this.showMessage('No se han podido obtener datos de clientes!!', false, false);
                    }
                }
            ).catch(e => this.handleError(e));
    }

    // queda
    public getClientes(clientid) {
        if (clientid) {
            return this.clienteService.getByID(clientid).then(
                response => {
                    if (response) {
                        const cliente = response as Cliente;
                        this.dataClientes.push(cliente);
                        this.isLoading = false;
                    } else {
                        this.showMessage('No se han podido obtener datos del cliente!!', false, false);
                    }
                }
            ).catch(e => this.handleError(e));
        } else {
            return this.clienteService.getList().then(
                response => {
                    if (response) {
                        this.dataClientes = response;
                        this.isLoading = false;
                    } else {
                        this.showMessage('No se han podido obtener datos de clientes!!', false, false);
                    }
                }
            ).catch(e => this.handleError(e));
        }
    }

    obtenerPresupuestos() {
        if (typeof this.selectedEntity.idcliente === 'undefined') {
            return;
        }

        if (this.isEmited()) {
            this.getEmited();
        } else {
            this.getPendientes();
        }
    }

    public isEmited(): boolean {
        return !TWUtils.isNullDate(this.selectedEntity.fecemision);
    }

    getEmited() {
        this.presupuestoService.getByID(this.selectedEntity.idpresupuesto).then(
            response => {
                if (response) {
                    this.dataPresupuestos.push(response);
                    this.selectedEntity.idpresupuesto = this.dataPresupuestos[0].id;
                    this.selectedEntity.montopresupuesto = this.dataPresupuestos[0].total;
                    let saldo = 0;
                    this.dataReciboItems.forEach(item => {
                        saldo += +item.monto
                    });
                    saldo = (+this.selectedEntity.montopresupuesto) - (+saldo);
                    this.selectedEntity.saldopresupuesto = saldo;
                } else {
                    this.showMessage('No se han podido obtener datos de presupuestos!!', false, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    getPendientes() {
        this.presupuestoService.getPendientesByIdCliente(this.selectedEntity.idcliente).then(
            response => {
                if (response) {
                    this.dataPresupuestos = response;
                    if (this.dataPresupuestos.length === 0) {
                            this.showMessage('El cliente no posee prespuestos pendientes de cobro.', false, false);
                    } else {
                        this.selectedEntity.idpresupuesto = this.dataPresupuestos[0].id;
                        this.montopresupuesto = this.cp.transform(this.dataPresupuestos[0].total);
                        this.saldopresupuesto = this.cp.transform(this.dataPresupuestos[0].saldo);
                    }
                } else {
                    this.showMessage('No se han podido obtener datos de presupuestos!!', false, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public existClient() {
        return (this.selectedEntity.idcliente > 0);
    }

    public onSave() {
        this.selectedEntity.fecha = TWUtils.arrayDateToString(this.fechaRecibo);
        this.reciboService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.predefinedIDPresupuesto = this.selectedEntity.idpresupuesto;
                    this.showMessageStay('El recibo se guardó correctamente');
                } else {
                    this.showMessage('El recibo no se guardó!!', false, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public isDirty() {
        return (this.selectedEntity.fecemision.length > 0);
    }

    public eliminar() {
        this.reciboService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.reciboService.ENTITY_DELETED) {
                    this.showMessage('El recibo se ha eliminado correctamente', true, true);
                } else  if (+response === this.reciboService.ERROR_FOREING_KEY) {
                    this.showMessage('El recibo está siendo utilizado y no puede eliminarse.', false, true);
                } else {
                    this.showMessage('El recibo no se ha podido eliminar!', false, true);
                }
            }
        ).catch(e => this.handleError(e));
    }

    isFromPresupuesto() {
        return this.fromPresupuesto;
    }

    onPresupuestoChange() {
        const presupuesto = this.dataPresupuestos.find(
            item =>
            +item.id === +this.selectedEntity.idpresupuesto);

        this.selectedEntity.montopresupuesto = presupuesto.total;
        this.selectedEntity.saldopresupuesto = presupuesto.saldo;
        this.montopresupuesto = this.cp.transform(this.selectedEntity.montopresupuesto);
        this.saldopresupuesto = this.cp.transform(this.selectedEntity.saldopresupuesto);

    }

    public onDataChange() {
        this.obtenerPresupuestos();
    }

    showMessageStay(message: string) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        this.router.navigate(['/recibo', this.selectedID, this.predefinedIDPresupuesto]);
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    showMessage(message: string, goBack: boolean, process: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (!process) {
                            resolve();
                            return;
                        }
                        if (goBack) {
                            this.router.navigate(['/recibos']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
