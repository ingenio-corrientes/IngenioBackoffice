import { Component, OnInit, ViewContainerRef , Input, Output, OnChanges, EventEmitter, SimpleChange  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Recibo, ReciboService,
    ReciboItem, ReciboItemService,
    FormaCobroService, BancoService,
    CurrencyAR, TWUtils } from '../../../../shared/';

@Component({
    selector: 'app-item-carga',
    templateUrl: './item-carga.component.html',
    styleUrls: ['./item-carga.component.scss'],
    providers: [
        DatePipe,
        ReciboService,
        ReciboItemService,
        FormaCobroService,
        BancoService,
        CurrencyAR
    ]
})
export class ItemCargaComponent implements OnInit, OnChanges {
    @Input() selectedItem: ReciboItem;
    @Output() itemUpdated = new EventEmitter();

    public modalMessage: string;
    public modalTitle = 'Recibo';
    // variables de campos
    public fechaItem: {};
    public fecVencimientoItem: {};

    // datoa estructurados
    private recibo: Recibo;
    selectedEntity: ReciboItem;
    private selectedEntityAux: ReciboItem;
    private selectedIDRecibo: number;

    // arreglos
    public dataFormasCobros: Array<any> = Array<any>();
    public dataBancos: Array<any> = Array<any>();
    public dataReciboItems: Array<any> = Array<any>();

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private datePipe: DatePipe,
        private cp: CurrencyAR,
        private reciboService: ReciboService,
        private reciboItemService: ReciboItemService,
        private formaCobroService: FormaCobroService,
        private bancoService: BancoService) {
            this.isLoading = true;
            this.cancelar();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDRecibo = params['id'];
            this.getData();
        });
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue;
            if (this.selectedEntity.fecha !== '' && this.selectedEntity.fecha !== '0000-00-00') {
                this.fechaItem = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                this.fecVencimientoItem = TWUtils.stringDateToJson(this.selectedEntity.fecvencimiento);
                this.isLoading = false;
            } else {
                this.selectedEntity.fecha = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
                this.fechaItem = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                this.fecVencimientoItem = TWUtils.stringDateToJson(this.selectedEntity.fecvencimiento);
                this.isLoading = false;
            }
        }
    }

    public getData() {
        const p0 = this.formaCobroService.getList();
        const p1 = this.bancoService.getList();
        const p2 = this.reciboService.getByID(this.selectedIDRecibo);
        const p3  = this.reciboItemService.getListByIDRecibo(this.selectedIDRecibo);
        Promise.all([
            p0, p1, p2, p3
        ]).then(([result0, result1, result2, result3]) => {
            this.dataFormasCobros = result0;
            this.dataBancos = result1;
            this.recibo = result2;
            this.dataReciboItems = result3;
        }).catch(e => this.handleError(e));
    }

    onFormaCobroChange() {
        if (this.isNew() && +this.selectedEntity.monto === 0) {
            let saldo = 0.0;
            this.dataReciboItems.forEach(item => {
                saldo += +item.monto
            });
            if (+this.recibo.saldopresupuesto > 0) {
                saldo = +parseFloat((+this.recibo.saldopresupuesto - saldo) + '').toFixed(2);
                this.selectedEntity.monto = +saldo;
            }
        }
    }

    public isEmited(): boolean {
        const retval = (this.recibo !== undefined) ? !TWUtils.isNullDate(this.recibo.fecemision) : false;
        return retval;
    }

    onSubmit() {
        let montoRecibo = 0;
        this.dataReciboItems.forEach(item => {
            montoRecibo += +item.monto;
        })
        if (montoRecibo > +this.recibo.saldopresupuesto) {
            this.showMessage('El monto de la forma de cobro supera el saldo del presupuesto!!', false, false);
            return;
        }

        this.selectedEntity.idrecibo = this.selectedIDRecibo;
        if (!this.isCheque()) {
            this.selectedEntity.origendestino = '';
        } else {
            this.selectedEntity.origendestino = this.recibo.razonsocial;
        }
        if (this.isBancario()) {
            this.selectedEntity.fecha = TWUtils.arrayDateToString(this.fechaItem);
            this.selectedEntity.fecvencimiento = TWUtils.arrayDateToString(this.fecVencimientoItem);
        } else {
            this.selectedEntity.fecha = '';
            this.selectedEntity.fecvencimiento = '';
            this.selectedEntity.idbanco = -1;
            this.selectedEntity.numero = '';
            this.selectedEntity.librador = '';
        }
        this.reciboItemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.updateItemList();
                    this.itemUpdated.emit(true);
                    this.cancelar();
                } else {
                    this.showMessage('No se han podido guardar los datos del item de recibo!!', false, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    updateItemList() {
        this.reciboItemService.getListByIDRecibo(this.selectedIDRecibo).then(
            response => {
                if (response) {
                    this.dataReciboItems = response;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
      this.reciboItemService.delete(this.selectedEntity)
      .then(
            response => {
            if (response) {
                this.dataReciboItems = this.dataReciboItems.filter(item =>
                    item.id !== this.selectedEntity.id);
                this.itemUpdated.emit(true);
                this.cancelar();
            } else {
                this.showMessage('No se han podido eliminar los datos del item de recibo!!', false, false);
            }
        }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new ReciboItem();

        // this.selectedEntity.fecha = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        // this.fechaItem = TWUtils.stringDateToJson(this.selectedEntity.fecha);

        // this.selectedEntity.fecvencimiento = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        // this.fecVencimientoItem = TWUtils.stringDateToJson(this.selectedEntity.fecvencimiento);

        this.fechaItem = TWUtils.stringDateToJson(this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
        this.fecVencimientoItem = TWUtils.stringDateToJson(this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
        this.itemUpdated.emit(true);
    }

    isNew() {
        return (this.selectedEntity.id === -1);
    }

    public isBancario() {
        const formaCobro = this.dataFormasCobros.find(
            x =>
            // tslint:disable-next-line:triple-equals
            x.id == this.selectedEntity.idformacobro);
        // tslint:disable-next-line:triple-equals
        const retval = (typeof formaCobro !== 'undefined') ? formaCobro.bancario == 1 : false;
        return retval;
    }

    public isCheque() {
        const formaCobro = this.dataFormasCobros.find(
            x =>
            // tslint:disable-next-line:triple-equals
            x.id == this.selectedEntity.idformacobro);
        // tslint:disable-next-line:triple-equals
        const retval = (typeof formaCobro !== 'undefined') ? formaCobro.cheque == 1 : false;
        return retval;
    }

    isCero() {
        return +this.selectedEntity.monto === 0;
    }

    showMessage(message: string, goBack: boolean, action: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (action) {
                            if (goBack) {
                                this.router.navigate(['/recibo']);
                            } else {
                                this.router.navigate(['/recibo', this.selectedIDRecibo]);
                            }
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false, false);
        return Promise.reject(error.message || error);
    }
}
