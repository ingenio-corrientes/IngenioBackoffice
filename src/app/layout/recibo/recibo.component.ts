import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import { Recibo, ReciboService,
    ReciboItem, ReciboItemService,
    CurrencyAR,
    NgbDateCustomParserFormatter,
    FormaCobroService, AppSettings, TWUtils } from '../../shared';

import 'rxjs/add/observable/forkJoin';
import { GenRecibo } from 'app/shared/services/vouchers/gen-recibo';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-form',
    templateUrl: './recibo.component.html',
    styleUrls: ['./recibo.component.scss'],
    animations: [routerTransition()],
    providers: [
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        DatePipe,
        ReciboService,
        ReciboItemService,
        FormaCobroService]
})

export class ReciboComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Recibo';

    public error = false;
    public loading = false;
    public selectedID: number;
    public selectedEntity: Recibo;
    public predefinedIDPresupuesto: number;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private cp: CurrencyAR,
        private datePipe: DatePipe,
        private reciboService: ReciboService,
        private reciboItemService: ReciboItemService) {
        this.selectedEntity = new Recibo();
    }

    public ngOnInit(): void {
        this.loading = false;
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.predefinedIDPresupuesto = +params['idpresupuesto'];
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        if (this.selectedID > -1) {
            this.reciboService.getByID(this.selectedID).then(
                response => {
                    this.loading = false;
                    this.selectedEntity = response as Recibo;
                }
            ).catch(e => this.handleError(e));
        }
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public isDirty() {
        return (this.selectedEntity.fecemision.length > 0);
    }

    public getEstado() {
        if (this.selectedID === -1) {
            return AppSettings.EstadosRecibo.nuevo;
        }
        if (!(+this.selectedEntity.monto > 0)) {
            return AppSettings.EstadosRecibo.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            return AppSettings.EstadosRecibo.emitido;
        }
    }

    public volver() {
        if (this.predefinedIDPresupuesto > -1) {
            this.router.navigate(['/presupuesto', this.predefinedIDPresupuesto]);
        } else {
            this.router.navigate(['/recibos']);
        }
    }

    public emitir() {
        const p0 = this.reciboService.getByID(this.selectedID);
        const p1 = this.reciboItemService.getListByIDRecibo(this.selectedID);
        if (this.selectedID !== -1) {
            Promise.all([p0, p1])
            .then( ([entity, items]) => {
                this.selectedEntity = entity as Recibo;
                if (TWUtils.isNullDate(this.selectedEntity.fecemision)) {
                    this.selectedEntity.fecemision = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
                }
                this.emitirRecibo(items);
            });
        }
    }

    emitirRecibo(items: ReciboItem[]): any {
        if (items.length === 0) {
            this.showMessage('El recibo no posee ítems!', false);
            return;
        }
        this.registrarEmision(items);
    }

    registrarEmision(items: ReciboItem[]) {
        this.reciboService.emitir(this.selectedEntity).then(
            response => {
                if (+response === 200) {
                    // const presup = new GenRecibo(this.cp);
                    // presup.genRecibo(this.selectedEntity, items);
                    // this.onGetEntityData();
                    this.generarComprobante(items);
                    this.showMessage('Registrada la emisión correctamente.', false);
                } else if (+response === 1) {
                    if (!TWUtils.isNullDate(this.selectedEntity.fecemision)) {
                        // const presup = new GenRecibo(this.cp);
                        // presup.genRecibo(this.selectedEntity, items);
                        // this.onGetEntityData();
                        this.generarComprobante(items);
                        this.showMessage('El saldo actual del presupuesto es 0 por lo que no se puede emitir un nuevo recibo. Sólo se generará una copia.', false);
                    } else {
                        this.showMessage('El saldo actual del presupuesto es 0 por lo que no se puede emitir el recibo.', false);
                    }
                } else if (+response === 2) {
                    this.showMessage('El saldo actual del presupuesto es es inferior a lo que se intente cobrar.', false);
                } else if (+response === 3) {
                    this.showMessage('El se registraron formas de cobro.', false);
                } else {
                    this.showMessage('No se pudo registrar los datos del recibo por error en el servidor!', false);
                }
            }
        )
    }

    generarComprobante(items: ReciboItem[]) {
        this.loading = true;
        this.reciboService.getByID(this.selectedID).then(
            response => {
                this.selectedEntity = response as Recibo;
                const presup = new GenRecibo(this.cp);
                presup.genRecibo(this.selectedEntity, items);
                this.loading = false;
            }
        ).catch(e => this.handleError(e));
    }

    handleReciboUpdated() {
        this.onGetEntityData();
    }

    public eliminar() {
        console.log('eliminar');
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuestos']);
                        } else {
                            this.router.navigate(['/recibo', this.selectedEntity.id, -1]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        // console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
