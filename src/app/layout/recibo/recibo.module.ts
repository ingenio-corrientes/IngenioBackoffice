import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { ReciboRoutingModule } from './recibo-routing.module';
import { ReciboComponent } from './recibo.component';
import {
  ReciboDataComponent,
  ReciboItemsComponent,
  ItemCargaComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        ReciboRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      ReciboComponent,
      ReciboDataComponent,
      ReciboItemsComponent,
      ItemCargaComponent
    ]
})
export class ReciboModule { }
