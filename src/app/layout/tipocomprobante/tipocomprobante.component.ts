import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { TipoComprobante, TipoComprobanteService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './tipocomprobante.component.html',
    styleUrls: ['./tipocomprobante.component.scss'],
    animations: [routerTransition()],
    providers: [TipoComprobanteService]
})

export class TipoComprobanteComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'TipoComprobante';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: TipoComprobante;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private tipocomprobanteService: TipoComprobanteService) {
        this.selectedEntity = new TipoComprobante();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = +params['id'];
            this.getEntityData();
            this.loading = false;
        });
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.tipocomprobanteService.getByID(this.selectedID)
          .then(
              response => {
                  this.loading = false;
                  this.selectedEntity = response as TipoComprobante;
              })
          .catch(e => this.handleError(e));
      }
    }

    public onSave() {
        this.tipocomprobanteService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.selectedID = Number(response);
                    this.showMessage('El tipo de comprobante se guardó correctamente', true);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.tipocomprobanteService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.tipocomprobanteService.ENTITY_DELETED) {
                    this.showMessage('El tipo de comprobante se ha eliminado correctamente', true);
                } else  if (+response === this.tipocomprobanteService.ERROR_FOREING_KEY) {
                    this.showMessage('El tipo de comprobante está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El tipo de comprobante no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/tiposcomprobantes']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/tiposcomprobantes']);
                        } else {
                            this.router.navigate(['/tiposcomprobantes', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
