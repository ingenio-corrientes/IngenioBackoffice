import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoComprobanteComponent } from './tipocomprobante.component';

const routes: Routes = [
    { path: '', component: TipoComprobanteComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TipoComprobanteRoutingModule { }
