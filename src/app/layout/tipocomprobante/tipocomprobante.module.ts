import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { TipoComprobanteRoutingModule } from './tipocomprobante-routing.module';
import { TipoComprobanteComponent } from './tipocomprobante.component';
import { PageHeaderModule } from './../../shared';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        TipoComprobanteRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [TipoComprobanteComponent]
})
export class TipoComprobanteModule { }
