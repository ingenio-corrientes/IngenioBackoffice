import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LPrecioAdCarteleriaComponent } from './lprecioadcarteleria.component';
// import {
//   DataComponent,
//   ItemsComponent
// } from './components/';

import { PageHeaderModule } from './../../shared';

describe('LPrecioAdCarteleriaComponent', () => {
  let component: LPrecioAdCarteleriaComponent;
  let fixture: ComponentFixture<LPrecioAdCarteleriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        LPrecioAdCarteleriaComponent,
        // DataComponent,
        // ItemsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LPrecioAdCarteleriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
