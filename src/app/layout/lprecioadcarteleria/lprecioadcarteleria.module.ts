import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { LPrecioAdCarteleriaRoutingModule } from './lprecioadcarteleria-routing.module';
import { LPrecioAdCarteleriaComponent } from './lprecioadcarteleria.component';
import {
  ItemComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        LPrecioAdCarteleriaRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      LPrecioAdCarteleriaComponent,
      ItemComponent
    ]
})
export class LPrecioAdCarteleriaModule { }
