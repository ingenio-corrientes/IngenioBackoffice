import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LPrecioAdCarteleriaComponent } from './lprecioadcarteleria.component';

const routes: Routes = [
    { path: '', component: LPrecioAdCarteleriaComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LPrecioAdCarteleriaRoutingModule { }
