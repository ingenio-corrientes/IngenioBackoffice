import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../shared';

import { TamanoRoutingModule } from './tamano-routing.module';
import { TamanoComponent } from './tamano.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        TamanoRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [TamanoComponent]
})
export class TamanoModule { }
