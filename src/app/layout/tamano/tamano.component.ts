import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Tamano, TamanoService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './tamano.component.html',
    styleUrls: ['./tamano.component.scss'],
    animations: [routerTransition()],
    providers: [TamanoService]
})

export class TamanoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Tamaño';

    public selectedID: number;
    public selectedEntity: Tamano;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private entityService: TamanoService) {
        this.selectedEntity = new Tamano();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.entityService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as Tamano;
              })
          .catch(e => this.handleError(e));
      }
    }

    public onSave() {
        this.entityService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El tamaño se guardó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido obtener los datos del tamaño!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.entityService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.entityService.ENTITY_DELETED) {
                    this.showMessage('El tamaño se ha eliminado correctamente', true);
                } else  if (+response === this.entityService.ERROR_FOREING_KEY) {
                    this.showMessage('El tamaño está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El tamaño no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/tamanos']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/tamanos']);
                        } else {
                            this.router.navigate(['/tamano', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
