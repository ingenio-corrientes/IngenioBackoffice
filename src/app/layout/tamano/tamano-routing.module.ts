import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TamanoComponent } from './tamano.component';

const routes: Routes = [
    { path: '', component: TamanoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TamanoRoutingModule { }
