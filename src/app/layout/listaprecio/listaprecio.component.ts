import { Component, OnInit, ViewContainerRef  } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    ModuloXRolService,
    ListaPrecio,
    ListaPrecioService,
    ClienteTipoService,
    AppSettings } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './listaprecio.component.html',
    styleUrls: ['./listaprecio.component.scss'],
    animations: [routerTransition()],
    providers: [
        ListaPrecioService,
        ClienteTipoService,
        ModuloXRolService
    ]
})

export class ListaPrecioComponent implements OnInit {
    readonly IDMODULO = 8;
    readonly MESSAGE_STAYHERE = 'listaprecio';
    readonly MESSAGE_GOTO = 'listasprecios';
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Lista de Precio';

    public selectedID: number;
    public selectedEntity: ListaPrecio;

    // arreglos
    public dataClientesTipos: Array<any> = Array<any>();
    public dataListasBases: Array<any> = Array<any>();
    public dataListasBasesFilt: Array<any> = Array<any>();
    loggedId: number;

    initialized = false;
    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private entityService: ListaPrecioService,
        private listasService: ListaPrecioService,
        private clienteTipoService: ClienteTipoService,
        private moduloXRolService: ModuloXRolService) {
            this.isLoading = true;
            this.selectedEntity = new ListaPrecio();
    }

    public ngOnInit(): void {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedId = +jsonData.id;
        this.evalPermisos();
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getData();
        });
    }

    evalPermisos() {
        this.moduloXRolService.getListByIDUsuarioIDModulo(this.loggedId, this.IDMODULO)
            .then(response => {
                if (response) {
                    if (+response.ver !== 1) {
                        this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    }
                    this.isLoading = false;
                } else {
                    this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    public getData() {
        const p1 = this.clienteTipoService.getList();
        const p2 = this.listasService.getList();
        Promise.all([
            p1, p2
        ]).then(([result1, result2]) => {
            this.dataClientesTipos = result1;
            this.dataListasBases = result2;
            this.getEntityData()
        });
    }

    public getEntityData(): any {
        if (this.selectedID > -1) {
            this.entityService.getByID(this.selectedID)
            .then(
                response => {
                    this.selectedEntity = response as ListaPrecio;
                    this.initialized = true;
                    this.onChange(true);
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
        } else {
            this.initialized = true;
            this.isLoading = false;
        }
    }

    public onSave() {
        this.entityService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('La lista de precios se guardó correctamente!!', false, false);
                } else {
                    this.showMessage('No se han podido obtener los datos de la lista de precios!!', false, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.entityService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.entityService.ENTITY_DELETED) {
                    this.showMessage('la lista de precios se ha eliminado correctamente', true, false);
                } else  if (+response === this.entityService.ERROR_FOREING_KEY) {
                    this.showMessage('La lista de precios está siendo utilizado y no puede eliminarse.', false, false);
                } else {
                    this.showMessage('La lista de precios no se ha podido eliminar!', false, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public applyPorcentaje() {
        if (this.selectedEntity.id < 1) {
            this.showMessage(
            'Primero debe guardar la lista para que se genere y poder aplicar porcentajes o montos!!', false, false);
            return;
        }
        this.entityService.apply(this.selectedEntity, ListaPrecioService.APLICAR_PORCENTAJE).then(
            response => {
                if (response) {
                    this.showMessage(
                        'Se aplicó correctamente el porcentaje sobre todos los precios de los ítems!!', false, false);
                } else {
                    this.showMessage('No se han podido eliminar la lista de precios!!', false, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public applyMonto() {
        if (this.selectedEntity.id < 1) {
            this.showMessage(
            'Primero debe guardar la lista para que se genere y poder aplicar porcentajes o montos!!', false, false);
            return;
        }
        this.entityService.apply(this.selectedEntity, ListaPrecioService.APLICAR_MONTO).then(
            response => {
                if (response) {
                    this.showMessage(
                        'Se aplicó correctamente el monto sobre todos los precios de los ítems!!', false, false);
                } else {
                    this.showMessage('No se han podido eliminar la lista de precios!!', false, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public hasAdicionales() {
        return (this.selectedEntity.idtiposervicio === AppSettings.TiposServicios.caterleria
            || this.selectedEntity.idtiposervicio === AppSettings.TiposServicios.papeleria
            || this.selectedEntity.idtiposervicio === AppSettings.TiposServicios.impresionlaser);
    }

    public onChange(init: boolean) {
        if (!this.initialized) {
            return;
        }
        const idtiposervicio = this.selectedEntity.idtiposervicio;
        this.dataListasBasesFilt = this.dataListasBases.filter(data =>
            +data.idtiposervicio === +idtiposervicio);
        if (!init) {
            this.selectedEntity.idlistabase = -1;
        }
    }

    public volver() {
        this.router.navigate([this.MESSAGE_GOTO]);
    }

    public gotoItems() {
        switch (this.selectedEntity.idtiposervicio) {
            case AppSettings.TiposServicios.caterleria:
                this.router.navigate(['/lpreciocarteleria', this.selectedID]);
                break;
            case AppSettings.TiposServicios.diseno:
            this.router.navigate(['/lpreciodiseno', this.selectedID]);
                break;
            case AppSettings.TiposServicios.impresionlaser:
                this.router.navigate(['/lprecioimpresionlaser', this.selectedID]);
                break;
            case AppSettings.TiposServicios.papeleria:
                this.router.navigate(['/lpreciopapeleria', this.selectedID]);
                break;
        }
    }

    public gotoAdicionales() {
        switch (this.selectedEntity.idtiposervicio) {
            case AppSettings.TiposServicios.caterleria:
                this.router.navigate(['/lprecioadcarteleria', this.selectedID]);
                break;
            case AppSettings.TiposServicios.impresionlaser:
            this.router.navigate(['/lprecioadimpresionlaser', this.selectedID]);
                break;
            case AppSettings.TiposServicios.papeleria:
            this.router.navigate(['/lprecioadpapeleria', this.selectedID]);
                break;
        }
    }

    isPapeleria() {
        if (this.selectedEntity.idtiposervicio !== undefined) {
            return (+this.selectedEntity.idtiposervicio === AppSettings.TiposServicios.papeleria);
        } else {
            return false;
        }
    }

    isImpresion() {
        if (this.selectedEntity.idtiposervicio !== undefined) {
            return (+this.selectedEntity.idtiposervicio === AppSettings.TiposServicios.impresionlaser);
        } else {
            return false;
        }
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                            return;
                        }
                        if (goBack) {
                            this.router.navigate(['/listasprecios']);
                        } else {
                            this.router.navigate(['/listaprecio', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
