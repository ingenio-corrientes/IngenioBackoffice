import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaPrecioComponent } from './listaprecio.component';

const routes: Routes = [
    { path: '', component: ListaPrecioComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ListaPrecioRoutingModule { }
