import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../shared';

import { ListaPrecioRoutingModule } from './listaprecio-routing.module';
import { ListaPrecioComponent } from './listaprecio.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        ListaPrecioRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [ListaPrecioComponent]
})
export class ListaPrecioModule { }
