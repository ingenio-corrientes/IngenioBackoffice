import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Gramaje, GramajeService } from '../../shared';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
    selector: 'app-tables',
    templateUrl: './gramajes.component.html',
    styleUrls: ['./gramajes.component.scss'],
    animations: [routerTransition()],
    providers: [
        GramajeService,
    ]
})

export class GramajesComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
          id: { title: 'Código', filter: false, },
          gramaje: { title: 'Gramaje', filter: false, },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    public modalTitle = 'Gramajes';

    public data: Array<any> = Array<any>();

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private dataService: GramajeService) {
        this.source = new LocalDataSource(this.data);
        this.isLoading = true;
    }

    public ngOnInit(): void {
        this.dataService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar los datos!!', false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/gramaje', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            {
                field: 'id',
                search: query,
            },
            {
                field: 'gramaje',
                search: query,
            }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/gramaje', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/gramajes']);
                        } else {
                            this.router.navigate(['/gramajes']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
