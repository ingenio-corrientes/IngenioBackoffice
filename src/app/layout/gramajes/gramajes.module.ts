import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { GramajesRoutingModule } from './gramajes-routing.module';
import { GramajesComponent } from './gramajes.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        GramajesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [GramajesComponent]
})
export class GramajesModule { }
