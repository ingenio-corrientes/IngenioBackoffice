import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GramajesComponent } from './gramajes.component';

const routes: Routes = [
    { path: '', component: GramajesComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GramajesRoutingModule { }
