import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CajaInformeComponent } from './cajainforme.component';
// import {
//   InicioDiaComponent,
//   ArqueoComponent,
//   RetirosComponent
// } from './components/';

import { PageHeaderModule } from './../../shared';

describe('CajaInformeComponent', () => {
  let component: CajaInformeComponent;
  let fixture: ComponentFixture<CajaInformeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        CajaInformeComponent,
        // InicioDiaComponent,
        // ArqueoComponent
        // RetirosComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaInformeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
