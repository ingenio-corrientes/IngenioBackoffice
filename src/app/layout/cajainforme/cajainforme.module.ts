import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { CajaInformeRoutingModule } from './cajainforme-routing.module';
import { CajaInformeComponent } from './cajainforme.component';

@NgModule({
    imports: [
        CommonModule,
        CajaInformeRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      CajaInformeComponent
    ]
})
export class CajaInformeModule { }
