import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { LocalDataSource } from 'ng2-smart-table';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR } from '../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import { ModuloXRolService,
    NgbDateCustomParserFormatter,
    InformeGeneralCaja,
    RegistroCajaService, TWUtils } from '../../shared';

import 'rxjs/add/observable/forkJoin';

@Component({
    selector: 'app-form',
    templateUrl: './cajainforme.component.html',
    styleUrls: ['./cajainforme.component.scss'],
    animations: [routerTransition()],
    providers: [
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        RegistroCajaService,
        ModuloXRolService,
        DatePipe,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        CurrencyAR]
})

export class CajaInformeComponent implements OnInit {
    readonly IDMODULO = 10;
    public modalMessage: string;
    public modalTitle = 'Informe General de Caja';

    settingscobros = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            forma: { title: 'Tipo', filter: false, },
            total: {
                title: 'Total',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    sourcecobros: LocalDataSource;

    settingspagos = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            forma: { title: 'Tipo', filter: false, },
            total: {
                title: 'Total',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    sourcepagos: LocalDataSource;

    public data: Array<any> = Array<any>();

    public selectedEntity: InformeGeneralCaja;
    public fechaDesde:  {};
    public fechaHasta:  {};
    loggedId: number;

    isLoading = false;

    public constructor(public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private cp: CurrencyAR,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private datepipe: DatePipe,
        private moduloXRolService: ModuloXRolService,
        private cajaService: RegistroCajaService) {
        this.selectedEntity = new InformeGeneralCaja();
    }

    public ngOnInit(): void {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedId = +jsonData.id;
        this.evalPermisos();
        const today: Date = new Date();
        today.setMonth(today.getMonth() - 1);
        this.fechaDesde = TWUtils.stringDateToJson(this.datepipe.transform(today, 'yyyy-MM-dd'));
        this.fechaHasta = TWUtils.stringDateToJson(this.datepipe.transform(new Date(), 'yyyy-MM-dd'));
    }

    evalPermisos() {
        this.moduloXRolService.getListByIDUsuarioIDModulo(this.loggedId, this.IDMODULO)
            .then(response => {
                if (response) {
                    if (+response.ver !== 1) {
                        this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    }
                    this.isLoading = false;
                } else {
                    this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    public genInforme(): any {
        const fecDesde = TWUtils.arrayDateToString(this.fechaDesde);
        const fecHasta = TWUtils.arrayDateToString(this.fechaHasta);
        this.getFormasCobroPagos(fecDesde, fecHasta);
        this.getInformeGeneral(fecDesde, fecHasta);
    }

    private getFormasCobroPagos(fecDesde, fecHasta) {
        this.cajaService.getInformeCobroPagos(fecDesde, fecHasta).then(
            response => {
                if (response) {
                    this.data = response;
                    const dataCobros = this.data.filter(data => +data.tipo === 0);
                    this.sourcecobros = new LocalDataSource(dataCobros);
                    const dataPagos = this.data.filter(data => +data.tipo === 1);
                    this.sourcepagos = new LocalDataSource(dataPagos);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido obtener los datos de cobros y pagos!!', false, false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    private getInformeGeneral(fecDesde, fecHasta) {
        this.cajaService.getInformeGeneral(fecDesde, fecHasta).then(
            response => {
                this.selectedEntity = response as InformeGeneralCaja;
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelectRecibos(event: any): any {
        const fecinicio = TWUtils.arrayDateToNumber(this.fechaDesde);
        const fecfin = TWUtils.arrayDateToNumber(this.fechaHasta);
        this.router.navigate(['/recibos', fecinicio, fecfin]);
    }

    public userRowSelectGastos(event: any): any {
        const fecinicio = TWUtils.arrayDateToNumber(this.fechaDesde);
        const fecfin = TWUtils.arrayDateToNumber(this.fechaHasta);
        this.router.navigate(['/gastos', fecinicio, fecfin]);
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                        }
                        if (goBack) {
                            this.router.navigate(['/dashboard']);
                        } else {
                            this.router.navigate(['/dashboard']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
