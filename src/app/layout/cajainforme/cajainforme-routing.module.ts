import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CajaInformeComponent } from './cajainforme.component';

const routes: Routes = [
    { path: '', component: CajaInformeComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CajaInformeRoutingModule { }
