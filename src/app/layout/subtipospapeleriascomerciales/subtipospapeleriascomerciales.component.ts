import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { SubtipoPapeleriaComercial, SubtipoPapeleriaComercialService } from '../../shared';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
    selector: 'app-tables',
    templateUrl: './subtipospapeleriascomerciales.component.html',
    styleUrls: ['./subtipospapeleriascomerciales.component.scss'],
    animations: [routerTransition()],
    providers: [
        SubtipoPapeleriaComercialService
    ]
})

export class SubtiposPapeleriasComercialesComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
          id: { title: 'Código', filter: false, },
          tipopapeleriacomercial: { title: 'Papeleria Comercial', filter: false, },
          subtipopapeleriacomercial: { title: 'Subtipo Papelería Comercial', filter: false, },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public data: Array<any> = Array<any>();
    public modalMessage: string;
    public modalTitle = 'SubtipoPapeleriaComercial';

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private dataService: SubtipoPapeleriaComercialService) {
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.dataService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    this.showMessage('No se han podido cargar los datos!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/subtipopapeleriacomercial', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            {
                field: 'id',
                search: query,
            },
            {
                field: 'tipopapeleriacomercial',
                search: query,
            },
            {
                field: 'subtipopapeleriacomercial',
                search: query,
            }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/subtipopapeleriacomercial', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/subtipopapeleriacomercial']);
                        } else {
                            this.router.navigate(['/subtipopapeleriacomercial']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
