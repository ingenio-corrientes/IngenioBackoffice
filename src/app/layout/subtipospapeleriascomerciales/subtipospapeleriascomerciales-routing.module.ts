import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubtiposPapeleriasComercialesComponent } from './subtipospapeleriascomerciales.component';

const routes: Routes = [
    { path: '', component: SubtiposPapeleriasComercialesComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SubtiposPapeleriasComercialesRoutingModule { }
