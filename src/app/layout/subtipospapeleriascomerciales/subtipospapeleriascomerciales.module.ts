import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { SubtiposPapeleriasComercialesRoutingModule } from './subtipospapeleriascomerciales-routing.module';
import { SubtiposPapeleriasComercialesComponent } from './subtipospapeleriascomerciales.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        SubtiposPapeleriasComercialesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [SubtiposPapeleriasComercialesComponent]
})
export class SubtiposPapeleriasComercialesModule { }
