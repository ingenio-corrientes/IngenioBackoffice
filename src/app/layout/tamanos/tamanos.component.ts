import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Tamano, TamanoService } from '../../shared';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
    selector: 'app-tables',
    templateUrl: './tamanos.component.html',
    styleUrls: ['./tamanos.component.scss'],
    animations: [routerTransition()],
    providers: [
        TamanoService,
    ]
})

export class TamanosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
          id: { title: 'Código', filter: false, },
          tamano: { title: 'Tamaño', filter: false, },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public data: Array<any> = Array<any>();
    public modalMessage: string;
    public modalTitle = 'Banco';

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private dataService: TamanoService) {
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.dataService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    this.showMessage('No se han podido cargar los datos!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/tamano', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            {
                field: 'id',
                search: query,
            },
            {
                field: 'tamano',
                search: query,
            }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/tamano', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/tamano']);
                        } else {
                            this.router.navigate(['/tamano']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
