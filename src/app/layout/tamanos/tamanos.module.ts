import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { TamanosRoutingModule } from './tamanos-routing.module';
import { TamanosComponent } from './tamanos.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        TamanosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [TamanosComponent]
})
export class TamanosModule { }
