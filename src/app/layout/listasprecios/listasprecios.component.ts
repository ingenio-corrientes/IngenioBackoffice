import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { ModuloXRolService, ListaPrecio, ListaPrecioService } from '../../shared';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
    selector: 'app-tables',
    templateUrl: './listasprecios.component.html',
    styleUrls: ['./listasprecios.component.scss'],
    animations: [routerTransition()],
    providers: [
        ListaPrecioService,
        ModuloXRolService
    ]
})

export class ListasPreciosComponent implements OnInit {
    readonly IDMODULO = 8;
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            servicio: { title: 'Tipo de Servicio', filter: false, },
            id: { title: 'Código', filter: false, },
            listaprecio: { title: 'Descripción', filter: false, },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    public modalTitle = 'ListasPrecios';

    public data: Array<any> = Array<any>();
    loggedId: number;

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private dataService: ListaPrecioService,
        private moduloXRolService: ModuloXRolService) {
        this.source = new LocalDataSource(this.data);
        this.isLoading = true;
    }

    public ngOnInit(): void {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedId = +jsonData.id;
        this.evalPermisos();
    }

    evalPermisos() {
        this.moduloXRolService.getListByIDUsuarioIDModulo(this.loggedId, this.IDMODULO)
            .then(response => {
                if (response) {
                    if (+response.ver !== 1) {
                        this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    }
                    this.isLoading = false;
                } else {
                    this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    this.isLoading = false;
                } if (+response.ver === 1) {
                    this.dataService.getList().then(
                    lresponse => {
                        if (lresponse) {
                            this.data = lresponse;
                            this.source = new LocalDataSource(this.data);
                            this.source.setSort([{ field: 'servicio', direction: 'asc' }]);
                            this.isLoading = false;
                        } else {
                            this.showMessage('No se han podido cargar los datos!!', false, false);
                            this.isLoading = false;
                        }
                    }
                ).catch(e => this.handleError(e));
                }
            })
        .catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/listaprecio', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            {
                field: 'servicio',
                search: query,
            },
            {
                field: 'id',
                search: query,
            },
            {
                field: 'listaprecio',
                search: query,
            }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/listaprecio', event.data.id]);
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                            return;
                        }
                        if (goBack) {
                            this.router.navigate(['/listasprecios']);
                        } else {
                            this.router.navigate(['/listasprecios']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
