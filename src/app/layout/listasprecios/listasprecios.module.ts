import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { ListasPreciosRoutingModule } from './listasprecios-routing.module';
import { ListasPreciosComponent } from './listasprecios.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        ListasPreciosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [ListasPreciosComponent]
})
export class ListasPreciosModule { }
