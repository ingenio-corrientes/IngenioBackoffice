import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListasPreciosComponent } from './listasprecios.component';

const routes: Routes = [
    { path: '', component: ListasPreciosComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ListasPreciosRoutingModule { }
