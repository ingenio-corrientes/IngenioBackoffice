import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {Proveedor, ProveedorService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './proveedor.component.html',
    styleUrls: ['./proveedor.component.scss'],
    animations: [routerTransition()],
    providers: [ProveedorService]
})

export class ProveedorComponent implements OnInit {
    public modalTitle = 'Cliente';

    submitted = false;
    public selectedID: number;
    public selectedEntity: Proveedor;

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private proveedorService: ProveedorService) {
        this.selectedEntity = new Proveedor();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    public getEntityData(): any {
        if (this.selectedID > -1) {
          this.proveedorService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as Proveedor;
                  this.isLoading = false;
              })
          .catch(e => this.handleError(e));
        } else {
         this.isLoading = false;
        }
    }

    public onSave() {
        this.selectedEntity.cuit = (this.selectedEntity.cuit.toString().length === 0) ? (0) : (this.selectedEntity.cuit);
        this.proveedorService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El proveedor se guardó correctamente', true);
                    // this.router.navigate(['/clientes']);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.proveedorService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.proveedorService.ENTITY_DELETED) {
                    this.showMessage('El proveedor se ha eliminado correctamente', true);
                } else  if (+response === this.proveedorService.ERROR_FOREING_KEY) {
                    this.showMessage('El proveedor está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El proveedor no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/proveedores']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/proveedores']);
                        } else {
                            this.router.navigate(['/proveedor', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
