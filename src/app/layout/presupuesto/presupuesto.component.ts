import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { GenPresupuesto } from '../../shared/services/vouchers/gen-presupuesto';

import {
    CurrencyAR,
    SessionStore, NgbDateCustomParserFormatter,
    Presupuesto,
    PresupuestoService,
    TWUtils,
    AppSettings,
    PresupuestoItem,
    PresupuestoItemAds} from '../../shared';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-form',
    templateUrl: './presupuesto.component.html',
    styleUrls: ['./presupuesto.component.scss'],
    animations: [routerTransition()],
    providers: [
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        SessionStore,
        PresupuestoService,
        PresupuestoItem,
        CurrencyAR,
        DatePipe ]
})

export class PresupuestoComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Presupuesto';

    public selectedID: number;
    public preSelectedID: number;
    public selectedEntity: Presupuesto;

    activeTabId = '2';

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private sessionStore: SessionStore,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private cp: CurrencyAR,
        private datePipe: DatePipe,
        private presupuestoService: PresupuestoService,
        private itemService: PresupuestoService) {
        this.selectedEntity = new Presupuesto();
        this.sessionStore.init('presupuesto');
    }

    public ngOnInit(): void {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        const loggedId = +jsonData.id;
        this.selectedEntity.idusuario = loggedId;
        this.selectedEntity.usuario = jsonData.usuario;
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.preSelectedID = this.sessionStore.getNumberKey('preSelectedID');
            if (this.selectedID !== -1) {
                if (this.preSelectedID === this.selectedID) {
                    this.activeTabId = this.sessionStore.getKey('activeTabId');
                } else {
                    this.activeTabId = '1';
                }
            } else {
                this.activeTabId = '1';
            }
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        if (this.selectedID > -1) {
            this.presupuestoService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Presupuesto;
                }
            ).catch(e => this.handleError(e));
        }
    }

    getEstado() {
        if (this.selectedID === -1) {
            return AppSettings.EstadosPresupuesto.nuevo;
        }
        if (TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            return AppSettings.EstadosPresupuesto.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecentrega)) {
            return AppSettings.EstadosPresupuesto.entegado;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecaproduccion)) {
            return AppSettings.EstadosPresupuesto.en_produccion;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            return AppSettings.EstadosPresupuesto.emitido;
        }
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.sessionStore.setNumberKey('preSelectedID', -1);
        this.sessionStore.setKey('activeTabId', this.activeTabId);
        this.router.navigate(['/presupuestos']);
    }

    public registrar() {
        this.selectedEntity.dirty = 0;
        this.presupuestoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                } else {
                    this.showMessage('No se ha podido actualizar el estado a emitido', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public emitir() {
        const p0 = this.presupuestoService.getByID(this.selectedID);
        const p1 = this.itemService.getItemsById(this.selectedID);
        const p2 = this.itemService.getItemsAdsById(this.selectedID);
        if (this.selectedID !== -1) {
            Promise.all([p0, p1, p2])
            .then( ([entity, items, itemsads]) => {
                this.selectedEntity = entity as Presupuesto;
                if (TWUtils.isNullDate(this.selectedEntity.fecemision)) {
                    this.selectedEntity.fecemision = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
                }
                this.emitirPresupuesto(items, itemsads);
            });
        }
    }

    registrarEmision() {
        this.presupuestoService.emitir(this.selectedEntity).then(
            response => {
                if (response) {
                    this.onGetEntityData();
                    this.showMessage('Registrada la emisión correctamente.', false);
                }
            }
        )
    }

    emitirPresupuesto(items: PresupuestoItem[], itemsads: PresupuestoItemAds[]): any {
        if (items.length === 0) {
            this.showMessage('El presupuesto no posee ítems! No se puede emitir sin items.', false);
            return;
        }
        const presup = new GenPresupuesto(this.cp);
        presup.genPresupuesto(this.selectedEntity, items, itemsads);
        this.registrarEmision();
    }

    public aProduccion() {
        this.presupuestoService.aProduccion(this.selectedEntity).then(
            response => {
                if (response) {
                    this.onGetEntityData();
                    this.showMessage('Enviado a producción correctamente.', false);
                }
            }
        )
    }

    onTabChange(event) {
        this.activeTabId = event.nextId;
        this.sessionStore.setNumberKey('preSelectedID', this.selectedEntity.id);
        this.sessionStore.setKey('activeTabId', this.activeTabId);
    }

    public genRecibo(content) {
        this.sessionStore.setNumberKey('preSelectedID', this.selectedEntity.id);
        this.sessionStore.setKey('activeTabId', this.activeTabId);
        this.router.navigate(['/recibo', -1, this.selectedEntity.id]);
    }

    public getEntityData(): Promise<boolean>  {
        return this.presupuestoService.getByID(this.selectedID).then(
            response => {
                this.selectedEntity = response as Presupuesto;
                return true;
            }).catch(e => this.handleError(e));
    }

    // public getItems(): Promise<boolean> {
    //     return this.itemService.getItemsById(this.selectedID)
    //     .then(
    //         response => {
    //             if (response) {
    //                 this.dataItems = response;
    //                 return true;
    //             }
    //         }
    //     ).catch(e => this.handleError(e));
    // }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuestos']);
                        } else {
                            this.router.navigate(['/presupuesto', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        // console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
