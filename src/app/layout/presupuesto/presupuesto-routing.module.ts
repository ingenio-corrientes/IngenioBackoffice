import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresupuestoComponent } from './presupuesto.component';

const routes: Routes = [
    { path: '', component: PresupuestoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PresupuestoRoutingModule { }
