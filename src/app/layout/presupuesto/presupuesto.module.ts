import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { PresupuestoRoutingModule } from './presupuesto-routing.module';
import { PresupuestoComponent } from './presupuesto.component';
import {
  DataComponent,
  ItemsComponent,
  // ItemCargaCarteleriaComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        PresupuestoRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      PresupuestoComponent,
      DataComponent,
      ItemsComponent,
      // ItemCargaCarteleriaComponent
    ]
})
export class PresupuestoModule { }
