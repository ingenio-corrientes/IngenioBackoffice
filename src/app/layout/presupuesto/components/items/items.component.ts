import { Component, OnInit, Input,  SimpleChange, ViewContainerRef  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { AppSettings, TWUtils, Presupuesto } from '../../../../shared/';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { PresupuestoItem,
    PresupuestoService } from '../../../../shared/';

@Component({
    selector: 'app-items',
    templateUrl: './items.component.html',
    styleUrls: ['./items.component.scss'],
    providers: [ PresupuestoService]
})

export class ItemsComponent implements OnInit {
    @Input() selectedEntity: Presupuesto;
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            tiposervicio: { title: 'Tipo de servicio', filter: false, },
            descripcion: { title: 'Descripción', filter: false, },
            cantidad: { title: 'Cantidad', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public data: Array<any> = Array<any>();
    public selectedItem: PresupuestoItem = null;
    public selectedItemIDCarteleria: number;
    public selectedIDData: number;
    public selectedIDPresupuesto: number;
    public tipoItem: number;
    public modalMessage: string;
    public modalTitle = 'Item';

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private itemService: PresupuestoService) {
            this.isLoading = true;
            this.source = new LocalDataSource(this.data);
            this.tipoItem = -1;
            this.selectedItemIDCarteleria = -1;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDData = params['id'];
            this.getData();
        });
    }

    handleItemselectedItemCarteleriaUpdated(event) {
        this.getData();
    }

    public getData() {
        if (this.selectedIDData !== -1) {
            this.itemService.getItemsById(this.selectedIDData).then(
                response => {
                    if (response) {
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                    } else {
                        this.showMessage('No se ha podido guardar el ítem!!', false);
                    }
                    this.isLoading = false;
                }
            ).catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'tiposervicio', search: query, },
            { field: 'descripcion', search: query, },
            { field: 'cantidad', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        switch (event.data.idtiposervicio) {
            case AppSettings.TiposServicios.caterleria:
            this.router.navigate(['/presitcarteleria', this.selectedIDData, event.data.id]);
            break;
            case AppSettings.TiposServicios.diseno:
            this.router.navigate(['/presitdiseno', this.selectedIDData, event.data.id]);
            break;
            case AppSettings.TiposServicios.impresionlaser:
            this.router.navigate(['/presitimpresionlaser', this.selectedIDData, event.data.id]);
            break;
            case AppSettings.TiposServicios.papeleria:
            this.router.navigate(['/presitpapeleria', this.selectedIDData, event.data.id]);
            break;
        }
    }

    public onNuevo(tipoServicio) {
        switch (tipoServicio) {
            case AppSettings.TiposServicios.caterleria:
            this.router.navigate(['/presitcarteleria', this.selectedIDData, -1]);
            break;
            case AppSettings.TiposServicios.diseno:
            this.router.navigate(['/presitdiseno', this.selectedIDData, -1]);
            break;
            case AppSettings.TiposServicios.impresionlaser:
            this.router.navigate(['/presitimpresionlaser', this.selectedIDData, -1]);
            break;
            case AppSettings.TiposServicios.papeleria:
            this.router.navigate(['/presitpapeleria', this.selectedIDData, -1]);
            break;
        }
    }

    getEstadoHabilitante() {
        if (this.selectedIDData === -1) {
            return AppSettings.EstadosPresupuesto.nuevo;
        }
        if (TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            return AppSettings.EstadosPresupuesto.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecentrega)) {
            return AppSettings.EstadosPresupuesto.entegado;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecaproduccion)) {
            return AppSettings.EstadosPresupuesto.en_produccion;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            return AppSettings.EstadosPresupuesto.emitido;
        }
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuestos', '/' + this.selectedIDPresupuesto ]);
                        } else {
                            this.router.navigate(['/presupuesto', this.selectedIDData]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
