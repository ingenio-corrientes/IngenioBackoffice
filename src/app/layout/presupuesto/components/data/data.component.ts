import { Component, OnInit, SimpleChanges, ViewContainerRef, OnChanges, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { DatePipe } from '@angular/common';
import { CurrencyAR } from '../../../../shared/pipes/currency-ar';

import {
    Presupuesto, PresupuestoService,
    ClienteService,
    FormaCobroService,
    TWUtils,
    AppSettings} from '../../../../shared';

@Component({
    selector: 'app-data',
    templateUrl: './data.component.html',
    styleUrls: ['./data.component.scss'],
    providers: [
        DatePipe,
        CurrencyAR,
        PresupuestoService,
        ClienteService,
        FormaCobroService]
})

export class DataComponent implements OnInit, OnChanges {
    @Input() selectedEntity: Presupuesto;
    public modalTitle = 'Presupuesto';

    public dataClientes: Array<any> = Array<any>();
    public dataFormasCobros: Array<any> = Array<any>();

    public selectedID: number;
    public fechaEmision: {};
    public fechaAProduccion: {};
    public estado;
    hasPercChange: Boolean;
    total: number;
    totalStr: string;
    aCtaCte = false;

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private dp: DatePipe,
        private cp: CurrencyAR,
        private presupuestoService: PresupuestoService,
        private clienteService: ClienteService,
        private formaCobroService: FormaCobroService) {
            this.isLoading = true;
            const jsonData = JSON.parse(localStorage.getItem('userLog'));
            this.selectedEntity = new Presupuesto();
            this.selectedEntity.idusuario = jsonData.id;
            this.selectedEntity.usuario = jsonData.usuario;
            this.estado = 'Borrador';
            this.total = 0;
            this.totalStr = this.cp.transform(this.total);
            this.aCtaCte = false;
            this.hasPercChange = false;
            this.showDates();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getData();
        });
    }

    public getData() {
        this.isLoading = true;
        const p0 = this.clienteService.getList();
        const p1 = this.formaCobroService.getList()
        const p2 = this.presupuestoService.getItemsById(this.selectedID);
        const p3 = this.presupuestoService.getItemsAdsById(this.selectedID);
        Promise.all([
            p0, p1, p2, p3
        ]).then( ([clientes, formascobros, items, itemsads]) => {
            this.dataClientes = clientes;
            this.dataFormasCobros = formascobros;
            this.total = 0;
            items.forEach(item => {
                this.total += +item.subtotal;
            })
            itemsads.forEach(item => {
                this.total += +item.subtotal;
            })
            this.totalStr = this.cp.transform(this.total);
            this.onSetEntityData();
            this.isLoading = false;
        });
    }

    ngOnChanges() {
        this.showDates();
    }

    public getClientes() {
        return this.clienteService.getList().then(
            response => {
                if (response) {
                    this.dataClientes = response;
                } else {
                    this.showMessage('No se han podido obtener datos de clientes!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getFormaCobro() {
        return this.formaCobroService.getList().then(
            response => {
                if (response) {
                    this.dataFormasCobros = response;
                } else {
                    this.showMessage('No se han podido obtener datos de formas de cobro!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onSetEntityData(): any {
        if (this.selectedID > -1) {
            this.presupuestoService.getByID(this.selectedID)
            .then(
                response => {
                    this.selectedEntity = response as Presupuesto;
                    this.aCtaCte = (this.selectedEntity.actacte > 0) ;
                    this.showDates();
                    this.estado = this.getEstado();
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    showDates() {
        this.fechaEmision = '';
        this.fechaAProduccion = '';
        if (!TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            this.fechaEmision =
                        (!TWUtils.isNullDate(this.selectedEntity.fecemision)
                        ? this.dp.transform(this.selectedEntity.fecemision, 'dd-MM-yyyy')
                        : '');
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecaproduccion)) {
            this.fechaAProduccion =
                        (!TWUtils.isNullDate(this.selectedEntity.fecaproduccion)
                        ? this.dp.transform(this.selectedEntity.fecaproduccion, 'dd-MM-yyyy')
                        : '');
        }
    }

    getEstado() {
        if (this.selectedEntity.dirty === 1) {
            return 'Borrador';
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecentrega)) {
            return 'Entregado';
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecaproduccion)) {
            return 'En Producción';
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            return 'Emitido';
        }
    }

    getEstadoHabilitante() {
        if (this.selectedID === -1) {
            return AppSettings.EstadosPresupuesto.nuevo;
        }
        if (TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            return AppSettings.EstadosPresupuesto.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecentrega)) {
            return AppSettings.EstadosPresupuesto.entegado;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecaproduccion)) {
            return AppSettings.EstadosPresupuesto.en_produccion;
        }
        if (!TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            return AppSettings.EstadosPresupuesto.emitido;
        }
    }

    onDescyRecChange() {
        this.hasPercChange = true;
    }

    applyPorcentaje() {
        this.presupuestoService.aplicarDescRec(this.selectedEntity).then(
            response => {
                if (response) {
                    this.hasPercChange = false;
                    this.onSetEntityData();
                    this.showMessage('Descuento o recargo aplicado!', false);
                }}
        ).catch(e => this.handleError(e));
    }

    public onSave() {
        this.isLoading = true;
        this.presupuestoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El presupuesto se guardó correctamente', false);
                } else {
                    this.showMessage('El presupuesto no se guardó correctamente', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public isDirty() {
        return (this.selectedEntity.dirty === 1);
    }

    public verCliente() {
        if (this.selectedEntity.idcliente > -1) {
            this.router.navigate(['/cliente', this.selectedEntity.idcliente]);
        }
    }

    public eliminar() {
        this.isLoading = true;
        this.presupuestoService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.presupuestoService.ENTITY_DELETED) {
                    this.showMessage('El presupuesto se ha eliminado correctamente', true);
                } else  if (+response === this.presupuestoService.ERROR_FOREING_KEY) {
                    this.showMessage('El presupuesto está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El presupuesto no se ha podido eliminar!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    onCheck() {
        this.aCtaCte = !this.aCtaCte;
        this.selectedEntity.actacte = this.aCtaCte ? 1 : 0;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuestos']);
                        } else {
                            this.router.navigate(['/presupuesto', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
