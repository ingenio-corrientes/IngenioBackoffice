import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

// relative import
import { CompraRoutingModule } from './compra-routing.module';
import { CompraComponent } from './compra.component';
import {
    CompraDataComponent,
    CompraItemsComponent,
    ItemCargaComponent
} from './components/';

@NgModule({
    imports: [
        CommonModule,
        CompraRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
        CompraComponent,
        CompraDataComponent,
        CompraItemsComponent,
        ItemCargaComponent
    ]
})
export class CompraModule { }
