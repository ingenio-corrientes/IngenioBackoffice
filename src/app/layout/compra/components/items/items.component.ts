import { Component, OnInit, Input, SimpleChange, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { CompraItem, CompraItemService } from '../../../../shared/';

@Component({
    selector: 'app-items',
    templateUrl: './items.component.html',
    styleUrls: ['./items.component.scss'],
    providers: [CompraItemService]
})

export class CompraItemsComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            descripcion: { title: 'Ítem', filter: false, },
            cantidad: { title: 'Cantidad', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public modalTitle = 'ItemsCompra';
    public data: Array<any> = Array<any>();
    public selectedItem: CompraItem = null;
    public selectedIDCompra: string;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private compraitemService: CompraItemService) {
        this.isLoading = true;
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDCompra = params['id'];
            this.getData();
        });
    }

    handleItemUpdated(user) {
        this.getData();
    }

    public getData() {
        if (this.selectedIDCompra.length > 0) {
            this.compraitemService.getListByIDGasto(this.selectedIDCompra).then(
                response => {
                    if (response) {
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                        this.isLoading = false;
                    } else {
                        this.showMessage('No se han podido cargar datos de formas de pagos!!', false);
                        this.isLoading = false;
                    }
                }
            ).catch(e => this.handleError(e));
        }
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'descripcion', search: query, },
            { field: 'cantidad', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data as CompraItem;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            if (goBack) {
                                this.router.navigate(['/compras']);
                            } else {
                                this.router.navigate(['/compra', this.selectedIDCompra]);
                            }
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
