import { Component, OnInit, Input,
    Output, EventEmitter, SimpleChange,
    ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Compra,
    CompraService,
    CompraItem,
    CompraItemService } from '../../../../shared/';

@Component({
    selector: 'app-item-carga',
    templateUrl: './item-carga.component.html',
    styleUrls: ['./item-carga.component.scss'],
    providers: [
        Compra,
        CompraService,
        CompraItemService ]
})

export class ItemCargaComponent implements OnInit {
    public modalTitle = 'Compra';
    @Input() selectedItem: CompraItem;
    @Output() itemUpdated = new EventEmitter();

    // datoa estructurados
    public selectedCompra: Compra;
    public selectedEntity: CompraItem;
    private selectedIDCompra: string;
    public compra: Compra;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private viewContainer: ViewContainerRef,
        private modal: ModalDialogService,
        private compraitemService: CompraItemService,
        private compraService: CompraService) {
            this.compra = new Compra();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDCompra = params['id'];
            this.cancelar();
            // this.getCompra();
        });
    }

    // getCompra(): any {
    //     this.compraService.getByID(this.selectedIDCompra).then(
    //         response => {
    //             if (response) {
    //                 this.selectedCompra = response;
    //             } else {
    //                 alert('No se pudo obtener los datos de compra.');
    //             }
    //         }
    //     ).catch(e => this.handleError(e));
    // }

    public getSubtotal() {
        this.compraService.getByID(this.selectedIDCompra).then(
            response => {
                if (response) {
                    this.compra = response;
                } else {
                    alert('No se han podido obtener los datos del subtotal!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue;
        }
    }

    onSubmit() {
        this.selectedEntity.idcompra = this.selectedIDCompra;
        this.compraitemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.itemUpdated.emit(true);
                    this.cancelar();
                } else {
                    this.showMessage('No se han podido guardar los datos del item de compra!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
      this.compraitemService.delete(this.selectedEntity)
      .then(
          response => {
            if (response) {
                this.itemUpdated.emit(true);
                this.cancelar();
            } else {
                this.showMessage('No se han podido eliminar los datos del item de compra!!', false);
            }
        }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new CompraItem();
        this.itemUpdated.emit(true);
        this.getSubtotal();
    }

    isNew() {
      return (this.selectedEntity.id === '-');
    }

    public isSalded() {
        if (this.isNew()) {
            return false;
        }
        if (this.compra) {
            return (+this.compra.bloqueado === 1);
        } else {
            return false;
        }
    }
    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
