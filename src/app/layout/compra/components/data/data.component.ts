import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { DatePipe } from '@angular/common';

import {
    Compra, CompraService,
    TipoComprobanteService,
    ProveedorService,
    TWUtils } from '../../../../shared';

@Component({
    selector: 'app-data',
    templateUrl: './data.component.html',
    styleUrls: ['./data.component.scss'],
    providers: [
        CompraService,
        TipoComprobanteService,
        ProveedorService,
        DatePipe]
})

export class CompraDataComponent implements OnInit {
    public modalTitle = 'Compra';

    public dataProveedores: Array<any> = Array<any>();
    public dataTiposComprobantes: Array<any> = Array<any>();

    public selectedEntity: Compra;
    public selectedID: string;
    public fechaEmision: {};

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private dp: DatePipe,
        private compraService: CompraService,
        private tipoComprobanteService: TipoComprobanteService,
        private proveedorService: ProveedorService) {
        this.isLoading = true;
        this.selectedID = '';
        this.selectedEntity = new Compra();
        this.showDates();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = (params['id'] === '-') ? '' : params['id'];
            this.getData();
        });
    }

    public getData() {
        const p0 = this.proveedorService.getList();
        const p1 = this.tipoComprobanteService.getList();
        Promise.all([
            p0, p1
        ]).then(([result0, result1]) => {
            this.dataProveedores = result0;
            this.dataTiposComprobantes = result1;
            this.onSetEntityData();
        }).catch(e => this.handleError(e));
    }

    public onSetEntityData(): any {
        if (this.selectedID.length > 0) {
            this.compraService.getByID(this.selectedID)
            .then(
                response => {
                    this.selectedEntity = response as Compra;
                    this.fechaEmision = TWUtils.stringDateToJson(this.selectedEntity.fecemision);
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        if (+this.selectedEntity.total < +this.selectedEntity.subtotal) {
            this.showMessage('El subtotal no puede ser menor que el total', false);
            return;
        }
        this.selectedEntity.fecemision = TWUtils.arrayDateToDateTimeString(this.fechaEmision);
        this.compraService.save(this.selectedEntity).then(
            response => {
                if (+response === this.compraService.ERROR_NOT_MODIFIED || +response === this.compraService.ERROR_UNKNOWN) {
                    this.showMessage('El comprobante no se guardó!! ' +
                        'Probablemente ya existe el tipo y número comprobante para el cliente indicado.', false);
                } else {
                    this.selectedID = response + '';
                    this.showMessage('El comprobante se guardó correctamente', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    showDates() {
        this.fechaEmision = '';
        if (!TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            this.fechaEmision = this.selectedEntity.fecemision;
        } else {
            this.fechaEmision = TWUtils.getTodayJson();
        }
    }

    public isNew() {
        return (this.selectedID.length === 0);
    }

    public eliminar() {
        this.compraService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.compraService.ENTITY_DELETED) {
                    this.showMessage('El compra se ha eliminado correctamente', true);
                } else  if (+response === this.compraService.ERROR_FOREING_KEY) {
                    this.showMessage('El compra está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El compra no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isSalded() {
        if (this.isNew()) {
            return false;
        }
        if (this.selectedEntity) {
            return (+this.selectedEntity.bloqueado === 1);
        } else {
            return false;
        }
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/compras']);
                        } else {
                            const id = (this.selectedID.length > 0) ? this.selectedID : '-';
                            this.router.navigate(['/compra', id]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
