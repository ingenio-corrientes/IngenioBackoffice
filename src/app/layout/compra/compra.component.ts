import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import {
    SessionStore, NgbDateCustomParserFormatter,
    Compra, CompraService, Gasto, GastoService } from '../../shared';

import 'rxjs/add/observable/forkJoin';

@Component({
    selector: 'app-form',
    templateUrl: './compra.component.html',
    styleUrls: ['./compra.component.scss'],
    animations: [routerTransition()],
    providers: [
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CompraService, GastoService ]
})

export class CompraComponent implements OnInit {
    public modalTitle = 'Compra';

    public selectedID: string;
    public selectedEntity: Compra;

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private viewContainer: ViewContainerRef,
        private modal: ModalDialogService,
        private compraService: CompraService,
        private gastoService: GastoService) {
        this.isLoading = true;
        this.selectedEntity = new Compra();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        if (this.selectedID !== '-') {
            this.compraService.getByID(this.selectedID)
            .then(
                response => {
                    this.selectedEntity = response as Compra;
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public isNew() {
        return (this.selectedID === '-');
    }

    public getEstado() {
        // if (this.selectedID === -1) {
        //     return AppSettings.EstadosRecibo.nuevo;
        // }
        // if (!(+this.selectedEntity.monto > 0)) {
        //     return AppSettings.EstadosRecibo.borrador;
        // }
    }

    public volver() {
        this.router.navigate(['/compras']);
    }

    public pagar() {
        const gasto = new Gasto();
        gasto.idproveedor = +this.selectedEntity.idproveedor;
        gasto.monto = this.selectedEntity.total;
        gasto.concepto = 'Compra';
        gasto.fecha = '';
        gasto.idcompraapagar = this.selectedEntity.id;
        this.gastoService.pagar(gasto)
            .then(response => {
                if (response !== -1) {
                    this.router.navigate(['/gasto', response]);
                }
            })
            .catch(e => this.handleError(e));
    }

    public eliminar() {
        console.log('eliminar');
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/compras']);
                        } else {
                            this.router.navigate(['/compra', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
