import { Component, OnInit, ViewContainerRef  } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {Gramaje, GramajeService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './gramaje.component.html',
    styleUrls: ['./gramaje.component.scss'],
    animations: [routerTransition()],
    providers: [GramajeService]
})

export class GramajeComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Gramaje';

    public selectedID: number;
    public selectedEntity: Gramaje;

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private entityService: GramajeService) {
        this.selectedEntity = new Gramaje();
        this.isLoading = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.entityService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as Gramaje;
                  this.isLoading = false;
              })
          .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        this.entityService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El gramaje se guardó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido obtener los datos del gramaje!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.entityService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.entityService.ENTITY_DELETED) {
                    this.showMessage('El gramaje se ha eliminado correctamente', true);
                } else  if (+response === this.entityService.ERROR_FOREING_KEY) {
                    this.showMessage('El gramaje está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El gramaje no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/gramajes']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/gramajes']);
                        } else {
                            this.router.navigate(['/gramaje', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
