import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GramajeComponent } from './gramaje.component';

const routes: Routes = [
    { path: '', component: GramajeComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GramajeRoutingModule { }
