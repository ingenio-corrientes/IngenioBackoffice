import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { SubtiposDisenosRoutingModule } from './subtiposdisenos-routing.module';
import { SubtiposDisenosComponent } from './subtiposdisenos.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        SubtiposDisenosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [SubtiposDisenosComponent]
})
export class SubtiposDisenosModule { }
