import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LPrecioCarteleriaComponent } from './lpreciocarteleria.component';

const routes: Routes = [
    { path: '', component: LPrecioCarteleriaComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LPrecioCarteleriaRoutingModule { }
