import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { LPrecioCarteleriaRoutingModule } from './lpreciocarteleria-routing.module';
import { LPrecioCarteleriaComponent } from './lpreciocarteleria.component';
import {
  ItemComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        LPrecioCarteleriaRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      LPrecioCarteleriaComponent,
      ItemComponent
    ]
})
export class LPrecioCarteleriaModule { }
