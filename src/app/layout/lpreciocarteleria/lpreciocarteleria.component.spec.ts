import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LPrecioCarteleriaComponent } from './lpreciocarteleria.component';
import {
    ItemComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

describe('LPrecioCarteleriaComponent', () => {
    let component: LPrecioCarteleriaComponent;
    let fixture: ComponentFixture<LPrecioCarteleriaComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                FormsModule,
                ReactiveFormsModule,
                NgbModule.forRoot(),
                PageHeaderModule
            ],
            declarations: [
                LPrecioCarteleriaComponent
                // ItemComponent
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LPrecioCarteleriaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
