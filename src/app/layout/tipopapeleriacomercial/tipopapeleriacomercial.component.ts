import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {TipoPapeleriaComercial, TipoPapeleriaComercialService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './tipopapeleriacomercial.component.html',
    styleUrls: ['./tipopapeleriacomercial.component.scss'],
    animations: [routerTransition()],
    providers: [TipoPapeleriaComercialService]
})

export class TipoPapeleriaComercialComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Tipo de Papelería Comercial';

    public selectedID: number;
    public selectedEntity: TipoPapeleriaComercial;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private tipopapeleriacomercialService: TipoPapeleriaComercialService) {
        this.selectedEntity = new TipoPapeleriaComercial();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.tipopapeleriacomercialService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as TipoPapeleriaComercial;
              })
          .catch(e => this.handleError(e));
      }
    }

    public onSave() {
        this.tipopapeleriacomercialService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El tipo de papelería comercial se guardó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido obtener los datos del tipo de papelería comercial!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.tipopapeleriacomercialService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.tipopapeleriacomercialService.ENTITY_DELETED) {
                    this.showMessage('El tipo de papelería comerical se ha eliminado correctamente', true);
                } else  if (+response === this.tipopapeleriacomercialService.ERROR_FOREING_KEY) {
                    this.showMessage('El tipo de papelería comerical está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El tipo de papelería comerical no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/tipospapeleriascomerciales']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/tipospapeleriascomerciales']);
                        } else {
                            this.router.navigate(['/tipopapeleriacomercial', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
