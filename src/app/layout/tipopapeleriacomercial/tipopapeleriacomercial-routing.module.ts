import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoPapeleriaComercialComponent } from './tipopapeleriacomercial.component';

const routes: Routes = [
    { path: '', component: TipoPapeleriaComercialComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TipoPapeleriaComercialRoutingModule { }
