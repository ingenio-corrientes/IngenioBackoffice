import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../shared';

import { TipoPapeleriaComercialRoutingModule } from './tipopapeleriacomercial-routing.module';
import { TipoPapeleriaComercialComponent } from './tipopapeleriacomercial.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        TipoPapeleriaComercialRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [TipoPapeleriaComercialComponent]
})
export class TipoPapeleriaComercialModule { }
