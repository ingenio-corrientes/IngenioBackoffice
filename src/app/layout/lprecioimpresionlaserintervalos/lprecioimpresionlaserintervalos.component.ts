import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { CurrencyPipe, PercentPipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { LPrecioImpresionLaserIntervalo, LPrecioImpresionLaserIntervaloService, LPrecioImpresionLaserService, LPrecioImpresionLaser } from '../../shared/';

@Component({
    selector: 'app-data',
    templateUrl: './lprecioimpresionlaserintervalos.component.html',
    styleUrls: ['./lprecioimpresionlaserintervalos.component.scss'],
    animations: [routerTransition()],
    providers: [
        LPrecioImpresionLaserService,
        LPrecioImpresionLaserIntervaloService,
        CurrencyPipe, PercentPipe]
})

export class LPrecioImpresionLaserIntervalosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            descripcion: { title: 'Descripción', filter: false, },
            maximo: { title: 'Máximo', filter: false, },
            porcentaje: {
                title: 'Porcentaje', filter: false,
                valuePrepareFunction: (value) => {
                    return this.pp.transform(value / 100);
                }
            },
            precio: {
                title: 'Precio', filter: false,
                valuePrepareFunction: (value) => {
                    return this.cp.transform(value, 'USD', true, '1.2-2');
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered table-hover',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'Papeleria intervalos';

    public data: Array<any> = Array<any>();
    selectedItemLista: LPrecioImpresionLaser = null;
    public selectedItem: LPrecioImpresionLaserIntervalo = null;
    public selectedIDListaPapel: number;
    public selectedIDLista: number;
    public redondear: boolean;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private cp: CurrencyPipe,
        private pp: PercentPipe,
        private itemListaService: LPrecioImpresionLaserService,
        private itemService: LPrecioImpresionLaserIntervaloService) {
        this.isLoading = true;
        this.redondear = false;
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDLista = params['id'];
            this.selectedIDListaPapel = params['idimpresion'];
            this.getData();
        });
    }

    handleItemUpdated(user) {
        this.getData();
    }

    public getData() {
        const p0 = this.itemListaService.getByID(this.selectedIDListaPapel);
        const p1 = this.itemService.getListByIDLista(this.selectedIDListaPapel);
        Promise.all([
            p0, p1
        ]).then( ([result1, result2]) => {
            this.selectedItemLista = result1;
            this.redondear = (this.selectedItemLista.redondear > 0);
            this.data = result2;
            this.source = new LocalDataSource(this.data);
            this.isLoading = false;
            }
        );
    }

    onRedondear() {
        this.redondear = !this.redondear;
        this.selectedItemLista.redondear = (this.redondear) ? 1 : 0;
        this.itemListaService.save(this.selectedItemLista).then(
            response => {
                if (response) {
                    this.getData();
                } else {
                    this.showMessage('No se ha podido guardar los datos del ítem de lista de precio!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public volver() {
        this.router.navigate(['/lprecioimpresionlaser', this.selectedIDLista]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'descripcion', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            if (goBack) {
                                this.router.navigate(['/lprecioimpresionlaser']);
                            } else {
                                this.router.navigate(['/lprecioimpresionlaser', this.selectedIDLista]);
                            }
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
