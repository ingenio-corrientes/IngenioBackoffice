import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { LPrecioImpresionLaserIntervalosRoutingModule } from './lprecioimpresionlaserintervalos-routing.module';
import { LPrecioImpresionLaserIntervalosComponent } from './lprecioimpresionlaserintervalos.component';
import {
  ItemComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        LPrecioImpresionLaserIntervalosRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      LPrecioImpresionLaserIntervalosComponent,
      ItemComponent
    ]
})
export class LPrecioImpresionLaserIntervalosModule { }
