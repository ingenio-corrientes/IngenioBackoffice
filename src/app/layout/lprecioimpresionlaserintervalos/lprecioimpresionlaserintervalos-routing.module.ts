import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LPrecioImpresionLaserIntervalosComponent } from './lprecioimpresionlaserintervalos.component';

const routes: Routes = [
    { path: '', component: LPrecioImpresionLaserIntervalosComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LPrecioImpresionLaserIntervalosRoutingModule { }
