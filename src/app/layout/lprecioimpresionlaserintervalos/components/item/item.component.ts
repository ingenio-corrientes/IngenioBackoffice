import { Component, OnInit, Input, Output, OnChanges, EventEmitter, SimpleChange, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import {
    LPrecioImpresionLaserIntervalo, LPrecioImpresionLaserIntervaloService,
    LPrecioImpresionLaser, LPrecioImpresionLaserService,
    AppSettings} from '../../../../shared/';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    // styleUrls: ['./item.component.scss'],
    providers: [
        LPrecioImpresionLaserIntervaloService,
        LPrecioImpresionLaserService]
})
export class ItemComponent implements OnInit {
    @Input() selectedItem: LPrecioImpresionLaserIntervalo;
    @Output() itemUpdated = new EventEmitter();
    public modalTitle = 'Precio Impresión Láser';

    // variables de campos
    public preciofinal: number;

    // datos estructurados
    public selectedEntity: LPrecioImpresionLaserIntervalo;
    public selectedLPrecioImpresion: LPrecioImpresionLaser;
    private selectedIDLista: number;
    public selectedIDListaImpresion: number;

    // arreglos
    // public dataSustratos: Array<any> = Array<any>();
    // public dataSubtiposSustratos: Array<any> = Array<any>();
    // public dataSubtiposSustratosFilt: Array<any> = Array<any>();

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private itemsService: LPrecioImpresionLaserIntervaloService,
        private lprecioService: LPrecioImpresionLaserService) {
            this.isLoading = true;
            this.selectedLPrecioImpresion = new LPrecioImpresionLaser();
            this.cancelar();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDLista = params['id'];
            this.selectedIDListaImpresion = params['idimpresion'];
            this.getData();
        });
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue;
            this.onDataChange();
        }
    }

    public getData() {
        this.lprecioService.getByID(this.selectedIDListaImpresion).then(
            response => {
                if (response) {
                    this.selectedLPrecioImpresion = response;
                    this.isLoading = false;
                } else {
                    alert('No se ha podido obtener los datos de la lista de impresión láser!!');
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSave(content) {
        this.selectedEntity.idlprecioimpresionlaser = this.selectedIDListaImpresion;
        this.itemsService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                    this.itemUpdated.emit(true);
                    this.cancelar();
                } else {
                    alert('No se ha podido guardar los datos del intervalo de lista de precio!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.itemsService.delete(this.selectedEntity)
        .then(
            response => {
                if (response) {
                    this.itemUpdated.emit(true);
                    this.cancelar();
                } else {
                    alert('No se ha podido eliminar los datos del ítem de lista de precio!!');
                }
        }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new LPrecioImpresionLaserIntervalo();
        this.selectedEntity.maximo = 0;
        this.selectedEntity.porcentaje = 0;
        this.preciofinal = 0;
        this.itemUpdated.emit(true);
    }

    isNew() {
        return (this.selectedEntity !== null) ? (this.selectedEntity.id === -1) : (true);
    }

    public onDataChange() {
        if (this.selectedEntity.porcentaje === undefined) {
            return;
        }

        this.preciofinal = this.selectedLPrecioImpresion.precio * (1 + this.selectedEntity.porcentaje / 100);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/listaprecio']);
                        } else {
                            this.router.navigate(['/listaprecio', this.selectedIDListaImpresion]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
