import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresItPapeleriaComponent } from './presitpapeleria.component';

const routes: Routes = [
    { path: '', component: PresItPapeleriaComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PresItPapeleriaRoutingModule { }
