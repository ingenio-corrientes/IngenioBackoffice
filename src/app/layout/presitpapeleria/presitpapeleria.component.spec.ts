import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PresItPapeleriaComponent } from './presitpapeleria.component';
import {
  DataComponent,
  // ItemsComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

describe('PresItPapeleriaComponent', () => {
  let component: PresItPapeleriaComponent;
  let fixture: ComponentFixture<PresItPapeleriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        PresItPapeleriaComponent,
        DataComponent,
        // GastoItemsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresItPapeleriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
