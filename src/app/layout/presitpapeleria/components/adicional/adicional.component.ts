import { Component, OnInit, Input, Output, OnChanges, EventEmitter, SimpleChange, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    ListaPrecio, ListaPrecioService,
    PresupuestoItAdPapeleria, PresupuestoItAdPapeleriaService,
    TipoAdicional, TipoAdicionalService,
    LPrecioAdicional, LPrecioAdicionalService,
    Presupuesto, PresupuestoService,
    AppSettings, TWUtils} from '../../../../shared/';
@Component({
    selector: 'app-adicional',
    templateUrl: './adicional.component.html',
    styleUrls: ['./adicional.component.scss'],
    providers: [
        ListaPrecioService,
        PresupuestoItAdPapeleriaService,
        TipoAdicionalService,
        LPrecioAdicionalService,
        PresupuestoService]
})
export class AdicionalComponent implements OnInit, OnChanges {
    @Input() selectedItem: PresupuestoItAdPapeleria;
    @Output() itemUpdated = new EventEmitter();
    public modalTitle = 'Adicional';

    // variables de campos
    public subtotal;

    // datos estructurados
    public selectedEntity: PresupuestoItAdPapeleria;
    private selectedIDItemPapel: number;
    public selectedIDPresupuesto: number;
    public selectedLPrecio: ListaPrecio;
    selectedPresupuesto: Presupuesto;

    // arreglos
    public dataTiposAdicionales: Array<any> = Array<any>();
    public dataPrecios: Array<any> = Array<LPrecioAdicional>();

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private listaPrecioService: ListaPrecioService,
        private adicionalService: PresupuestoItAdPapeleriaService,
        private tiposAdicionalesService: TipoAdicionalService,
        private precioAdicionalService: LPrecioAdicionalService,
        private presupuestoService: PresupuestoService) {
            this.isLoading = true;
            this.subtotal = 0.00;
            this.selectedPresupuesto = new Presupuesto();
            this.cancelar();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDItemPapel = params['id'];
            this.selectedIDPresupuesto = +params['idpresupuesto'];
            this.getData();
        });
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue;
            this.onDataChange('');
        }
    }

    public getData() {
        this.isLoading = true;
        const p0 = this.listaPrecioService.getByIDPrsupuesto(this.selectedIDPresupuesto,
                AppSettings.TiposServicios.papeleria)
        const p1 = this.precioAdicionalService.getList();
        const p2 = this.presupuestoService.getByID(this.selectedIDPresupuesto);
        Promise.all([
            p0, p1, p2
        ]).then(([lista, precios, presupuesto]) => {
            this.selectedLPrecio = lista;
            this.dataPrecios = precios;
            this.selectedPresupuesto = presupuesto;
            this.getEntityData();
            this.isLoading = false;
            }
        );
    }

    getEntityData(): any {
        this.tiposAdicionalesService.getListByIDServicio(AppSettings.TiposServicios.papeleria).then(
            response => {
                if (response) {
                    this.dataTiposAdicionales = response;
                    this.isLoading = false;
                } else {
                    this.showMessage('No se ha podido obtener los datos de tipos de adicionales!!', false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSave() {
        this.isLoading = true;
        this.selectedEntity.idpresitpapeleria = this.selectedIDItemPapel;

        this.adicionalService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.itemUpdated.emit(true);
                    this.cancelar();
                } else {
                    this.showMessage('No se ha podido guardar los datos del adicional!!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    eliminar() {
        this.isLoading = true;
        this.adicionalService.delete(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        this.itemUpdated.emit(true);
                        this.cancelar();
                    } else {
                        this.showMessage('No se ha podido eliminar los datos del adicional!!', false);
                    }
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
    }

    cancelar() {
        this.selectedEntity = new PresupuestoItAdPapeleria();
    }

    isNew() {
      return (this.selectedEntity !== null) ? (this.selectedEntity.id === -1) : (true);
    }

    isRequired(field) {
        if  (!this.selectedEntity.idtipoadicional) {
            return false;
        }

        const tipo: TipoAdicional = this.dataTiposAdicionales.find(
            data =>
            +data.id === +this.selectedEntity.idtipoadicional);
        switch (field) {
            case 0:
                return tipo.anchomin > 0;
            case 1:
                return tipo.altomin > 0;
            case 2:
                return tipo.unidad > 0;
        }
    }

    isCero() {
        if (this.isRequired(2) && this.selectedEntity.unidades === 0) {
            return true;
        }
        return false;
    }

    onDataChange(value) {
        const idtipoadicional = this.selectedEntity.idtipoadicional;
        const unidad = (this.selectedEntity.unidades) ? this.selectedEntity.unidades : 0;

        const precio: LPrecioAdicional = this.dataPrecios.find(
            data  =>
            +data.idtipoadicional === +this.selectedEntity.idtipoadicional &&
            +data.idlistaprecio === +this.selectedLPrecio.id);
        if (precio === undefined) {
            this.subtotal = 0.00;
            this.selectedEntity.precio = 0.00;
            return;
        }
        this.selectedEntity.precio = precio.precio;
        this.subtotal = this.selectedEntity.unidades * precio.precio;
    }

    getEstadoHabilitante() {
        if (this.selectedIDPresupuesto === -1) {
            return AppSettings.EstadosPresupuesto.nuevo;
        }
        if (TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecentrega)) {
            return AppSettings.EstadosPresupuesto.entegado;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecaproduccion)) {
            return AppSettings.EstadosPresupuesto.en_produccion;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.emitido;
        }
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuestos']);
                        } else {
                            this.router.navigate(['/presitpapeleria', this.selectedIDPresupuesto, this.selectedIDItemPapel]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
