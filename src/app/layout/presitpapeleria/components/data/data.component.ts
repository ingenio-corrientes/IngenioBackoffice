import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    ListaPrecio, ListaPrecioService,
    PresupuestoItemPapeleria, PresupuestoItemPapeleriaService,
    TipoTrabajoPapeleria, TipoTrabajoPapeleriaService,
    GramajeService,
    TamanoService,
    TipoPapeleriaComercialService,
    SubtipoPapeleriaComercialService,
    TipoPapeleriaCorteService,
    LPrecioPapeleria, LPrecioPapeleriaService,
    LPrecioPapeleriaIntervalo, LPrecioPapeleriaIntervaloService,
    Presupuesto, PresupuestoService,
    TWUtils, AppSettings, TipoPapeleriaComercial, SubtipoPapeleriaComercial } from '../../../../shared';

@Component({
    selector: 'app-data',
    templateUrl: './data.component.html',
    styleUrls: ['./data.component.scss'],
    providers: [
        ListaPrecioService,
        PresupuestoItemPapeleriaService,
        TipoTrabajoPapeleriaService,
        GramajeService,
        TamanoService,
        TipoPapeleriaComercialService,
        SubtipoPapeleriaComercialService,
        TipoPapeleriaCorteService,
        LPrecioPapeleriaService,
        LPrecioPapeleriaIntervaloService,
        PresupuestoService]
    })

export class DataComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Presupuesto -> Papelería';

    // variables de campos
    public initialized: boolean;
    public selectedItemID: number;
    public selectedIDPresupuesto: number;
    public subtotal: number;
    public doblefaz: boolean;
    public color: boolean;

    // datos estructurados
    public selectedEntity: PresupuestoItemPapeleria;
    public selectedLPrecio: ListaPrecio;
    selectedPresupuesto: Presupuesto;

    // arreglos
    public dataTipoTrabajoPapeleria: Array<any> = Array<any>();
    public dataGramajes: Array<any> = Array<any>();
    public dataTamanos: Array<any> = Array<any>();
    public dataTipoPapeleriaComercial: Array<any> = Array<any>();
    public dataSubtipoPapeleriaComercial: Array<any> = Array<any>();
    public dataSubtipoPapeleriaComercialFilt: Array<any> = Array<any>();
    public dataTipoPapeleriaCorte: Array<any> = Array<any>();
    public dataPrecios: Array<any> = Array<LPrecioPapeleria>();
    public dataIntervalosPrecios: Array<any> = Array<LPrecioPapeleriaIntervalo>();

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private listaPrecioService: ListaPrecioService,
        private presupuestoitemService: PresupuestoItemPapeleriaService,
        private tipoTrabajoPapeleriaService: TipoTrabajoPapeleriaService,
        private gramajeService: GramajeService,
        private tamanoService: TamanoService,
        private tipoPapeleriaComercialService: TipoPapeleriaComercialService,
        private subtipoPapeleriaComercialService: SubtipoPapeleriaComercialService,
        private tipoTrabajoCorteService: TipoPapeleriaCorteService,
        private preciopapeleriaService: LPrecioPapeleriaService,
        private precioPapeleriaIntervaloService: LPrecioPapeleriaIntervaloService,
        private presupuestoService: PresupuestoService) {
            this.isLoading = true;
            this.selectedEntity = new PresupuestoItemPapeleria();
            this.subtotal = 0.00;
            this.doblefaz = false;
            this.color = true;
            this.initialized = false;
            this.selectedPresupuesto = new Presupuesto();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedItemID = +params['id'];
            this.selectedIDPresupuesto = +params['idpresupuesto'];
            this.getData();
        });
    }

    public getData() {
        this.isLoading = true;
        const p0 = this.listaPrecioService.getByIDPrsupuesto(this.selectedIDPresupuesto,
                AppSettings.TiposServicios.papeleria);
        const p1 = this.tipoTrabajoPapeleriaService.getList();
        const p2 = this.gramajeService.getList();
        const p3 = this.tamanoService.getList();
        const p4 = this.tipoPapeleriaComercialService.getList();
        const p5 = this.subtipoPapeleriaComercialService.getList();
        const p6 = this.tipoTrabajoCorteService.getList();
        const p7 = this.preciopapeleriaService.getList();
        const p8 = this.precioPapeleriaIntervaloService.getList();
        const p9 = this.presupuestoService.getByID(this.selectedIDPresupuesto);
        Promise.all([
            p0, p1, p2, p3, p4, p5, p6, p7, p8, p9
        ]).then(([lista, tipotrabajopapeleria, gramajes, tamanos,
                tipopacom, subtipopacom, tipopapcorte, precios,
                intervalos, presupuesto]) => {
            this.selectedLPrecio = lista;
            this.dataTipoTrabajoPapeleria = tipotrabajopapeleria;
            this.dataGramajes = gramajes;
            this.dataTamanos = tamanos;
            this.dataTipoPapeleriaComercial = tipopacom;
            this.dataSubtipoPapeleriaComercial = subtipopacom;
            this.dataTipoPapeleriaCorte = tipopapcorte;
            this.dataPrecios = precios;
            this.dataIntervalosPrecios = intervalos;
            this.selectedPresupuesto = presupuesto;
            this.getEntityData();
            this.isLoading = false;
            });
    }

    public getEntityData(): any {
        if (this.selectedItemID > -1) {
            this.presupuestoitemService.getByID(this.selectedItemID)
            .then(
                response => {
                    this.selectedEntity = response as PresupuestoItemPapeleria;
                    this.doblefaz = this.selectedEntity.doblefaz === 1;
                    this.color = this.selectedEntity.color === 1;
                    this.initialized = true;
                    this.onChangeTipoPapelComData();
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        this.isLoading = true;
        this.selectedEntity.idpresupuesto = this.selectedIDPresupuesto;
        this.selectedEntity.doblefaz = this.doblefaz ? 1 : 0;
        this.selectedEntity.color = this.color ? 1 : 0;
        if (!this.verifyData()) {
            this.isLoading = false;
            return;
        }
        this.presupuestoitemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    if (this.selectedItemID === -1) {
                        this.selectedItemID = Number(response);
                    }
                    this.showMessage('El ítem de papelería se guardó correctamente', false);
                } else {
                    this.showMessage('No se pudo guardar el ítem!!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    public onNuevo() {
        this.isLoading = true;
        this.selectedEntity.idpresupuesto = this.selectedIDPresupuesto;
        this.selectedEntity.doblefaz = this.doblefaz ? 1 : 0;
        this.selectedEntity.color = this.color ? 1 : 0;
        if (!this.verifyData()) {
            this.isLoading = false;
            return;
        }
        this.presupuestoitemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedEntity = new PresupuestoItemPapeleria();
                    this.showMessage('El ítem de papelería se guardó correctamente', false);
                    this.subtotal = 0;
                    this.router.navigate(['/presitpapeleria', this.selectedIDPresupuesto, -1]);
                } else {
                    this.showMessage('No se pudo guardar el ítem!!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    public noServicio(): boolean {
        return +this.selectedEntity.idtipotrabajopapeleria === -1;
    }

    public isComercial(): boolean {
        const tipo: TipoTrabajoPapeleria = this.dataTipoTrabajoPapeleria.find(
            x =>
            +x.id === +this.selectedEntity.idtipotrabajopapeleria);
        const retval = (typeof tipo !== 'undefined') ? +tipo.comercial === 1 : false;
        return retval;
    }

    public isConCorte(): boolean {
        const tipo: TipoTrabajoPapeleria = this.dataTipoTrabajoPapeleria.find(
            x =>
            +x.id === +this.selectedEntity.idtipotrabajopapeleria);
        const retval = (typeof tipo !== 'undefined') ? +tipo.concorte === 1 : false;
        return retval;
    }

    protected verifyData() {
        if (this.isComercial() && this.selectedEntity.unidades > 0) {
            return true;
        }
        if (this.isConCorte() && this.selectedEntity.pliegos > 0) {
            return true;
        }

        return false;
    }

    public isNew() {
        return (this.selectedItemID === -1);
    }

    public onTipoTrabajoPapeleriaChange() {
        this.selectedEntity.descripcion = '';
        this.selectedEntity.idgramaje =
            (this.dataGramajes.length > 0) ? +this.dataGramajes[0].id : 0;
        this.selectedEntity.idsubpapelcom =
            (this.dataSubtipoPapeleriaComercial.length > 0) ? +this.dataSubtipoPapeleriaComercial[0].id : 0;
        this.selectedEntity.idtamano =
            (this.dataTamanos.length > 0) ? +this.dataTamanos[0].id : 0;
        this.selectedEntity.idtipocorte =
            (this.dataTipoPapeleriaCorte.length > 0) ? +this.dataTipoPapeleriaCorte[0].id : 0;
        this.selectedEntity.idtipopapelcom =
            (this.dataTipoPapeleriaComercial.length > 0) ? +this.dataTipoPapeleriaComercial[0].id : 0;
        this.selectedEntity.pliegos = 0;
        this.selectedEntity.doblefaz = 0;
        this.selectedEntity.color = 0;
        this.selectedEntity.unidades = 0;
        const tipoTrabajoPapeleria: TipoTrabajoPapeleria =
            this.findObjectInArray(this.dataTipoTrabajoPapeleria, this.selectedEntity.idtipotrabajopapeleria);
        if (+tipoTrabajoPapeleria.comercial === 1) {
            this.onChangeTipoPapelComData();
        }
    }

    public onDataChange(value) {
        const tipoTrabajoPapeleria: TipoTrabajoPapeleria =
            this.findObjectInArray(this.dataTipoTrabajoPapeleria, this.selectedEntity.idtipotrabajopapeleria);

        if (+tipoTrabajoPapeleria.comercial === 1) {
            const tipopapelcom: TipoPapeleriaComercial =
                this.findObjectInArray(this.dataTipoPapeleriaComercial, this.selectedEntity.idtipopapelcom);
            const subtipopapelcom: SubtipoPapeleriaComercial =
                this.findObjectInArray(this.dataSubtipoPapeleriaComercial, this.selectedEntity.idsubpapelcom);

            this.selectedEntity.descripcion =
                ((this.color) ? ('Color|') : 'Blanco y negro|') +
                ((this.doblefaz) ? ('Doble faz|') : 'Simple faz|') +
                ((tipopapelcom) ? (tipopapelcom.tipopapeleriacomercial + '|') : '') +
                ((subtipopapelcom) ? (subtipopapelcom.subtipopapeleriacomercial) : '');
            this.calculatePrecio(1);
        } else {
            // this.selectedEntity.descripcion =
            //     ((this.color) ? ('Color|') : 'Blanco y negro|') +
            //     ((this.doblefaz) ? ('Doble faz|') : 'Simple faz|') +
            //     ((gramaje) ? (gramaje.gramaje + '|') : '') +
            //     ((concorte) ? (concorte.tipocorte + '|') : '') +
            // this.selectedEntity.pliegos.toString();
                this.calculatePrecio(2);
        }
    }

    private calculatePrecio(tipoTrabajo: number) {
        const dataPreciosFilt = this.dataPrecios.filter(
            data  =>
            +data.idlistaprecio === +this.selectedLPrecio.id &&
            +data.idtipopapeleriatrabajo === +this.selectedEntity.idtipotrabajopapeleria);

        if (dataPreciosFilt.length < 1) {
            return;
        }

        let precio = new LPrecioPapeleria;
        if (tipoTrabajo === 1) {
            this.selectedEntity.pliegos = 1;
            precio = dataPreciosFilt.find(
                data  =>
                // +data.idgramaje === +this.selectedEntity.idgramaje &&
                +data.idsubtipopapelcom === +this.selectedEntity.idsubpapelcom // &&
                // +data.idtamano === +this.selectedEntity.idtamano
                );
            if (precio === undefined) {
                return;
            }
        } else {
            this.selectedEntity.unidades = 1;
            precio = dataPreciosFilt.find(
                data  =>
                +data.idgramaje === +this.selectedEntity.idgramaje); // &&
                // +data.doblefaz === +this.selectedEntity.doblefaz &&
                // +data.idtipocorte === +this.selectedEntity.idtipocorte);
        }

        let intervalo: LPrecioPapeleriaIntervalo;
        for (let i = 0; i < this.dataIntervalosPrecios.length; i++) {
            const data = this.dataIntervalosPrecios[i];

            if (+data.idlpreciopapeleria === +precio.id) {
                intervalo = data;
                if (tipoTrabajo === 1) {
                    if (+data.maximo >= +this.selectedEntity.unidades) {
                        break;
                    }
                } else {
                    if (+data.maximo >= +this.selectedEntity.pliegos) {
                        break;
                    }
                }
            }
        }

        let porcentaje = 0;
        if (intervalo !== undefined) {
            porcentaje = +precio.precio * (+intervalo.porcentaje) / 100;
        }

        if (tipoTrabajo === 1) {
            precio.color = 0;
            precio.doblefaz = 0;
        }
        const precioBruto = +precio.precio + porcentaje;
        const precioBrutoC = precioBruto * ((this.color ? 0 :  +precio.color / 100)  + 1);
        const precioBrutoCD = precioBrutoC * ((this.doblefaz ? +precio.doblefaz / 100  : 0) + 1);

        this.selectedEntity.precio = precioBrutoCD;

        this.subtotal = this.selectedEntity.precio * this.selectedEntity.unidades * this.selectedEntity.pliegos;
    }

    protected findObjectInArray(arr: Array<any>, idtodind: number) {
        const object = arr.find(
            data =>
            +data.id === +idtodind);
        return object;
    }

    public onChangeTipoPapelComData() {
        const idtipopapelcom = this.selectedEntity.idtipopapelcom;
        this.dataSubtipoPapeleriaComercialFilt = this.dataSubtipoPapeleriaComercial.filter(
            data =>
            +data.idtipopapeleriacomercial === +idtipopapelcom);
        if (this.dataSubtipoPapeleriaComercialFilt.length > 0) {
            this.selectedEntity.idsubpapelcom = this.dataSubtipoPapeleriaComercialFilt[0].id;
            this.onDataChange('');
        }
    }

    hasNoSubtipos() {
        return this.dataSubtipoPapeleriaComercialFilt.length === 0;
    }

    hasNoPriceList() {
        const dataPreciosFilt = this.dataPrecios.filter(
            data  =>
            +data.idlistaprecio === +this.selectedLPrecio.id &&
            +data.idtipopapeleriatrabajo === +this.selectedEntity.idtipotrabajopapeleria);
        return dataPreciosFilt.length === 0;
    }

    onDobleFazChage() {
        this.doblefaz = !this.doblefaz;
        this.onDataChange('');
    }

    onColorChage() {
        this.color = !this.color;
        this.onDataChange('');
    }

    isCero() {
        if (this.selectedEntity.idtipotrabajopapeleria === undefined ||
            this.selectedEntity.idtipotrabajopapeleria === -1) {
            return true;
        }

        const tipoTrabajoPapeleria: TipoTrabajoPapeleria =
            this.findObjectInArray(this.dataTipoTrabajoPapeleria, this.selectedEntity.idtipotrabajopapeleria);

        if (+this.selectedEntity.precio === 0) {
            return true;
        }

        if (+this.subtotal === 0) {
            return true;
        }

        if (+tipoTrabajoPapeleria.comercial === 1) {
            return (this.selectedEntity.unidades !== undefined) ? (+this.selectedEntity.unidades === 0) : true;
        } else {
            return (this.selectedEntity.pliegos !== undefined) ? (+this.selectedEntity.pliegos === 0) : true;
        }
    }

    public eliminar() {
        this.isLoading = true;
        this.presupuestoitemService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.presupuestoitemService.ENTITY_DELETED) {
                    this.showMessage('El ítem de papeliría se ha eliminado correctamente', true);
                } else  if (+response === this.presupuestoitemService.ERROR_DUPLICATED) {
                    this.showMessage('El ítem de papeliría está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El ítem de papeliría no se ha podido eliminar!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    hasLista() {
        if (this.selectedLPrecio !== undefined) {
            if (this.selectedLPrecio.id > -1) {
                return true;
            }
        }
        return false;
    }

    getEstadoHabilitante() {
        if (this.selectedIDPresupuesto === -1) {
            return AppSettings.EstadosPresupuesto.nuevo;
        }
        if (TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecentrega)) {
            return AppSettings.EstadosPresupuesto.entegado;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecaproduccion)) {
            return AppSettings.EstadosPresupuesto.en_produccion;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.emitido;
        }
    }

    canSave(): boolean {
        let can = !this.isCero() || this.hasLista() || this.getEstadoHabilitante() < AppSettings.EstadosPresupuesto.emitido;
        if (can) {
            can = this.isComercial() ? !this.hasNoSubtipos() : can;
            can = this.isConCorte() ? !this.hasNoPriceList() : can;
        }
        return can;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuesto', this.selectedIDPresupuesto]);
                        } else {
                            this.router.navigate(['/presitpapeleria', this.selectedIDPresupuesto, this.selectedItemID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        this.isLoading = false;
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
