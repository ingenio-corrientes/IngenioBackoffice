import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { PresItPapeleriaRoutingModule } from './presitpapeleria-routing.module';
import { PresItPapeleriaComponent } from './presitpapeleria.component';
import {
  DataComponent,
  AdicionalesComponent,
  AdicionalComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        PresItPapeleriaRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      PresItPapeleriaComponent,
      DataComponent,
      AdicionalesComponent,
      AdicionalComponent
    ]
})
export class PresItPapeleriaModule { }
