import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProveedoresCtaCtesComponent } from './proveedoresctactes.component';

const routes: Routes = [
    { path: '', component: ProveedoresCtaCtesComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProveedoresCtaCtesRoutingModule { }
