import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { CurrencyPipe } from '@angular/common';
import { DatePipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { ModuloXRolService,
    ProveedorService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './proveedoresctactes.component.html',
    styleUrls: ['./proveedoresctactes.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        ProveedorService,
        ModuloXRolService,
        CurrencyPipe
    ]
})

export class ProveedoresCtaCtesComponent implements OnInit {
    readonly IDMODULO = 11;
    public modalMessage: string;
    public modalTitle = '';
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            tipocomprobante: { title: 'Tipo de Comprobante', filter: false, },
            numero: { title: 'Número', filter: false, },
            fecemision: {
                title: 'Fecha de Emisión', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }},
            saldo: {
                title: 'Saldo',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return this.cp.transform(value, 'USD', true, '1.2-2');
                }
            },
            total: {
                title: 'Total',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return this.cp.transform(value, 'USD', true, '1.2-2');
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public dataProveedores: Array<any> = Array<any>();
    public idProveedor: number;
    public data: Array<any> = Array<any>();
    loggedId: number;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private datePipe: DatePipe,
        private cp: CurrencyPipe,
        private moduloXRolService: ModuloXRolService,
        private proveedorService:  ProveedorService) {
        this.source = new LocalDataSource(this.data);
        this.idProveedor = 0;
    }

    public ngOnInit(): void {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedId = +jsonData.id;
        this.evalPermisos();
        this.getProveedores();
    }

    evalPermisos() {
        this.moduloXRolService.getListByIDUsuarioIDModulo(this.loggedId, this.IDMODULO)
            .then(response => {
                if (response) {
                    if (+response.ver !== 1) {
                        this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    }
                    this.isLoading = false;
                } else {
                    this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    private getProveedores() {
        this.proveedorService.getList().then(
            response => {
                if (response) {
                    this.dataProveedores = response;
                } else {
                    alert('No se han podido cargar datos de proveedores!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onGenerar() {
        this.proveedorService.getCuentaCorrienteProvedor(this.idProveedor).then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se han podido cargar datos de cuentascorrientes!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'tipocomprobante', search: query, },
            { field: 'numero', search: query, },
            { field: 'fecemision', search: query, },
            { field: 'saldo', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        // this.selectedEntity = event.data;
        // this.router.navigate(['/cuentacorriente', event.data.id]);
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                        }
                        if (goBack) {
                            this.router.navigate(['/dashboard']);
                        } else {
                            this.router.navigate(['/dashboard']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
