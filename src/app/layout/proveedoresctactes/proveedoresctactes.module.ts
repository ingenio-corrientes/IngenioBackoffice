import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { ProveedoresCtaCtesRoutingModule } from './proveedoresctactes-routing.module';
import { ProveedoresCtaCtesComponent } from './proveedoresctactes.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ProveedoresCtaCtesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule,
        Ng2SmartTableModule
    ],
    declarations: [ProveedoresCtaCtesComponent]
})
export class ProveedoresCtaCtesModule { }
