import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { PreciosRoutingModule } from './precios-routing.module';
import { PreciosComponent } from './precios.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        PreciosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule,
        Ng2SmartTableModule
    ],
    declarations: [PreciosComponent]
})
export class PreciosModule { }
