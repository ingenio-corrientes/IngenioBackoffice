import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Precio, PrecioService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './precios.component.html',
    styleUrls: ['./precios.component.scss'],
    animations: [routerTransition()],
    providers: [
        PrecioService,
    ]
})

export class PreciosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Código', filter: false, },
            precio: { title: 'Precio', filter: false, },
            servicio: { title: 'Tipo de Servicio', filter: false, },
            tipoclient: { title: 'Tipo de Cliente', filter: false, },
            desyreca: { title: 'Descuento y Recargo', filter: false, },
            monto: { title: 'Monto', filter: false, },
            idlistabase: { title: 'Lista base', filter: false, },
            descripcion: { title: 'Descripción', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public modalMessage: string;
    public modalTitle = 'Precios';
    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Precio;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();

    isLoading = false;

    public constructor(public router: Router, private route: ActivatedRoute,
        private precioService: PrecioService,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef) {
            this.isLoading = true;
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.precioService.getList()
        .then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de precios!!', true);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/precio', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query, },
            { field: 'precio', search: query, },
            { field: 'tipoclient', search: query, },
            { field: 'desyreca', search: query, },
            { field: 'monto', search: query, },
            { field: 'idlistabase', search: query, },
            { field: 'descripcion', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/precio', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/precios']);
                        } else {
                            this.router.navigate(['/precios']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
