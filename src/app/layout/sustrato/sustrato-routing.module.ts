import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SustratoComponent } from './sustrato.component';

const routes: Routes = [
    { path: '', component: SustratoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SustratoRoutingModule { }
