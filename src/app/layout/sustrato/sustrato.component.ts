import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {Sustrato, SustratoService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './sustrato.component.html',
    styleUrls: ['./sustrato.component.scss'],
    animations: [routerTransition()],
    providers: [SustratoService]
})

export class SustratoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Sustrato';

    public selectedID: number;
    public selectedEntity: Sustrato;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private sustratoService: SustratoService) {
        this.selectedEntity = new Sustrato();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.sustratoService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as Sustrato;
              })
          .catch(e => this.handleError(e));
      }
    }

    public onSave() {
        this.sustratoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El sustrato se guardó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido obtener los datos del sustrato!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.sustratoService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.sustratoService.ENTITY_DELETED) {
                    this.showMessage('El sustrato se ha eliminado correctamente', true);
                } else  if (+response === this.sustratoService.ERROR_FOREING_KEY) {
                    this.showMessage('El sustrato está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El sustrato no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/sustratos']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/sustratos']);
                        } else {
                            this.router.navigate(['/sustrato', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
