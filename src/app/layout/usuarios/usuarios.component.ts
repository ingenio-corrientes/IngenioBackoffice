import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { LocalDataSource } from 'ng2-smart-table';

import { ModuloXRolService, Usuario, UsuarioService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './usuarios.component.html',
    styleUrls: ['./usuarios.component.scss'],
    animations: [routerTransition()],
    providers: [
        UsuarioService,
        ModuloXRolService
    ]
})

export class UsuariosComponent implements OnInit {
    readonly IDMODULO = 2;
    // public rows: Array<any> = [];
    // public columns: Array<any> = [
    //     { title: 'Código', name: 'id', sort: '' },
    //     {
    //         title: 'Usuario', name: 'usuario', sort: '',
    //         filtering: { filterString: '', placeholder: 'Filtrar por usuario' }
    //     }
    // ];

    // public page = 1;
    // public itemsPerPage = 10;
    // public maxSize = 5;
    // public numPages = 1;
    // public length = 0;
    // public config: any = {
    //     paging: true,
    //     sorting: { columns: this.columns },
    //     filtering: { filterString: '' },
    //     className: ['table-striped', 'table-bordered']
    // };
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Código', filter: false, },
            usuario: { title: 'Usuario', filter: false, },
            apellido: { title: 'Apellidos', filter: false, },
            nombre: { title: 'Nombres', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'Usuarios';

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Usuario;
    public error = false;
    public loading = false;
    public data: Array<any> = Array<any>();
    loggedId: number;

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private usuarioService: UsuarioService,
        private dataService: UsuarioService,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private moduloXRolService: ModuloXRolService) {
            // this.length = this.data.length;
            this.source = new LocalDataSource(this.data);
            this.isLoading = true;
    }

    public ngOnInit(): void {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedId = +jsonData.id;
        this.evalPermisos();
        this.usuarioService.getList().then(
            response => {
                if (response) {
                    this.loading = false;
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    // this.length = this.data.length;
                    // this.onChangeTable(this.config);
                } else {
                    this.error = true;
                    this.loading = false;
                    // this.length = 0;
                }
            }
        ).catch(e => this.handleError(e));
    }

    evalPermisos() {
        this.moduloXRolService.getListByIDUsuarioIDModulo(this.loggedId, this.IDMODULO)
            .then(response => {
                if (response) {
                    if (+response.ver !== 1) {
                        this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    }
                    this.isLoading = false;
                } else {
                    this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    this.isLoading = false;
                } if (+response.ver === 1) {
                    this.dataService.getList().then(
                    lresponse => {
                        if (lresponse) {
                            this.data = lresponse;
                            this.source = new LocalDataSource(this.data);
                            this.source.setSort([{ field: 'servicio', direction: 'asc' }]);
                            this.isLoading = false;
                        } else {
                            this.showMessage('No se han podido cargar los datos!!', false, false);
                            this.isLoading = false;
                        }
                    }
                ).catch(e => this.handleError(e));
                }
            })
        .catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/usuario', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query, },
            { field: 'usuario', search: query, },
            { field: 'apellido', search: query, },
            { field: 'nombre', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/usuario', event.data.id]);
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                            return;
                        }
                        if (goBack) {
                            this.router.navigate(['/usuarios']);
                        } else {
                            this.router.navigate(['/usuarios']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    // public onCellClick(data: any): any {
    //     this.selectedEntity = data.row;
    //     this.router.navigate(['/usuario', data.row.id]);
    // }

    // public changePage(page: any, data: Array<any> = this.data): Array<any> {
    //     const start = (page.page - 1) * page.itemsPerPage;
    //     const end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    //     return data.slice(start, end);
    // }

    // public changeSort(data: any, config: any): any {
    //     if (!config.sorting) {
    //         return data;
    //     }

    //     const columns = this.config.sorting.columns || [];
    //     let columnName: string = void 0;
    //     let sort: string = void 0;

    //     for (let i = 0; i < columns.length; i++) {
    //         if (columns[i].sort !== '' && columns[i].sort !== false) {
    //             columnName = columns[i].name;
    //             sort = columns[i].sort;
    //         }
    //     }

    //     if (!columnName) {
    //         return data;
    //     }

    //     // simple sorting
    //     return data.sort((previous: any, current: any) => {
    //         if (previous[columnName] > current[columnName]) {
    //             return sort === 'desc' ? -1 : 1;
    //         } else if (previous[columnName] < current[columnName]) {
    //             return sort === 'asc' ? -1 : 1;
    //         }
    //         return 0;
    //     });
    // }

    // public changeFilter(data: any, config: any): any {
    //     let filteredData: Array<any> = data;
    //     this.columns.forEach((column: any) => {
    //         if (column.filtering) {
    //             filteredData = filteredData.filter((item: any) => {
    //                 return item[column.name].toLowerCase().match(column.filtering.filterString);
    //             });
    //         }
    //     });

    //     if (!config.filtering) {
    //         return filteredData;
    //     }

    //     if (config.filtering.columnName) {
    //         return filteredData.filter((item: any) =>
    //             item[config.filtering.columnName].toLowerCase().match(this.config.filtering.filterString));
    //     }

    //     const tempArray: Array<any> = [];
    //     filteredData.forEach((item: any) => {
    //         let flag = false;
    //         this.columns.forEach((column: any) => {
    //             if (item[column.name].toString().toLowerCase().match(this.config.filtering.filterString)) {
    //                 flag = true;
    //             }
    //         });
    //         if (flag) {
    //             tempArray.push(item);
    //         }
    //     });
    //     filteredData = tempArray;

    //     return filteredData;
    // }

    // public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    //     if (config.filtering) {
    //         Object.assign(this.config.filtering, config.filtering);
    //     }

    //     if (config.sorting) {
    //         Object.assign(this.config.sorting, config.sorting);
    //     }

    //     const filteredData = this.changeFilter(this.data, this.config);
    //     const sortedData = this.changeSort(filteredData, this.config);
    //     this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    //     this.length = sortedData.length;
    // }
}
