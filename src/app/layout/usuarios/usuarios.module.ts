import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// import { PaginationModule } from 'ng2-bootstrap';
// import { Ng2TableModule } from 'ng2-table';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent } from './usuarios.component';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        FormsModule,
        // Ng2TableModule,
        // PaginationModule.forRoot(),
        Ng2SmartTableModule,
        CommonModule,
        UsuariosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [UsuariosComponent]
})
export class UsuariosModule { }
