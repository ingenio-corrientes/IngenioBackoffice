import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// import { PaginationModule } from 'ng2-bootstrap';
// import { Ng2TableModule } from 'ng2-table';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { RetiroefectivosRoutingModule } from './retiroefectivos-routing.module';
import { RetiroefectivosComponent } from './retiroefectivos.component';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        FormsModule,
        // Ng2TableModule,
        // PaginationModule.forRoot(),
        Ng2SmartTableModule,
        CommonModule,
        RetiroefectivosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [RetiroefectivosComponent]
})
export class RetiroefectivosModule { }
