import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RetiroefectivosComponent } from './retiroefectivos.component';

const routes: Routes = [
    { path: '', component: RetiroefectivosComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RetiroefectivosRoutingModule { }
