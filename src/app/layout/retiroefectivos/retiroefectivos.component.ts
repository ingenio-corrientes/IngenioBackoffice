import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { RetiroEfectivo, RetiroEfectivoService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './retiroefectivos.component.html',
    styleUrls: ['./retiroefectivos.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        RetiroEfectivoService,
    ]
})

export class RetiroefectivosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            fecha: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            responsable: { title: 'Responsable', filter: false, },
            concepto: { title: 'Concepto', filter: false, },
            monto: { title: 'Monto', filter: false,
            type: 'html',
            valuePrepareFunction: (value) => {
                return `<p class="cell_right">$ ` + value + '</p>';
                }
            },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'RetirosEfectivos';

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: RetiroEfectivo;
    public data: Array<any> = Array<any>();

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private datePipe: DatePipe,
        private retiroefectivoService: RetiroEfectivoService) {
            this.isLoading = true;
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.retiroefectivoService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.source.setSort([{ field: 'fecha', direction: 'asc' }]);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido obtener los datos de retiros de efectivo', false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/retiroefectivo', -1]);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/retiroefectivo', event.data.id]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'fecha', search: query, },
            { field: 'responsable', search: query, },
            { field: 'concepto', search: query, },
            { field: 'monto', search: query, }
        ], false);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/retiroefectivos']);
                        } else {
                            this.router.navigate(['/retiroefectivos']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
