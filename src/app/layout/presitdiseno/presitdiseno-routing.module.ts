import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresItDisenoComponent } from './presitdiseno.component';

const routes: Routes = [
    { path: '', component: PresItDisenoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PresItDisenoRoutingModule { }
