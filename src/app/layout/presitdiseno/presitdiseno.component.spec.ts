import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PresItDisenoComponent } from './presitdiseno.component';
import {
  DataComponent,
  // ItemsComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

describe('PresItDisenoComponent', () => {
  let component: PresItDisenoComponent;
  let fixture: ComponentFixture<PresItDisenoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        PresItDisenoComponent,
        DataComponent,
        // GastoItemsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresItDisenoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
