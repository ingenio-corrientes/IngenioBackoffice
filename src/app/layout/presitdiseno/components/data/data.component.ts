import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    ListaPrecio, ListaPrecioService,
    PresupuestoItemDiseno, PresupuestoItemDisenoService,
    TipoDiseno, TipoDisenoService,
    SubtipoDiseno, SubtipoDisenoService,
    LPrecioDiseno, LPrecioDisenoService,
    Presupuesto, PresupuestoService,
    AppSettings, TWUtils} from '../../../../shared';

@Component({
    selector: 'app-data',
    templateUrl: './data.component.html',
    styleUrls: ['./data.component.scss'],
    providers: [
        ListaPrecioService,
        PresupuestoItemDisenoService,
        TipoDisenoService,
        SubtipoDisenoService,
        LPrecioDisenoService,
        PresupuestoService]
    })

export class DataComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Presupuesto -> Diseño';

    // variables de campos
    public initialized: boolean;
    public selectedItemID: number;
    public selectedIDPresupuesto: number;
    public subtotal: number;
    sinPrecios: boolean;

    // datos estructurados
    public selectedEntity: PresupuestoItemDiseno;
    public selectedLPrecio: ListaPrecio;
    selectedPresupuesto: Presupuesto;

    // arreglos
    public dataTiposDisenos: Array<any> = Array<any>();
    public dataSubtiposDisenos: Array<any> = Array<any>();
    public dataSubtiposDisenosFilt: Array<any> = Array<any>();
    public dataPrecios: Array<any> = Array<LPrecioDiseno>();

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private listaPrecioService: ListaPrecioService,
        private presupuestoitemService: PresupuestoItemDisenoService,
        private tipoDisenoService: TipoDisenoService,
        private subtipoDisenoService: SubtipoDisenoService,
        private precioDisenoService: LPrecioDisenoService,
        private presupuestoService: PresupuestoService) {
            this.isLoading = true;
            this.selectedEntity = new PresupuestoItemDiseno();
            this.subtotal = 0.00;
            this.initialized = false;
            this.sinPrecios = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedItemID = +params['id'];
            this.selectedIDPresupuesto = +params['idpresupuesto'];
            this.getData();
        });
    }

    public getData() {
        const p0 = this.listaPrecioService.getByIDPrsupuesto(this.selectedIDPresupuesto,
                AppSettings.TiposServicios.diseno);
        const p1 = this.tipoDisenoService.getList();
        const p2 = this.subtipoDisenoService.getList();
        const p3 = this.precioDisenoService.getList();
        const p4 = this.presupuestoService.getByID(this.selectedIDPresupuesto);
        this.isLoading = true;
        Promise.all([
            p0, p1, p2, p3, p4
        ]).then( ([lista, tiposdisenos, subtiposdiseno,
            precios, presupuesto]) => {
            this.selectedLPrecio = lista;
            this.dataTiposDisenos = tiposdisenos;
            this.dataSubtiposDisenos = subtiposdiseno;
            this.dataPrecios = precios;
            this.selectedPresupuesto = presupuesto;
            this.getEntityData();
            this.isLoading = false;
        });
    }

    public getEntityData(): any {
      if (this.selectedItemID > -1) {
          this.presupuestoitemService.getByID(this.selectedItemID)
          .then(
                response => {
                    this.selectedEntity = response as PresupuestoItemDiseno;
                    this.initialized = true;
                    this.onTipoDisenoChange();
                    this.isLoading = false;
                })
          .catch(e => this.handleError(e));
        } else {
            this.initialized = true;
            this.isLoading = false;
        }
    }

    public onSave() {
        this.isLoading = true;
        this.selectedEntity.idpresupuesto = this.selectedIDPresupuesto;
        if (!this.verifyData()) {
            this.isLoading = false;
            return;
        }
        this.presupuestoitemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    if (this.selectedItemID === -1) {
                        this.selectedItemID = Number(response);
                    }
                    this.showMessage('El ítem de diseño se guardó correctamente', true);
                } else {
                    this.showMessage('No se pudo guardar el ítem!!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    public onNuevo() {
        this.isLoading = true;
        this.selectedEntity.idpresupuesto = this.selectedIDPresupuesto;
        if (!this.verifyData()) {
            this.isLoading = false;
            return;
        }
        this.presupuestoitemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.showMessage('El ítem de diseño se guardó correctamente', false);
                    this.selectedEntity = new PresupuestoItemDiseno();
                    this.subtotal = 0;
                    this.router.navigate(['/presitdiseno', this.selectedIDPresupuesto, -1]);
                } else {
                    this.showMessage('No se pudo guardar el ítem!!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    protected verifyData() {
        return true;
    }

    public isNew() {
        return (this.selectedItemID === -1);
    }

    isCero() {
        return (this.selectedEntity.cantidad !== undefined) ? (+this.selectedEntity.cantidad === 0) : true;
    }

    protected findObjectInArray(arr: Array<any>, idtodind: number) {
        const object = arr.find(
            data =>
            +data.id === +idtodind);
        return object;
    }

    public onTipoDisenoChange() {
        if (!this.initialized) {
            return;
        }
        const idtipodise = this.selectedEntity.idtipodiseno;
        this.dataSubtiposDisenosFilt = this.dataSubtiposDisenos.filter(
            data =>
            +data.idtipodiseno === +idtipodise);
        if (this.dataSubtiposDisenosFilt.length === 0) {
            this.showMessage('El tipo de diseño seleccionado no posee subtipo. Por favor agregue subtipo.', false);
            this.selectedEntity.idsubtipodiseno = undefined;
            this.selectedEntity.precio = 0;
            this.subtotal = 0;
            this.selectedEntity.descripcion = '';
            return;
        }
        this.selectedEntity.idsubtipodiseno = this.dataSubtiposDisenosFilt[0].id;
        this.onDataChange('');
    }

    public onDataChange(value) {
        if (!this.initialized) {
            return;
        }
        const tipoDiseno: TipoDiseno =
            this.findObjectInArray(this.dataTiposDisenos, this.selectedEntity.idtipodiseno);
        const subtipoDiseno: SubtipoDiseno =
            this.findObjectInArray(this.dataSubtiposDisenos, this.selectedEntity.idsubtipodiseno);

        this.selectedEntity.descripcion =
            ((tipoDiseno) ? (tipoDiseno.tipodiseno + '|') : '') +
            ((subtipoDiseno) ? (subtipoDiseno.subtipodiseno + '|') : '') +
            ((this.selectedEntity.cantidad) ? (this.selectedEntity.cantidad) : '');

        const precio: LPrecioDiseno = this.dataPrecios.find(
            data  =>
            +data.idsubtipodiseno === +this.selectedEntity.idsubtipodiseno &&
            +data.idlistaprecio === +this.selectedLPrecio.id);
        if (precio) {
            this.selectedEntity.precio = precio.precio;
            this.subtotal = this.selectedEntity.cantidad * precio.precio;
            this.sinPrecios = false;
        } else {
            this.sinPrecios = true;
            this.selectedEntity.precio = 0;
            this.subtotal = 0;
        }
    }

    hasLista() {
        if (this.selectedLPrecio !== undefined) {
            if (this.selectedLPrecio.id > -1) {
                return true;
            }
        }
        return false;
    }

    sinPrecio(): boolean {
        return this.sinPrecios;
    }

    public eliminar() {
        this.isLoading = true;
        this.presupuestoitemService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.presupuestoitemService.ENTITY_DELETED) {
                    this.showMessage('El ítem de diseño se ha eliminado correctamente', true);
                } else  if (+response === this.presupuestoitemService.ERROR_FOREING_KEY) {
                    this.showMessage('El ítem de diseño está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El ítem de diseño no se ha podido eliminar!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    getEstadoHabilitante() {
        if (this.selectedIDPresupuesto === -1) {
            return AppSettings.EstadosPresupuesto.nuevo;
        }
        if (TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecentrega)) {
            return AppSettings.EstadosPresupuesto.entegado;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecaproduccion)) {
            return AppSettings.EstadosPresupuesto.en_produccion;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.emitido;
        }
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuesto', this.selectedIDPresupuesto]);
                        } else {
                            this.router.navigate(['/presitdiseno', this.selectedIDPresupuesto, this.selectedItemID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        this.isLoading = false;
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
