import { Component, OnInit, Input, Output, EventEmitter, SimpleChange, ViewContainerRef, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Sustrato, SustratoService,
    SubtipoSustrato, SubtipoSustratoService,
    EventDataChange, AppSettings
} from '../../../../shared/';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    providers: [
        SustratoService,
        SubtipoSustratoService]
})
export class ItemComponent implements OnInit, OnChanges {
    @Input() selectedItem: SubtipoSustrato;
    @Output() itemUpdated = new EventEmitter();
    public modalMessage: string;
    public modalTitle = 'Subtipo de Sustrato';
    private actualEvent = AppSettings.EventType.new;

    // datos estructurados
    public selectedEntity: SubtipoSustrato;
    // private selectedIDLista: number;

    // arreglos
    public dataSustratos: Array<any> = Array<any>();
    public dataSubtiposSustratos: Array<any> = Array<any>();
    public dataSubtiposSustratosFilt: Array<any> = Array<any>();

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private itemsService: SubtipoSustratoService,
        private sustratoService: SustratoService,
        private subtipoSustratoService: SubtipoSustratoService) {
        this.cancelar();
        this.isLoading = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            // this.selectedIDLista = params['id'];
            this.getSustratos();
        });
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.actualEvent = AppSettings.EventType.update;
            this.selectedEntity = changes['selectedItem'].currentValue;
        }
    }

    public getSustratos() {
        this.sustratoService.getList()
        .then(
            response => {
                this.dataSustratos = response;
                this.isLoading = false;
            })
        .catch(e => this.handleError(e));
    }

    onSave(content) {
        const auxSelectedEntity = Object.assign({}, this.selectedEntity);
        const eventType = (this.isNew()) ? AppSettings.EventType.new : AppSettings.EventType.update;
        this.itemsService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    auxSelectedEntity.id = +response;
                    const sustrato: Sustrato = this.dataSustratos.find(
                        data =>
                        +data.id === +auxSelectedEntity.idsustrato);
                    auxSelectedEntity.sustrato = sustrato.sustrato;

                    const selectedItem = new EventDataChange();
                    selectedItem.eventType = eventType;
                    selectedItem.object = auxSelectedEntity;
                    this.itemUpdated.emit(selectedItem);
                    this.cancelar();
                } else {
                    this.showMessage('No se ha podido guardar los datos del subtipo de sustrato!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar(content) {
        this.itemsService.delete(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        const selectedItem = new EventDataChange();
                        selectedItem.eventType = AppSettings.EventType.delete;
                        selectedItem.object = this.selectedEntity;
                        this.itemUpdated.emit(selectedItem);
                        this.cancelar();
                    } else {
                        this.showMessage('No se ha podido eliminar los datos del subtipo de sustrato!!', false);
                    }
                }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new SubtipoSustrato();
        const selectedItem = new EventDataChange();
        selectedItem.eventType = AppSettings.EventType.cancel;
        selectedItem.object = this.selectedEntity;
        this.itemUpdated.emit(selectedItem);
    }

    isNew() {
        if (this.selectedEntity !== null) {
            if (this.selectedEntity.id > -1) {
                return false;
            }
        }
        return true;
        // return (this.selectedEntity === null) ? (+this.selectedEntity.id === -1) : (true);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            // if (goBack) {
                            //     this.router.navigate(['/lpreciocarteleria']);
                            // } else {
                            //     this.router.navigate(['/lpreciocarteleria', this.selectedIDLista]);
                            // }
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
