import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { SubtipoSustrato, SubtipoSustratoService,
    EventDataChange,
    AppSettings} from '../../shared';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
    selector: 'app-tables',
    templateUrl: './subtipossustratos.component.html',
    styleUrls: ['./subtipossustratos.component.scss'],
    animations: [routerTransition()],
    providers: [
        SubtipoSustratoService
    ]
})

export class SubtiposSustratosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
          id: { title: 'Código', filter: false, },
          sustrato: { title: 'Sustrato', filter: false, },
          subtiposustrato: { title: 'Subtipo Sustrato', filter: false, },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public data: Array<any> = Array<any>();
    public modalMessage: string;
    public selectedItem: SubtipoSustrato = null;
    public modalTitle = 'SubtipoSustrato';

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private dataService: SubtipoSustratoService) {
        this.source = new LocalDataSource(this.data);
        this.isLoading = true;
    }

    public ngOnInit(): void {
        this.dataService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    this.showMessage('No se han podido cargar los datos!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    handleItemUpdated(newItem) {
        if (newItem instanceof EventDataChange) {
            switch (newItem.eventType) {
                case AppSettings.EventType.new:
                    this.source.append(newItem.object as SubtipoSustrato);
                    break;
                case AppSettings.EventType.update:
                    this.source.update(this.selectedItem, newItem.object as SubtipoSustrato);
                    break;
                case AppSettings.EventType.delete:
                    this.source.remove(newItem.object as SubtipoSustrato);
                    break;
            }
        }
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            {
                field: 'id',
                search: query,
            },
            {
                field: 'sustrato',
                search: query,
            },
            {
                field: 'subtiposustrato',
                search: query,
            }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/subtiposustrato']);
                        } else {
                            this.router.navigate(['/subtiposustrato']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
