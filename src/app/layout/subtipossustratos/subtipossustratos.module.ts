import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { SubtiposSustratosRoutingModule } from './subtipossustratos-routing.module';
import { SubtiposSustratosComponent } from './subtipossustratos.component';
import { ItemComponent } from './components/index';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        SubtiposSustratosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [
        SubtiposSustratosComponent,
        ItemComponent
    ]
})
export class SubtiposSustratosModule { }
