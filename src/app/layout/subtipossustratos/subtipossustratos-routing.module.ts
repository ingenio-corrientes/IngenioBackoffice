import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubtiposSustratosComponent } from './subtipossustratos.component';

const routes: Routes = [
    { path: '', component: SubtiposSustratosComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SubtiposSustratosRoutingModule { }
