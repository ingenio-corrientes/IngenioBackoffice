import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { SustratosRoutingModule } from './sustratos-routing.module';
import { SustratosComponent } from './sustratos.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        SustratosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [SustratosComponent]
})
export class SustratosModule { }
