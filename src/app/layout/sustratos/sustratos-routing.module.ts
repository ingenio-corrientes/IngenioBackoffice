import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SustratosComponent } from './sustratos.component';

const routes: Routes = [
    { path: '', component: SustratosComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SustratosRoutingModule { }
