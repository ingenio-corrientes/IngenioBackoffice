import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../shared';

import { SubtipoDisenoRoutingModule } from './subtipodiseno-routing.module';
import { SubtipoDisenoComponent } from './subtipodiseno.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        SubtipoDisenoRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [SubtipoDisenoComponent]
})
export class SubtipoDisenoModule { }
