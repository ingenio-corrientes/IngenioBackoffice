import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubtipoDisenoComponent } from './subtipodiseno.component';

const routes: Routes = [
    { path: '', component: SubtipoDisenoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SubtipoDisenoRoutingModule { }
