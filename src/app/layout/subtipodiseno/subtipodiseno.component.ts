import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    SubtipoDiseno, SubtipoDisenoService,
    TipoDiseno, TipoDisenoService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './subtipodiseno.component.html',
    styleUrls: ['./subtipodiseno.component.scss'],
    animations: [routerTransition()],
    providers: [
        SubtipoDisenoService,
        TipoDisenoService]
})

export class SubtipoDisenoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Subtipo de Diseño';

    public selectedID: number;
    public selectedEntity: SubtipoDiseno;
    public dataTiposDisenos: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private subtipodisenoService: SubtipoDisenoService,
        private tipodisenoService: TipoDisenoService) {
        this.selectedEntity = new SubtipoDiseno();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getData();
        });
    }

    public getData() {
        const p0 = this.getTiposDisenos();
        Promise.all([
            p0
        ]).then(
            this.getEntityData()
        );
    }

    public getTiposDisenos() {
        return this.tipodisenoService.getList()
        .then(
            response => {
                this.dataTiposDisenos = response;
            })
        .catch(e => this.handleError(e));
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.subtipodisenoService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as SubtipoDiseno;
              })
          .catch(e => this.handleError(e));
      }
    }

    public onSave() {
        this.subtipodisenoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El subtipo de diseño se guardó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido obtener los datos del subtipo de diseño!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.subtipodisenoService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.subtipodisenoService.ENTITY_DELETED) {
                    this.showMessage('El subtipo de diseño se ha eliminado correctamente', true);
                } else  if (+response === this.subtipodisenoService.ERROR_FOREING_KEY) {
                    this.showMessage('El subtipo de diseño está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El subtipo de diseño no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/subtiposdisenos']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/subtiposdisenos']);
                        } else {
                            this.router.navigate(['/subtipodiseno', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
