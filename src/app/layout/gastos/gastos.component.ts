import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR } from '../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Gasto, GastoService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './gastos.component.html',
    styleUrls: ['./gastos.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        CurrencyAR,
        GastoService,
    ]
})

export class GastosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Número', filter: false, },
            razonsocial: { title: 'Proveedor', filter: false, },
            concepto: { title: 'Concepto', filter: false, },
            monto: {
                title: 'Monto',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            },
            fecha: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            borrador: {
                title: 'Borrador',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    if (value === '1') {
                        return `<i class="fa fa-check" aria-hidden="true"></i>`
                    } else {
                    return ``
                    }
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'Gastos';

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Gasto;
    public data: Array<any> = Array<any>();
    public gastos: number;
    public monto: string;

    fecinicio: number;
    fecfin: number;

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private datePipe: DatePipe,
        private cp: CurrencyAR,
        private gastoService: GastoService) {
            this.source = new LocalDataSource(this.data);
            this.isLoading = true;
            this.gastos = 0;
            this.monto = '';
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.fecinicio = +params['fecinicio'];
            this.fecfin = +params['fecfin'];
            if (this.fecinicio && this.fecfin) {
                this.getFilteredList();
            } else {
                this.getList();
            }
        });
    }

    protected getFilteredList() {
        this.gastoService.getFilteredByDate(this.fecinicio, this.fecfin).then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.source.setSort([{ field: 'fecha', direction: 'asc' }]);
                    this.calcularResumen();
                } else {
                    this.showMessage('No se han podido cargar datos de gastos!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    private calcularResumen() {
        this.source.getFilteredAndSorted().then(data => {
            let mont = 0;
            let cant = 0;
            data.forEach(element => {
                mont += +element.monto;
                cant++;
            });
            this.gastos = cant;
            this.monto = this.cp.transform(mont);
        });
    }

    protected getList() {
        this.gastoService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.source.setSort([{ field: 'fecha', direction: 'asc' }]);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de gastos!!', false);
                    this.isLoading = false;
                }
                this.calcularResumen();
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/gasto', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            this.calcularResumen();
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query, },
            { field: 'razonsocial', search: query, },
            { field: 'concepto', search: query, },
            { field: 'monto', search: query, },
            { field: 'fecha', search: query, }
        ], false);
        this.calcularResumen();
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/gasto', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/gastos']);
                        } else {
                            this.router.navigate(['/gastos']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
