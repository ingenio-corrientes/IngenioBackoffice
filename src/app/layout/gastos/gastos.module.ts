import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { GastosRoutingModule } from './gastos-routing.module';
import { GastosComponent } from './gastos.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        GastosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule,
        Ng2SmartTableModule
    ],
    declarations: [GastosComponent]
})
export class GastosModule { }
