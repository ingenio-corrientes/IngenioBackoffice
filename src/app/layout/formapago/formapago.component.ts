import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { FormaPago, FormaPagoService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './formapago.component.html',
    styleUrls: ['./formapago.component.scss'],
    animations: [routerTransition()],
    providers: [FormaPagoService]
})

export class FormaPagoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Forma de Pago';

    public selectedID: number;
    public selectedEntity: FormaPago;
    public bancario: boolean;
    public cheque: boolean;

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private formaPagoService: FormaPagoService) {
        this.isLoading = true;
        this.selectedEntity = new FormaPago();
        this.bancario = false;
        this.cheque = false;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.formaPagoService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as FormaPago;
                  this.bancario = this.selectedEntity.bancario === 1;
                  this.cheque = this.selectedEntity.cheque === 1;
                  this.isLoading = false;
              })
          .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        this.selectedEntity.bancario = this.bancario ? 1 : 0;
        this.selectedEntity.cheque = this.cheque ? 1 : 0;
        this.formaPagoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('La forma de pago se guardó correctamente', true);
                } else {
                    this.showMessage('No se han podido cargar los datos!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.formaPagoService.delete(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('La forma de pago se eliminó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido eliminar el forma de pago!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/formaspagos']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/formaspagos']);
                        } else {
                            this.router.navigate(['/formapago', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
