import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { TiposDisenosRoutingModule } from './tiposdisenos-routing.module';
import { TiposDisenosComponent } from './tiposdisenos.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        TiposDisenosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [TiposDisenosComponent]
})
export class TiposDisenosModule { }
