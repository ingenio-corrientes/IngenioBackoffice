import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TiposDisenosComponent } from './tiposdisenos.component';

const routes: Routes = [
    { path: '', component: TiposDisenosComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TiposDisenosRoutingModule { }
