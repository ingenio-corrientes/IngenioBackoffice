import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoDisenoComponent } from './tipodiseno.component';

const routes: Routes = [
    { path: '', component: TipoDisenoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TipoDisenoRoutingModule { }
