import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {TipoDiseno, TipoDisenoService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './tipodiseno.component.html',
    styleUrls: ['./tipodiseno.component.scss'],
    animations: [routerTransition()],
    providers: [TipoDisenoService]
})

export class TipoDisenoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Tipo de Diseño';

    public selectedID: number;
    public selectedEntity: TipoDiseno;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private entityService: TipoDisenoService) {
        this.selectedEntity = new TipoDiseno();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.entityService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as TipoDiseno;
              })
          .catch(e => this.handleError(e));
      }
    }

    public onSave() {
        this.entityService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El tipo de diseño se guardó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido obtener los datos del tipo de diseño!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.entityService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.entityService.ENTITY_DELETED) {
                    this.showMessage('El tipo de diseño se ha eliminado correctamente', true);
                } else  if (+response === this.entityService.ERROR_FOREING_KEY) {
                    this.showMessage('El tipo de diseño está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El tipo de diseño no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/tiposdisenos']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/tiposdisenos']);
                        } else {
                            this.router.navigate(['/tipodiseno', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
