import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { ComprasRoutingModule } from './compras-routing.module';
import { ComprasComponent } from './compras.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ComprasRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule,
        Ng2SmartTableModule
    ],
    declarations: [ComprasComponent]
})
export class ComprasModule { }
