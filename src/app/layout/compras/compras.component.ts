import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR } from '../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Compra, CompraService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './compras.component.html',
    styleUrls: ['./compras.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        CurrencyAR,
        CompraService
    ]
})

export class ComprasComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            tipocomprobante: { title: 'Tipo', filter: false, },
            numero: { title: 'Número', filter: false, },
            proveedor: { title: 'Proveedor', filter: false, },
            fecemision: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            total: {
                title: 'Monto',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            },
            saldo: {
                title: 'Saldo',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'Compras';

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Compra;
    public data: Array<any> = Array<any>();

    fecinicio: number;
    fecfin: number;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private datePipe: DatePipe,
        private cp: CurrencyAR,
        private compraService: CompraService) {
            this.isLoading = true;
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.getList();
    }

    protected getList() {
        this.compraService.getList()
        .then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de gastos!!', false);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/compra', '-']);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'tipocomprobante', search: query, },
            { field: 'numero', search: query, },
            { field: 'proveedor', search: query, },
            { field: 'fecha', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/compra', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/compra']);
                        } else {
                            this.router.navigate(['/compra']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
