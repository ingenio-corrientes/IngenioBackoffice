import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PresItImpresionLaserComponent } from './presitimpresionlaser.component';
import {
  DataComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

describe('PresItImpresionLaserComponent', () => {
  let component: PresItImpresionLaserComponent;
  let fixture: ComponentFixture<PresItImpresionLaserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        PresItImpresionLaserComponent,
        DataComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresItImpresionLaserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
