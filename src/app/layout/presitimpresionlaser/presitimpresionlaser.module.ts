import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { PresItImpresionLaserRoutingModule } from './presitimpresionlaser-routing.module';
import { PresItImpresionLaserComponent } from './presitimpresionlaser.component';
import {
  DataComponent,
  AdicionalesComponent,
  AdicionalComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        PresItImpresionLaserRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      PresItImpresionLaserComponent,
      DataComponent,
      AdicionalesComponent,
      AdicionalComponent
    ]
})
export class PresItImpresionLaserModule { }
