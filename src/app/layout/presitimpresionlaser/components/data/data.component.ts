import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    ListaPrecio, ListaPrecioService,
    PresupuestoItemImpresionLaser, PresupuestoItemImpresionLaserService,
    Gramaje, GramajeService,
    Tamano, TamanoService,
    LPrecioImpresionLaser, LPrecioImpresionLaserService,
    AppSettings,
    LPrecioImpresionLaserIntervalo,
    Presupuesto, PresupuestoService,
    LPrecioImpresionLaserIntervaloService,
    TWUtils} from '../../../../shared';

@Component({
    selector: 'app-data',
    templateUrl: './data.component.html',
    styleUrls: ['./data.component.scss'],
    providers: [
        ListaPrecioService,
        PresupuestoItemImpresionLaserService,
        GramajeService,
        TamanoService,
        LPrecioImpresionLaserService,
        LPrecioImpresionLaserIntervaloService,
        PresupuestoService]
    })

export class DataComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Presupuesto -> Impresión Láser';

    // variables de campos
    public initialized: boolean;
    public selectedItemID: number;
    public selectedIDPresupuesto: number;
    public subtotal: number;
    public doblefaz: boolean;
    public color: boolean;

    // datos estructurados
    public selectedEntity: PresupuestoItemImpresionLaser;
    public selectedLPrecio: ListaPrecio;
    selectedPresupuesto: Presupuesto;

    // arreglos
    dataGramajes: Array<any> = Array<any>();
    dataTamanos: Array<any> = Array<any>();
    dataPrecios: Array<any> = Array<LPrecioImpresionLaser>();
    dataIntervalosPrecios: Array<any> = Array<LPrecioImpresionLaserIntervalo>();

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private listaPrecioService: ListaPrecioService,
        private presupuestoitemService: PresupuestoItemImpresionLaserService,
        private gramajeService: GramajeService,
        private tamanoService: TamanoService,
        private precioImpresionLaserService: LPrecioImpresionLaserService,
        private precioImpresionLaserIntervaloService: LPrecioImpresionLaserIntervaloService,
        private presupuestoService: PresupuestoService) {
            this.isLoading = true;
            this.selectedEntity = new PresupuestoItemImpresionLaser();
            this.subtotal = 0.00;
            this.initialized = false;
            this.color = true;
            this.initialized = false;
            this.selectedPresupuesto = new Presupuesto();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedItemID = +params['id'];
            this.selectedIDPresupuesto = +params['idpresupuesto'];
            this.getData();
        });
    }

    public getData() {
        this.isLoading = true;
        const p0 = this.listaPrecioService.getByIDPrsupuesto(this.selectedIDPresupuesto,
            AppSettings.TiposServicios.impresionlaser);
        const p1 = this.gramajeService.getList();
        const p2 = this.tamanoService.getList();
        const p3 = this.precioImpresionLaserService.getList();
        const p4 = this.precioImpresionLaserIntervaloService.getList();
        const p5 = this.presupuestoService.getByID(this.selectedIDPresupuesto);
        Promise.all([
            p0, p1, p2, p3, p4, p5
        ]).then(([lista, gramajes, tamanos,
            precios, intervalos, presupuesto]) => {
            this.selectedLPrecio = lista;
            this.dataGramajes = gramajes;
            this.dataTamanos = tamanos;
            this.dataPrecios = precios;
            this.dataIntervalosPrecios = intervalos;
            this.selectedPresupuesto = presupuesto;
            this.getEntityData();
            this.isLoading = false;
        });
    }

    public getEntityData(): any {
        if (this.selectedItemID > -1) {
            this.presupuestoitemService.getByID(this.selectedItemID)
            .then(
                    response => {
                        this.selectedEntity = response as PresupuestoItemImpresionLaser;
                        this.doblefaz = this.selectedEntity.doblefaz === 1;
                        this.color = this.selectedEntity.color === 1;
                        this.dataChange('');
                        this.isLoading = false;
                    })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        this.isLoading = true;
        this.selectedEntity.idpresupuesto = this.selectedIDPresupuesto;
        this.selectedEntity.doblefaz = this.doblefaz ? 1 : 0;
        this.selectedEntity.color = this.color ? 1 : 0;
        if (!this.verifyData()) {
            this.isLoading = false;
            return;
        }
        this.presupuestoitemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    if (this.selectedItemID === -1) {
                        this.selectedItemID = Number(response);
                    }
                    this.showMessage('El ítem de impresión láser se guardó correctamente', false);
                } else {
                    this.showMessage('No se pudo guardar el ítem!!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    public onNuevo() {
        this.isLoading = true;
        this.selectedEntity.idpresupuesto = this.selectedIDPresupuesto;
        this.selectedEntity.doblefaz = this.doblefaz ? 1 : 0;
        this.selectedEntity.color = this.color ? 1 : 0;
        if (!this.verifyData()) {
            this.isLoading = false;
            return;
        }
        this.presupuestoitemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.showMessage('El ítem de impresión láser se guardó correctamente', false);
                    this.selectedEntity = new PresupuestoItemImpresionLaser();
                    this.subtotal = 0;
                    this.router.navigate(['/presitimpresionlaser', this.selectedIDPresupuesto, -1]);
                } else {
                    this.showMessage('No se pudo guardar el ítem!!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    protected verifyData() {
        if (this.selectedEntity.cantidad > 0) {
            return true;
        }
    }

    public isNew() {
        return (this.selectedItemID === -1);
    }

    isCero() {
        return (this.selectedEntity.cantidad !== undefined) ? (+this.selectedEntity.cantidad === 0) : true;
    }

    public dataChange(value) {
        // selecciona datos básicos
        const gramaje: Gramaje =
            this.findObjectInArray(this.dataGramajes, this.selectedEntity.idgramaje);
        const tamano: Tamano =
            this.findObjectInArray(this.dataTamanos, this.selectedEntity.idtamano);

        // agrega descripcion
        this.selectedEntity.descripcion =
            ((gramaje) ? (gramaje.gramaje + '|') : '') +
            ((tamano) ? (tamano.tamano + '|') : '') +
            ((this.color) ? ('Color|') : 'Blanco y negro|') +
            ((this.doblefaz) ? ('Doble faz|') : 'Simple faz|') +
            ((this.selectedEntity.cantidad) ? (this.selectedEntity.cantidad) : '');

        if (gramaje === undefined || tamano === undefined) {
            return;
        }

        // obtiene precio en funcion
        const precio: LPrecioImpresionLaser = this.dataPrecios.find(
                data  =>
                +data.idtamano === +this.selectedEntity.idtamano &&
                +data.idgramaje === +this.selectedEntity.idgramaje &&
                +data.idlistaprecio === +this.selectedLPrecio.id);

        // setea el precio base
        this.selectedEntity.precio = precio.precio;

        // obtiene intervalo
        let intervalo: LPrecioImpresionLaserIntervalo;
        for (let i = 0; i < this.dataIntervalosPrecios.length; i++) {
            const data = this.dataIntervalosPrecios[i];

            if (+data.idlprecioimpresionlaser === +precio.id) {
                intervalo = data;
                if (+data.maximo >= +this.selectedEntity.cantidad) {
                    break;
                }
            }
        }

        let porcentaje = 0;
        if (intervalo !== undefined) {
            porcentaje = (precio.redondear > 0) ?
                Math.round(+this.selectedEntity.precio * (+intervalo.porcentaje) / 100) :
                (+this.selectedEntity.precio * (+intervalo.porcentaje) / 100);
        }

        if (this.selectedEntity.cantidad === null) {
            return;
        }
        if (this.selectedEntity.cantidad === undefined) {
            return;
        }

        // calcula precio final
        const precioBruto = +precio.precio + porcentaje;
        const precioBrutoC = precioBruto * ((this.color ? 0 :  +precio.color / 100)  + 1);
        const precioBrutoCD = precioBrutoC * ((this.doblefaz ? +precio.doblefaz / 100  : 0) + 1);

        this.selectedEntity.precio = precioBrutoCD;

        // calcula subtotal
        this.subtotal = this.selectedEntity.precio * this.selectedEntity.cantidad;
    }

    protected findObjectInArray(arr: Array<any>, idtodind: number) {
        const object = arr.find(
            data =>
            +data.id === +idtodind);
        return object;
    }

    onDobleFazChage() {
        this.doblefaz = !this.doblefaz;
        this.dataChange('');
    }

    onColorChage() {
        this.color = !this.color;
        this.dataChange('');
    }

    public eliminar() {
        this.isLoading = true;
        this.presupuestoitemService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.presupuestoitemService.ENTITY_DELETED) {
                    this.showMessage('El banco se ha eliminado correctamente', true);
                } else  if (+response === this.presupuestoitemService.ERROR_FOREING_KEY) {
                    this.showMessage('El banco está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El banco no se ha podido eliminar!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    hasLista() {
        if (this.selectedLPrecio !== undefined) {
            if (this.selectedLPrecio.id > -1) {
                return true;
            }
        }
        return false;
    }

    getEstadoHabilitante() {
        if (this.selectedIDPresupuesto === -1) {
            return AppSettings.EstadosPresupuesto.nuevo;
        }
        if (TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecentrega)) {
            return AppSettings.EstadosPresupuesto.entegado;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecaproduccion)) {
            return AppSettings.EstadosPresupuesto.en_produccion;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.emitido;
        }
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuesto', this.selectedIDPresupuesto]);
                        } else {
                            this.router.navigate(['/presitimpresionlaser', this.selectedIDPresupuesto, this.selectedItemID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        this.isLoading = false;
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
