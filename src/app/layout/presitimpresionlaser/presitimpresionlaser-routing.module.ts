import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresItImpresionLaserComponent } from './presitimpresionlaser.component';

const routes: Routes = [
    { path: '', component: PresItImpresionLaserComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PresItImpresionLaserRoutingModule { }
