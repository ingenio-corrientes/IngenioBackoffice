import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { LPrecioImpresionLaser, LPrecioImpresionLaserService, CurrencyAR, EventDataChange, AppSettings } from '../../shared/';

@Component({
    selector: 'app-data',
    templateUrl: './lprecioimpresionlaser.component.html',
    styleUrls: ['./lprecioimpresionlaser.component.scss'],
    animations: [routerTransition()],
    providers: [
        CurrencyAR,
        LPrecioImpresionLaserService]
})

export class LPrecioImpresionLaserComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            gramaje: { title: 'Tipo Sustrato', filter: false, },
            tamano: { title: 'Tamaño', filter: false, },
            precio: {
                title: 'Precio',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered table-hover',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'LPrecioImpresionLaser';

    public data: Array<any> = Array<any>();
    public selectedItem: LPrecioImpresionLaser = null;
    public selectedIDLista: number;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private cp: CurrencyAR,
        private itemService: LPrecioImpresionLaserService) {
        this.isLoading = true;
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDLista = params['id'];
            this.getData();
        });
    }

    handleItemUpdated(newItem) {
        if (newItem instanceof EventDataChange) {
            const item = newItem.object as LPrecioImpresionLaser;
            switch (newItem.eventType) {
                case AppSettings.EventType.update:
                    this.source.update(this.selectedItem, item);
                    break;
                case AppSettings.EventType.cancel:
                    this.selectedItem = item;
                    break;
            }
        }
    }

    public getData() {
        if (this.selectedIDLista !== -1) {
            this.itemService.getListByIDLista(this.selectedIDLista).then(
                response => {
                    if (response) {
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                        this.isLoading = false;
                    } else {
                        this.showMessage('No se pudo obtener los datos de la lista!!', false);
                        this.isLoading = false;
                    }
                }
            ).catch(e => this.handleError(e));
        }
    }

    public volver() {
        this.router.navigate(['/listaprecio', this.selectedIDLista]);
    }

    actualizar() {
        this.itemService.updateLista(this.selectedIDLista).then(
                response => {
                    if (response) {
                        this.showMessage('Datos de la lista actualizados!!', null);
                        this.getData();
                    } else {
                        this.showMessage('No se pudo obtener los datos de la lista!!', false);
                        this.isLoading = false;
                    }
                }
            ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'gramaje', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            if (goBack) {
                                this.router.navigate(['/lprecioimpresionlaser']);
                            } else if (goBack === false) {
                                this.router.navigate(['/lprecioimpresionlaser', this.selectedIDLista]);
                            }
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
