import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LPrecioImpresionLaserComponent } from './lprecioimpresionlaser.component';
import {
    ItemComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

describe('LPrecioImpresionLaserComponent', () => {
    let component: LPrecioImpresionLaserComponent;
    let fixture: ComponentFixture<LPrecioImpresionLaserComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                FormsModule,
                ReactiveFormsModule,
                NgbModule.forRoot(),
                PageHeaderModule
            ],
            declarations: [
                LPrecioImpresionLaserComponent,
                ItemComponent
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LPrecioImpresionLaserComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
