import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { LPrecioImpresionLaserRoutingModule } from './lprecioimpresionlaser-routing.module';
import { LPrecioImpresionLaserComponent } from './lprecioimpresionlaser.component';
import {
  ItemComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        LPrecioImpresionLaserRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      LPrecioImpresionLaserComponent,
      ItemComponent
    ]
})
export class LPrecioImpresionLaserModule { }
