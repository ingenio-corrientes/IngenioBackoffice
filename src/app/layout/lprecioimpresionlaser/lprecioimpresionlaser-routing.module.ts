import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LPrecioImpresionLaserComponent } from './lprecioimpresionlaser.component';

const routes: Routes = [
    { path: '', component: LPrecioImpresionLaserComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LPrecioImpresionLaserRoutingModule { }
