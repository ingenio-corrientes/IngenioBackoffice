import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Proveedor, ProveedorService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './proveedores.component.html',
    styleUrls: ['./proveedores.component.scss'],
    animations: [routerTransition()],
    providers: [
        ProveedorService,
    ]
})

export class ProveedoresComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Código', filter: false, },
            razonsocial: { title: 'Razón Social', filter: false, },
            telefono: { title: 'Teléfono', filter: false, },
            email: { title: 'Email', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'Proveedores';

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Proveedor;
    public data: Array<any> = Array<any>();

    isLoading = false;

    public constructor(public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private proveedorService: ProveedorService) {
            this.isLoading = true;
            this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.proveedorService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.source.setSort([{ field: 'razonsocial', direction: 'asc' }]);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de proveedores!!', false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/proveedor', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query, },
            { field: 'razonsocial', search: query, },
            { field: 'telefono', search: query, },
            { field: 'email', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/proveedor', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/proveedores']);
                        } else {
                            this.router.navigate(['/proveedores']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
