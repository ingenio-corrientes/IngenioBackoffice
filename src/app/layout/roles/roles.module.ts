import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { RolesRoutingModule } from './roles-routing.module';
import { RolesComponent } from './roles.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RolesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule,
        Ng2SmartTableModule
    ],
    declarations: [RolesComponent]
})
export class RolesModule { }
