import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { ModuloXRolService, Cheque, ChequeService } from '../../shared/services';

@Component({
    selector: 'app-tables',
    templateUrl: './cheques.component.html',
    styleUrls: ['./cheques.component.scss'],
    animations: [routerTransition()],
    providers: [ DatePipe, ChequeService,
        ModuloXRolService, CurrencyPipe ]
})

export class ChequesComponent implements OnInit {
    readonly IDMODULO = 10;
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            librador: { title: 'Emisor', filter: false, },
            entrega: { title: 'Entregado por', filter: false, },
            tenedoractual: { title: 'Entregado a', filter: false, },
            banco: { title: 'Banco', filter: false, },
            numero: { title: 'Número', filter: false, },
            fecemision: {
                title: 'Emisión', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            fecvencimiento: {
                title: 'Vencimiento', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            monto: {
                title: 'Monto',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value, 'USD', true, '1.2-2') + ` </div>`;
                }
            }
        },
        defaultStyle: false,
        rowClassFunction : function(row) {
                const depositado = +row.data.depositado;
                const cobrado = +row.data.cobrado;
                if (depositado === 1 || cobrado === 1) {
                    return 'depositado';
                } if (depositado === 0 && cobrado === 0) {
                    return 'encartera';
                } else {
                    return '';
                }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public modalTitle = 'Cheques';
    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Cheque;
    public selectedEstado: number;
    public data: Array<any> = Array<any>();
    public recibos: boolean;
    public gastos: boolean;
    loggedId: number;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private datePipe: DatePipe,
        private moduloXRolService: ModuloXRolService,
        private cp: CurrencyPipe,
        private chequeService: ChequeService) {
        this.isLoading = true;
        this.source = new LocalDataSource(this.data);
        this.selectedEstado = 1;
        this.recibos = true;
        this.gastos = true;
    }

    public ngOnInit(): void {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedId = +jsonData.id;
        this.evalPermisos();
        this.chequeService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    // this.source.setSort([{ field: 'fecemision', direction: 'desc' }]);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de cheques!!', false, false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    evalPermisos() {
        this.moduloXRolService.getListByIDUsuarioIDModulo(this.loggedId, this.IDMODULO)
            .then(response => {
                if (response) {
                    if (+response.ver !== 1) {
                        this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    }
                    this.isLoading = false;
                } else {
                    this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    onCheck(origen: number) {
        if (origen === 0) {
            this.recibos = !this.recibos;
        } else {
            this.gastos = !this.gastos;
        }
        const temp = this.data.filter(data => {
            return (+data.origen === 0 && this.recibos) ||
                (+data.origen === 1 && this.gastos);
        });
        this.source = new LocalDataSource(temp);
    }

    nuevo() {
        this.router.navigate(['/cheque', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'tenedororiginal', search: query, },
            { field: 'banco', search: query, },
            { field: 'fecemision', search: query, },
            { field: 'fecvencimiento', search: query, },
            { field: 'numero', search: query, },
            { field: 'tenedoractual', search: query, },
            { field: 'entrega', search: query, },
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/cheque', event.data.id]);
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                        }
                        if (goBack) {
                            this.router.navigate(['/dashboard']);
                        } else {
                            this.router.navigate(['/dashboard']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
