import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { ChequesRoutingModule } from './cheques-routing.module';
import { ChequesComponent } from './cheques.component';


@NgModule({
    imports: [
        FormsModule,
        // Ng2TableModule,
        // PaginationModule.forRoot(),
        Ng2SmartTableModule,
        CommonModule,
        ChequesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [ChequesComponent]
})
export class ChequesModule { }
