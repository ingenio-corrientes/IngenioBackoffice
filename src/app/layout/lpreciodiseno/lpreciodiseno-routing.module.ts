import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LPrecioDisenoComponent } from './lpreciodiseno.component';

const routes: Routes = [
    { path: '', component: LPrecioDisenoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LPrecioDisenoRoutingModule { }
