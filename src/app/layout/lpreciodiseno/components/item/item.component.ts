import { Component, OnInit, Input, Output, EventEmitter, SimpleChange, ViewContainerRef, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    LPrecioDiseno, LPrecioDisenoService,
    TipoDisenoService,
    SubtipoDisenoService, EventDataChange,
    AppSettings
    } from '../../../../shared/';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    providers: [
        LPrecioDisenoService,
        TipoDisenoService,
        SubtipoDisenoService]
})
export class ItemComponent implements OnInit, OnChanges {
    @Input() selectedItem: LPrecioDiseno;
    @Output() itemUpdated = new EventEmitter();
    public modalMessage: string;
    public modalTitle = 'LPreciodiseno';

    // datos estructurados
    public selectedEntity: LPrecioDiseno;
    private selectedIDLista: number;

    // arreglos
    public dataTiposDiseno: Array<any> = Array<any>();
    public dataSubtiposDiseno: Array<any> = Array<any>();

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private itemsService: LPrecioDisenoService) {
            this.cancelar();
            this.isLoading = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDLista = params['id'];
            this.isLoading = false;
        });
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue as LPrecioDiseno;
            this.isLoading = false;
        }
    }

    onSave() {
        this.selectedEntity.idlistaprecio = this.selectedIDLista;

        this.itemsService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    const selectedItem = new EventDataChange();
                    selectedItem.eventType = AppSettings.EventType.update;
                    selectedItem.object = this.selectedEntity;
                    this.itemUpdated.emit(selectedItem);

                    this.selectedEntity = response;
                    this.cancelar();
                } else {
                    this.showMessage('No se ha podido guardar los datos del ítem de lista de precio!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
      this.itemsService.delete(this.selectedEntity)
      .then(
          response => {
            if (response) {
                this.itemUpdated.emit(true);
                this.cancelar();
            } else {
                this.showMessage('No se ha podido eliminar los datos del ítem de lista de precio!!', false);
            }
        }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new LPrecioDiseno();
        const selectedItem = new EventDataChange();
        selectedItem.eventType = AppSettings.EventType.cancel;
        selectedItem.object = this.selectedEntity;
        this.itemUpdated.emit(selectedItem);
    }

    isNew() {
      return (this.selectedEntity !== null) ? (this.selectedEntity.id === -1) : (true);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/lpreciodiseno']);
                        } else {
                            this.router.navigate(['/lpreciodiseno', this.selectedIDLista]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
