import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LPrecioDisenoComponent } from './lpreciodiseno.component';
// import {
//   ItemComponent
// } from './components/';

import { PageHeaderModule } from './../../shared';

describe('LPrecioDisenoComponent', () => {
  let component: LPrecioDisenoComponent;
  let fixture: ComponentFixture<LPrecioDisenoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        LPrecioDisenoComponent,
        // DataComponent,
        // ItemsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LPrecioDisenoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
