import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'banco/:id', loadChildren: './banco/banco.module#BancoModule' },
            { path: 'bancos', loadChildren: './bancos/bancos.module#BancosModule' },
            { path: 'cheque/:id', loadChildren: './cheque/cheque.module#ChequeModule' },
            { path: 'cheques', loadChildren: './cheques/cheques.module#ChequesModule' },
            { path: 'cliente/:id', loadChildren: './cliente/cliente.module#ClienteModule' },
            { path: 'clientes', loadChildren: './clientes/clientes.module#ClientesModule' },
            { path: 'cuentascorrientes', loadChildren: './cuentascorrientes/cuentascorrientes.module#CuentasCorrientesModule' },
            { path: 'formapago/:id', loadChildren: './formapago/formapago.module#FormaPagoModule' },
            { path: 'formaspagos', loadChildren: './formaspagos/formaspagos.module#FormasPagosModule' },
            { path: 'gasto/:id', loadChildren: './gasto/gasto.module#GastoModule' },
            { path: 'gastos', loadChildren: './gastos/gastos.module#GastosModule' },
            { path: 'gastos/:fecinicio/:fecfin', loadChildren: './gastos/gastos.module#GastosModule' },
            // { path: 'precios', loadChildren: './precios/precios.module#PreciosModule' },
            { path: 'presupuesto/:id', loadChildren: './presupuesto/presupuesto.module#PresupuestoModule' },
            { path: 'presupuestos', loadChildren: './presupuestos/presupuestos.module#PresupuestosModule' },
            { path: 'proveedor/:id', loadChildren: './proveedor/proveedor.module#ProveedorModule' },
            { path: 'proveedores', loadChildren: './proveedores/proveedores.module#ProveedoresModule' },
            { path: 'proveedoresctactes', loadChildren: './proveedoresctactes/proveedoresctactes.module#ProveedoresCtaCtesModule' },
            { path: 'recibo/:id', loadChildren: './recibo/recibo.module#ReciboModule' },
            { path: 'recibo/:id/:idpresupuesto', loadChildren: './recibo/recibo.module#ReciboModule' },
            { path: 'recibos', loadChildren: './recibos/recibos.module#RecibosModule' },
            { path: 'recibos/:fecinicio/:fecfin', loadChildren: './recibos/recibos.module#RecibosModule' },
            { path: 'retiroefectivo/:id', loadChildren: './retiroefectivo/retiroefectivo.module#RetiroEfectivoModule' },
            { path: 'retiroefectivos', loadChildren: './retiroefectivos/retiroefectivos.module#RetiroefectivosModule' },
            { path: 'rol/:id', loadChildren: './rol/rol.module#RolModule' },
            { path: 'roles', loadChildren: './roles/roles.module#RolesModule' },
            { path: 'usuario/:id', loadChildren: './usuario/usuario.module#UsuarioModule' },
            { path: 'usuarios', loadChildren: './usuarios/usuarios.module#UsuariosModule' },
            { path: 'perfil/:id', loadChildren: './perfil/perfil.module#PerfilModule' },
            // tslint:disable-next-line:max-line-length
            { path: 'presitcarteleria/:idpresupuesto/:id', loadChildren: './presitcarteleria/presitcarteleria.module#PresItCarteleriaModule' },
            { path: 'presitpapeleria/:idpresupuesto/:id', loadChildren: './presitpapeleria/presitpapeleria.module#PresItPapeleriaModule' },
            { path: 'presitdiseno/:idpresupuesto/:id', loadChildren: './presitdiseno/presitdiseno.module#PresItDisenoModule' },
            // tslint:disable-next-line:max-line-length
            { path: 'presitimpresionlaser/:idpresupuesto/:id', loadChildren: './presitimpresionlaser/presitimpresionlaser.module#PresItImpresionLaserModule' },
            { path: 'sustrato/:id', loadChildren: './sustrato/sustrato.module#SustratoModule' },
            { path: 'sustratos', loadChildren: './sustratos/sustratos.module#SustratosModule' },
            { path: 'subtiposustrato/:id', loadChildren: './subtiposustrato/subtiposustrato.module#SubtipoSustratoModule' },
            { path: 'subtipossustratos', loadChildren: './subtipossustratos/subtipossustratos.module#SubtiposSustratosModule' },
            { path: 'tipoadicional/:id', loadChildren: './tipoadicional/tipoadicional.module#TipoAdicionalModule' },
            { path: 'tiposadicionales', loadChildren: './tiposadicionales/tiposadicionales.module#TiposAdicionalesModule' },
            { path: 'tipodiseno/:id', loadChildren: './tipodiseno/tipodiseno.module#TipoDisenoModule' },
            { path: 'tiposdisenos', loadChildren: './tiposdisenos/tiposdisenos.module#TiposDisenosModule' },
            { path: 'listaprecio/:id', loadChildren: './listaprecio/listaprecio.module#ListaPrecioModule' },
            { path: 'listasprecios', loadChildren: './listasprecios/listasprecios.module#ListasPreciosModule' },
            { path: 'lpreciocarteleria/:id', loadChildren: './lpreciocarteleria/lpreciocarteleria.module#LPrecioCarteleriaModule' },
            { path: 'lprecioadcarteleria/:id', loadChildren: './lprecioadcarteleria/lprecioadcarteleria.module#LPrecioAdCarteleriaModule' },
            { path: 'lpreciodiseno/:id', loadChildren: './lpreciodiseno/lpreciodiseno.module#LPrecioDisenoModule' },
            // tslint:disable-next-line:max-line-length
            { path: 'lprecioimpresionlaser/:id', loadChildren: './lprecioimpresionlaser/lprecioimpresionlaser.module#LPrecioImpresionLaserModule' },
            // tslint:disable-next-line:max-line-length
            { path: 'lprecioimpresionlaserintervalos/:id/:idimpresion', loadChildren: './lprecioimpresionlaserintervalos/lprecioimpresionlaserintervalos.module#LPrecioImpresionLaserIntervalosModule' },
            { path: 'lpreciopapeleria/:id', loadChildren: './lpreciopapeleria/lpreciopapeleria.module#LPrecioPapeleriaModule' },
            { path: 'lprecioadpapeleria/:id', loadChildren: './lprecioadpapeleria/lprecioadpapeleria.module#LPrecioAdPapeleriaModule' },
            // tslint:disable-next-line:max-line-length
            { path: 'lpreciopapeleriaintervalos/:id/:idpapel', loadChildren: './lpreciopapeleriaintervalos/lpreciopapeleriaintervalos.module#LPrecioPapeleriaIntervalosModule' },
            { path: 'subtiposdisenos', loadChildren: './subtiposdisenos/subtiposdisenos.module#SubtiposDisenosModule' },
            { path: 'subtipodiseno/:id', loadChildren: './subtipodiseno/subtipodiseno.module#SubtipoDisenoModule' },
            { path: 'gramaje/:id', loadChildren: './gramaje/gramaje.module#GramajeModule' },
            { path: 'gramajes', loadChildren: './gramajes/gramajes.module#GramajesModule' },
            { path: 'tamano/:id', loadChildren: './tamano/tamano.module#TamanoModule' },
            { path: 'tamanos', loadChildren: './tamanos/tamanos.module#TamanosModule' },
            // tslint:disable-next-line:max-line-length
            { path: 'tipopapeleriacomercial/:id', loadChildren: './tipopapeleriacomercial/tipopapeleriacomercial.module#TipoPapeleriaComercialModule' },
            // tslint:disable-next-line:max-line-length
            { path: 'tipospapeleriascomerciales', loadChildren: './tipospapeleriascomerciales/tipospapeleriascomerciales.module#TiposPapeleriasComercialesModule' },
            // tslint:disable-next-line:max-line-length
            { path: 'subtipopapeleriacomercial/:id', loadChildren: './subtipopapeleriacomercial/subtipopapeleriacomercial.module#SubtipoPapeleriaComercialModule' },
            // tslint:disable-next-line:max-line-length
            { path: 'subtipospapeleriascomerciales', loadChildren: './subtipospapeleriascomerciales/subtipospapeleriascomerciales.module#SubtiposPapeleriasComercialesModule' },
            { path: 'caja', loadChildren: './caja/caja.module#CajaModule' },
            { path: 'cajainforme', loadChildren: './cajainforme/cajainforme.module#CajaInformeModule' },
            { path: 'compra/:id', loadChildren: './compra/compra.module#CompraModule' },
            { path: 'compras', loadChildren: './compras/compras.module#ComprasModule' },
            { path: 'tipocomprobante/:id', loadChildren: './tipocomprobante/tipocomprobante.module#TipoComprobanteModule' },
            { path: 'tiposcomprobantes', loadChildren: './tiposcomprobantes/tiposcomprobantes.module#TiposComprobantesModule' },
            { path: 'lprecioadimpresionlaser/:id', loadChildren: './lprecioadimpresionlaser/lprecioadimpresionlaser.module#LPrecioAdImpresionLaserModule' },
/*
            { path: 'charts', loadChildren: './_charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './_tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './_form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './_bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './_grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './_bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './_blank-page/blank-page.module#BlankPageModule' },
            */
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
