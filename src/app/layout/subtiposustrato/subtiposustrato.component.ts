import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    SubtipoSustrato, SubtipoSustratoService,
    Sustrato, SustratoService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './subtiposustrato.component.html',
    styleUrls: ['./subtiposustrato.component.scss'],
    animations: [routerTransition()],
    providers: [
        SubtipoSustratoService,
        SustratoService]
})

export class SubtipoSustratoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Subtipo de Sustrato';

    public selectedID: number;
    public selectedEntity: SubtipoSustrato;
    public dataSustratos: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private subtiposustratoService: SubtipoSustratoService,
        private sustratoService: SustratoService) {
        this.selectedEntity = new SubtipoSustrato();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getData();
        });
    }

    public getData() {
        Promise.all([
            this.getSustratos()
        ]).then(
            this.getEntityData()
        );
    }

    public getSustratos() {
        this.sustratoService.getList()
        .then(
            response => {
                this.dataSustratos = response;
            })
        .catch(e => this.handleError(e));
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.subtiposustratoService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as SubtipoSustrato;
              })
          .catch(e => this.handleError(e));
      }
    }

    public onSave() {
        this.subtiposustratoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El subtipo sustrato se guardó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido obtener los datos del subtipo sustrato!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.subtiposustratoService.delete(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El subtipo sustrato se eliminó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido eliminar el subtipo sustrato!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/subtipossustratos']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/subtipossustratos']);
                        } else {
                            this.router.navigate(['/subtiposustrato', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
