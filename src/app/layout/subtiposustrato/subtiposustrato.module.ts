import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../shared';

import { SubtipoSustratoRoutingModule } from './subtiposustrato-routing.module';
import { SubtipoSustratoComponent } from './subtiposustrato.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        SubtipoSustratoRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [SubtipoSustratoComponent]
})
export class SubtipoSustratoModule { }
