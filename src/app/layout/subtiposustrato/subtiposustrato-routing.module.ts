import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubtipoSustratoComponent } from './subtiposustrato.component';

const routes: Routes = [
    { path: '', component: SubtipoSustratoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SubtipoSustratoRoutingModule { }
