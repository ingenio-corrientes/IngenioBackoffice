import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { BancosRoutingModule } from './bancos-routing.module';
import { BancosComponent } from './bancos.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        BancosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [BancosComponent]
})
export class BancosModule { }
