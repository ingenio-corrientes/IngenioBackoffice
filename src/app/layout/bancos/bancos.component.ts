import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { Banco, BancoService } from '../../shared';

@Component({
    selector: 'app-tables',
    templateUrl: './bancos.component.html',
    styleUrls: ['./bancos.component.scss'],
    animations: [routerTransition()],
    providers: [
        BancoService,
    ]
})

export class BancosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
          id: { title: 'Código', filter: false, },
          banco: { title: 'Banco', filter: false, },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public data: Array<any> = Array<any>();

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private bancoService: BancoService) {
        this.isLoading = true;
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.bancoService.getList().then(
            response => {
                if (response) {
                    this.isLoading = false;
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                } else {
                    alert('No se han podido obtener los datos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/banco', -1]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            {
                field: 'id',
                search: query,
            },
            {
                field: 'banco',
                search: query,
            }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/banco', event.data.id]);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
