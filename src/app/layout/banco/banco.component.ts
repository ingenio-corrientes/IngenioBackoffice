import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {Banco, BancoService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './banco.component.html',
    styleUrls: ['./banco.component.scss'],
    animations: [routerTransition()],
    providers: [BancoService]
})

export class BancoComponent implements OnInit {
    public modalTitle = 'Banco';

    public selectedID: number;
    public selectedEntity: Banco;

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private bancoService: BancoService) {
        this.isLoading = true;
        this.selectedEntity = new Banco();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.bancoService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as Banco;
                  this.isLoading = false;
              })
          .catch(e => this.handleError(e));
      } else {
        this.isLoading = false;
      }
    }

    public onSave() {
        this.bancoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El banco se guardó correctamente', true);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.bancoService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.bancoService.ENTITY_DELETED) {
                    this.showMessage('El banco se ha eliminado correctamente', true);
                } else  if (+response === this.bancoService.ERROR_FOREING_KEY) {
                    this.showMessage('El banco está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El banco no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/bancos']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/bancos']);
                        } else {
                            this.router.navigate(['/banco', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
