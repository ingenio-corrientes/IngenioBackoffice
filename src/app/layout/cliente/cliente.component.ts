import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Cliente, ClienteService,
    ClienteTipo, ClienteTipoService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './cliente.component.html',
    styleUrls: ['./cliente.component.scss'],
    animations: [routerTransition()],
    providers: [
        ClienteService,
        ClienteTipoService]
})

export class ClienteComponent implements OnInit {
    public modalTitle = 'Cliente';

    public selectedID: number;
    public selectedEntity: Cliente;
    public dataTiposClientes: Array<any> = Array<any>();

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private clienteService: ClienteService,
        private clienteTipoService: ClienteTipoService) {
        this.isLoading = true;
        this.selectedEntity = new Cliente();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getData();
        });
    }

    public getData() {
        Promise.all([
            this.getClienteTipo()
        ]).then(
            this.getEntityData()
        );
    }

    public getClienteTipo(): any {
        this.clienteTipoService.getList()
        .then(
            response => {
                this.dataTiposClientes = response as ClienteTipo[];
            })
        .catch(e => this.handleError(e));
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.clienteService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as Cliente;
                  this.isLoading = false;
              })
          .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        this.selectedEntity.cuit = (this.selectedEntity.cuit.toString().length === 0) ? (0) : (this.selectedEntity.cuit);
        this.clienteService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El cliente se guardó correctamente', true);
                    // this.router.navigate(['/clientes']);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/clientes']);
    }

    public eliminar() {
        this.clienteService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.clienteService.ENTITY_DELETED) {
                    this.showMessage('El cliente se ha eliminado correctamente', true);
                } else  if (+response === this.clienteService.ERROR_FOREING_KEY) {
                    this.showMessage('El cliente está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El cliente no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/clientes']);
                        } else {
                            this.router.navigate(['/cliente', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
