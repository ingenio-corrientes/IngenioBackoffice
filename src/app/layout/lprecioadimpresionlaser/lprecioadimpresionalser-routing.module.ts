import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LPrecioAdImpresionLaserComponent } from './lprecioadimpresionalser.component';

const routes: Routes = [
    { path: '', component: LPrecioAdImpresionLaserComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LPrecioAdImpresionLaserRoutingModule { }
