import { Component, OnInit, Input, SimpleChange, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { LPrecioAdicional, LPrecioAdicionalService, CurrencyAR, EventDataChange, AppSettings } from '../../shared/';

@Component({
    selector: 'app-data',
    templateUrl: './lprecioadimpresionalser.component.html',
    styleUrls: ['./lprecioadimpresionalser.component.scss'],
    animations: [routerTransition()],
    providers: [
        CurrencyAR,
        LPrecioAdicionalService]
})

export class LPrecioAdImpresionLaserComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            tipoadicional: { title: 'Adicional', filter: false, },
            precio: {
                title: 'Precio',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered table-hover',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'LPrecioAdImpresionLaser';

    public data: Array<any> = Array<any>();
    public selectedItem: LPrecioAdicional = null;
    public selectedIDLista: number;

    isLoading = false;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private cp: CurrencyAR,
        private itemService: LPrecioAdicionalService) {
        this.source = new LocalDataSource(this.data);
        this.isLoading = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDLista = params['id'];
            this.getData();
        });
    }

    handleItemUpdated(newItem) {
        if (newItem instanceof EventDataChange) {
            const item = newItem.object as LPrecioAdicional;
            switch (newItem.eventType) {
                case AppSettings.EventType.update:
                    this.source.update(this.selectedItem, item);
                    break;
                case AppSettings.EventType.cancel:
                    this.selectedItem = item;
                    break;
            }
        }
    }

    public getData() {
        if (this.selectedIDLista !== -1) {
            this.itemService.getListByIDLista(this.selectedIDLista).then(
                response => {
                    if (response) {
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                        this.isLoading = false;
                    } else {
                        this.showMessage('No se pudo obtener los datos de la lista!!', false);
                        this.isLoading = false;
                    }
                }
            ).catch(e => this.handleError(e));
        }
    }

    public volver() {
        this.router.navigate(['/listaprecio', this.selectedIDLista]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'adicional', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            if (goBack) {
                                this.router.navigate(['/lprecioadimpresionlaser']);
                            } else {
                                this.router.navigate(['/lprecioadimpresionlaser', this.selectedIDLista]);
                            }
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
