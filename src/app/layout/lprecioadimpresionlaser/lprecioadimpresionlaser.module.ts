import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { LPrecioAdImpresionLaserRoutingModule } from './lprecioadimpresionalser-routing.module';
import { LPrecioAdImpresionLaserComponent } from './lprecioadimpresionalser.component';
import {
  ItemComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        LPrecioAdImpresionLaserRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      LPrecioAdImpresionLaserComponent,
      ItemComponent
    ]
})
export class LPrecioAdImpresionLaserModule { }
