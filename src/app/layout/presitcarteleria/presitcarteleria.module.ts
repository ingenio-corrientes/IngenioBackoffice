import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { PresItCarteleriaRoutingModule } from './presitcarteleria-routing.module';
import { PresItCarteleriaComponent } from './presitcarteleria.component';
import {
  DataComponent,
  AdicionalesComponent,
  AdicionalComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        PresItCarteleriaRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      PresItCarteleriaComponent,
      DataComponent,
      AdicionalesComponent,
      AdicionalComponent
    ]
})
export class PresItCarteleriaModule { }
