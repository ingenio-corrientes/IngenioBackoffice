import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresItCarteleriaComponent } from './presitcarteleria.component';

const routes: Routes = [
    { path: '', component: PresItCarteleriaComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PresItCarteleriaRoutingModule { }
