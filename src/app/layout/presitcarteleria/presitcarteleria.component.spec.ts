import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PresItCarteleriaComponent } from './presitcarteleria.component';
import {
  DataComponent,
  // ItemsComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

describe('PresItCarteleriaComponent', () => {
  let component: PresItCarteleriaComponent;
  let fixture: ComponentFixture<PresItCarteleriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        PresItCarteleriaComponent,
        DataComponent,
        // GastoItemsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresItCarteleriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
