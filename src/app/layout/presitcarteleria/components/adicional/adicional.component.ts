import { Component, OnInit, Input, Output, OnChanges, EventEmitter, SimpleChange, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    ListaPrecio, ListaPrecioService,
    PresupuestoItAdCarteleria, PresupuestoItAdCarteleriaService,
    TipoAdicional, TipoAdicionalService,
    LPrecioAdicional, LPrecioAdicionalService,
    Presupuesto, PresupuestoService,
    AppSettings, TWUtils} from '../../../../shared/';

@Component({
    selector: 'app-adicional',
    templateUrl: './adicional.component.html',
    styleUrls: ['./adicional.component.scss'],
    providers: [
        ListaPrecioService,
        PresupuestoItAdCarteleria,
        TipoAdicionalService,
        LPrecioAdicionalService,
        PresupuestoService]
})
export class AdicionalComponent implements OnInit, OnChanges {
    @Input() selectedItem: PresupuestoItAdCarteleria;
    @Output() itemUpdated = new EventEmitter();
    public modalTitle = 'Adicional';

    // variables de campos
    public subtotal: number;

    // datos estructurados
    public selectedEntity: PresupuestoItAdCarteleria;
    private selectedIDItemCart: number;
    public selectedIDPresupuesto: number;
    public selectedLPrecio: ListaPrecio;
    selectedPresupuesto: Presupuesto;

    // arreglos
    public dataTiposAdicionales: Array<any> = Array<any>();
    public dataPrecios: Array<any> = Array<LPrecioAdicional>();

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private listaPrecioService: ListaPrecioService,
        private adicionalService: PresupuestoItAdCarteleriaService,
        private tiposAdicionalesService: TipoAdicionalService,
        private precioAdicionalService: LPrecioAdicionalService,
        private presupuestoService: PresupuestoService) {
            this.isLoading = true;
            this.selectedPresupuesto = new Presupuesto();
            this.cancelar();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDItemCart = params['id'];
            this.selectedIDPresupuesto = +params['idpresupuesto'];
            this.getData();
        });
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue;
            this.onDataChange('');
        }
    }

    public getData() {
        this.isLoading = true;
        const p0 = this.listaPrecioService.getByIDPrsupuesto(this.selectedIDPresupuesto,
                AppSettings.TiposServicios.caterleria);
        const p1 = this.precioAdicionalService.getList();
        const p2 = this.presupuestoService.getByID(this.selectedIDPresupuesto);
        Promise.all([
            p0, p1, p2
        ]).then( ([lista, precioadicionales, presupuesto]) => {
            this.selectedLPrecio = lista;
            this.dataPrecios = precioadicionales;
            this.selectedPresupuesto = presupuesto;
            this.getEntityData();
            this.isLoading = false;
        });
    }

    public getEntityData(): any {
        this.tiposAdicionalesService.getListByIDServicio(AppSettings.TiposServicios.caterleria).then(
            response => {
                if (response) {
                    this.dataTiposAdicionales = response;
                    this.isLoading = false;
                } else {
                    this.showMessage('No se has podido obtener los datos de tipos de adicionales!!', false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSave() {
        this.isLoading = true;
        this.selectedEntity.idpresitcarteleria = this.selectedIDItemCart;

        this.adicionalService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.itemUpdated.emit(true);
                    this.cancelar();
                } else {
                    this.showMessage('No se ha podido guardar los datos del adicional!!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.isLoading = true;
        this.adicionalService.delete(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        this.cancelar();
                    } else {
                        this.showMessage('No se ha podido eliminar los datos del adicional!!', false);
                    }
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new PresupuestoItAdCarteleria();
        this.subtotal = 0.00;
        this.itemUpdated.emit(true);
    }

    public onDataChange(value) {
        const idtipoadicional = this.selectedEntity.idtipoadicional;
        const ancho = (this.selectedEntity.ancho) ? this.selectedEntity.ancho : 0;
        const alto = (this.selectedEntity.alto) ? this.selectedEntity.alto : 0;
        const unidad = (this.selectedEntity.unidades) ? this.selectedEntity.unidades : 0;

        const precio: LPrecioAdicional = this.dataPrecios.find(
            data  =>
            +data.idtipoadicional === +this.selectedEntity.idtipoadicional &&
            +data.idlistaprecio === +this.selectedLPrecio.id);
        if (precio === undefined) {
            this.subtotal = 0.00;
            this.selectedEntity.precio = 0.00;
            return;
        }
        this.selectedEntity.precio = precio.precio;
        const m2 = (+ancho) * (+alto);
        this.subtotal = precio.precio * m2 + this.selectedEntity.unidades * precio.precio;
    }

    isNew() {
      return (this.selectedEntity !== null) ? (this.selectedEntity.id === -1) : (true);
    }

    public isRequired(field) {
        if  (!this.selectedEntity.idtipoadicional) {
            return false;
        }

        const tipo: TipoAdicional = this.dataTiposAdicionales.find(
            data =>
            +data.id === +this.selectedEntity.idtipoadicional);
        switch (field) {
            case 0:
                return tipo.anchomin > 0;
            case 1:
                return tipo.altomin > 0;
            case 2:
                return tipo.unidad > 0;
        }
    }

    isCero() {
        if (this.isRequired(0) && this.selectedEntity.alto === 0) {
            return true;
        }
        if (this.isRequired(1) && this.selectedEntity.ancho === 0) {
            return true;
        }
        if (this.isRequired(2) && this.selectedEntity.unidades === 0) {
            return true;
        }
        return false;
    }

    getEstadoHabilitante() {
        if (this.selectedIDPresupuesto === -1) {
            return AppSettings.EstadosPresupuesto.nuevo;
        }
        if (TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecentrega)) {
            return AppSettings.EstadosPresupuesto.entegado;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecaproduccion)) {
            return AppSettings.EstadosPresupuesto.en_produccion;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.emitido;
        }
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presitcarteleria']);
                        } else {
                            this.router.navigate(['/presitcarteleria', this.selectedIDPresupuesto, this.selectedIDItemCart]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
