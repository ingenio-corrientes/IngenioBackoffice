import { Component, OnInit, Input,  SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import { PresupuestoItAdCarteleria, PresupuestoItAdCarteleriaService } from '../../../../shared/';

@Component({
    selector: 'app-adicionales',
    templateUrl: './adicionales.component.html',
    styleUrls: ['./adicionales.component.scss'],
    providers: [ PresupuestoItAdCarteleriaService]
})

export class AdicionalesComponent implements OnInit {
  settings = {
    noDataMessage: 'Datos no disponibles',
    pager: { display: true, perPage: 10 },
    columns: {
      tipoadicional: { title: 'Tipo Adicional', filter: false, },
      unidades: { title: 'Unidades', filter: false, },
      alto: { title: 'Alto', filter: false, },
      ancho: { title: 'Ancho', filter: false, },
    },
    actions: { add: false, edit: false, delete: false, },
    attr: {
        class: 'table dataTable table-striped table-bordered table-hover',
    },
  };
  source: LocalDataSource;

  public data: Array<any> = Array<any>();
  public selectedItem: PresupuestoItAdCarteleria = null;
  public selectedIDItem: number;

  public constructor(public router: Router,
      private route: ActivatedRoute,
      private adicionalService: PresupuestoItAdCarteleriaService) {
        this.source = new LocalDataSource(this.data);
  }

  public ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.selectedIDItem = params['id'];
      this.getData();
    });
  }

  handleItemUpdated(user) {
    this.getData();
  }

  public getData() {
    if (this.selectedIDItem !== -1) {
      this.adicionalService.getByIDIPresItCarteleria(this.selectedIDItem).then(
          response => {
            if (response) {
              this.data = response;
              this.source = new LocalDataSource(this.data);
            } else {
              alert('No se pudo obtener los datos del adicional!!');
            }
          }
        ).catch(e => this.handleError(e));
    }
  }

  onSearch(query: string) {
      if (query.length === 0) {
          this.source.setFilter([]);
          return;
      }
      this.source.setFilter([
          { field: 'tipoadicional', search: query, },
          { field: 'unidades', search: query, }
      ], false);
  }

  public userRowSelect(event: any): any {
      this.selectedItem = event.data;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
