import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    ListaPrecio, ListaPrecioService,
    PresupuestoItemCarteleria, PresupuestoItemCarteleriaService,
    Sustrato, SustratoService,
    SubtipoSustrato, SubtipoSustratoService,
    LPrecioCarteleria, LPrecioCarteleriaService,
    Presupuesto, PresupuestoService,
    TWUtils, AppSettings} from '../../../../shared';

@Component({
    selector: 'app-data',
    templateUrl: './data.component.html',
    styleUrls: ['./data.component.scss'],
    providers: [
        ListaPrecioService,
        PresupuestoItemCarteleriaService,
        SustratoService,
        SubtipoSustratoService,
        LPrecioCarteleriaService,
        PresupuestoService ]
})

export class DataComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Presupuesto -> Cartelería';

    // variables de campos
    public initialized: boolean;
    public selectedItemID: number;
    public selectedIDPresupuesto: number;
    public subtotal: number;
    public anchominimo: number;
    public altominimo: number;
    public anchomaximo: number;
    public altomaximo: number;

    // datos estructurados
    public selectedEntity: PresupuestoItemCarteleria;
    public selectedLPrecio: ListaPrecio;
    selectedPresupuesto: Presupuesto;

    // arreglos
    public dataSustratos: Array<any> = Array<any>();
    public dataSubtiposSustratos: Array<any> = Array<any>();
    public dataSubtiposSustratosFilt: Array<any> = Array<any>();
    public dataPrecios: Array<any> = Array<LPrecioCarteleria>();

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private listaPrecioService: ListaPrecioService,
        private presupuestoitemService: PresupuestoItemCarteleriaService,
        private sustratoService: SustratoService,
        private subtipoSustratoService: SubtipoSustratoService,
        private precioCarteleriaService: LPrecioCarteleriaService,
        private presupuestoService: PresupuestoService) {
            this.isLoading = true;
            const jsonData = JSON.parse(localStorage.getItem('userLog'));
            this.selectedEntity = new PresupuestoItemCarteleria();
            this.subtotal = 0.00;
            this.initialized = false;
            this.selectedPresupuesto = new Presupuesto();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedItemID = +params['id'];
            this.selectedIDPresupuesto = +params['idpresupuesto'];
            this.getData();
        });
    }

    public getData() {
        const p0 = this.listaPrecioService.getByIDPrsupuesto(this.selectedIDPresupuesto,
                AppSettings.TiposServicios.caterleria);
        const p1 = this.sustratoService.getList();
        const p2 = this.subtipoSustratoService.getList();
        const p3 = this.precioCarteleriaService.getList();
        const p4 = this.presupuestoService.getByID(this.selectedIDPresupuesto);
        Promise.all([
            p0, p1, p2, p3, p4
        ]).then( ([lista, sustratos, subtiposustratos,
            precios, presupuesto]) => {
            this.selectedLPrecio = lista;
            this.dataSustratos = sustratos;
            this.dataSubtiposSustratos = subtiposustratos;
            this.dataPrecios = precios;
            this.selectedPresupuesto = presupuesto;
            this.getEntityData();
            this.initialized = true;
            this.onSustratoChange();
        });
    }

    public getEntityData(): any {
        if (this.selectedItemID > -1) {
            this.presupuestoitemService.getByID(this.selectedItemID)
            .then(
                    response => {
                        this.selectedEntity = response as PresupuestoItemCarteleria;
                        this.onSustratoChange();
                        this.isLoading = false;
                    })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        this.isLoading = true;
        this.selectedEntity.idpresupuesto = this.selectedIDPresupuesto;
        if (!this.verifyData()) {
            this.isLoading = false;
            return;
        }
        this.presupuestoitemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    if (response !== this.presupuestoitemService.ERROR_NOT_MODIFIED) {
                        this.selectedItemID = Number(response);
                        this.showMessage('El ítem de cartelería se guardó correctamente', false);
                    } else {
                        this.showMessage('El ítem de cartelería se guardó correctamente', false);
                    }
                } else {
                    this.showMessage('No se pudo guardar el ítem!!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    protected onNuevo() {
        this.selectedEntity.idpresupuesto = this.selectedIDPresupuesto;
        if (!this.verifyData()) {
            this.isLoading = false;
            return;
        }
        this.presupuestoitemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    if (response !== this.presupuestoitemService.ERROR_NOT_MODIFIED) {
                        this.showMessage('El ítem de cartelería se guardó correctamente', false);
                        this.selectedEntity = new PresupuestoItemCarteleria();
                        this.subtotal = 0;
                        this.router.navigate(['/presitcarteleria', this.selectedIDPresupuesto, -1]);
                    } else {
                        this.showMessage('El ítem de cartelería se guardó correctamente', false);
                    }
                } else {
                    this.showMessage('No se pudo guardar el ítem!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    protected verifyData() {
        const idsustrato = this.selectedEntity.idsustrato.toString();
        const sustrato = this.dataSustratos.find(
            data => data.id === idsustrato);
        const subtiposustrato = this.dataSubtiposSustratos.find(
            data => data.id === this.selectedEntity.idsubtiposus.toString()
        );
        // mínimos
        if (+subtiposustrato.anchomin > +this.selectedEntity.ancho) {
            alert('El ancho es inferior al mínimo!!');
            return false;
        }
        if (+subtiposustrato.altomin > +this.selectedEntity.alto) {
            alert('El alto es inferior al mínimo!!');
            return false;
        }
        // máximos
        if (+subtiposustrato.anchomax > 0 && +subtiposustrato.anchomax < +this.selectedEntity.ancho) {
            alert('El ancho es superior al máximo!!');
            return false;
        }
        if (+subtiposustrato.altomax > 0 && +subtiposustrato.altomax < +this.selectedEntity.alto) {
            alert('El alto es superior al máximo!!');
            return false;
        }
        return true;
    }

    public isNew() {
        return (this.selectedItemID === -1);
    }

    isCero() {
        return (this.selectedEntity.cantidad !== undefined) ? (+this.selectedEntity.cantidad === 0) : true;
    }

    public onSustratoChange() {
        if (!this.initialized) {
            return;
        }
        const idsustrato = this.selectedEntity.idsustrato.toString();
        this.dataSubtiposSustratosFilt = this.dataSubtiposSustratos.filter(
            data =>
            data.idsustrato === idsustrato);
        const sustrato = this.dataSustratos.find(data => data.id === idsustrato);
        if (sustrato === undefined) {
            this.anchominimo = 0;
            this.altominimo = 0;
            this.anchomaximo = 0;
            this.altomaximo = 0;
            return;
        }
        // this.anchominimo = sustrato.anchomin;
        // this.altominimo = sustrato.altomin;

        this.selectedEntity.idsubtiposus = this.dataSubtiposSustratosFilt[0].id;
        this.onDataChange('');
    }

    public onDataChange(value) {
        if (!this.initialized) {
            return;
        }
        if (this.selectedEntity.idsustrato === undefined || this.selectedEntity.idsubtiposus === undefined) {
            return;
        }
        const idsustrato = this.selectedEntity.idsustrato.toString();
        const idsubtiposustrato = this.selectedEntity.idsubtiposus.toString();

        const sustrato: Sustrato = this.dataSustratos.find(data => data.id === idsustrato);
        const subtiposustrato: SubtipoSustrato = this.dataSubtiposSustratosFilt.find(
            data =>
            data.id === idsubtiposustrato);

        this.anchominimo = subtiposustrato.anchomin;
        this.altominimo = subtiposustrato.altomin;
        this.anchomaximo = subtiposustrato.anchomax;
        this.altomaximo = subtiposustrato.altomax;

        const ancho = (this.selectedEntity.ancho) ? this.selectedEntity.ancho : 0;
        const alto = (this.selectedEntity.alto) ? this.selectedEntity.alto : 0;
        this.selectedEntity.descripcion
            = sustrato.sustrato + ' - '
            + subtiposustrato.subtiposustrato + ': '
            + ancho +  ' x ' + alto ;

        const precio: LPrecioCarteleria = this.dataPrecios.find(
            data  =>
            +data.idsubtiposustrato === +this.selectedEntity.idsubtiposus &&
            +data.idlistaprecio === +this.selectedLPrecio.id);
        if (precio === undefined) {
            this.selectedEntity.precio = 0;
        } else {
            this.selectedEntity.precio = precio.precio;
        }
        const m2 = (+ancho) * (+alto);
        this.subtotal = precio.precio * m2 * this.selectedEntity.cantidad;
    }

    public onCantidadChange() {
    }

    public eliminar() {
        this.isLoading = true;
        this.presupuestoitemService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.presupuestoitemService.ENTITY_DELETED) {
                    this.showMessage('El ítem de cartelería se ha eliminado correctamente', true);
                } else  if (+response === this.presupuestoitemService.ERROR_FOREING_KEY) {
                    this.showMessage('El ítem de cartelería está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El ítem de cartelería no se ha podido eliminar!', false);
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    hasLista() {
        if (this.selectedLPrecio !== undefined) {
            if (this.selectedLPrecio.id > -1) {
                return true;
            }
        }
        return false;
    }

    getEstadoHabilitante() {
        if (this.selectedIDPresupuesto === -1) {
            return AppSettings.EstadosPresupuesto.nuevo;
        }
        if (TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.borrador;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecentrega)) {
            return AppSettings.EstadosPresupuesto.entegado;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecaproduccion)) {
            return AppSettings.EstadosPresupuesto.en_produccion;
        }
        if (!TWUtils.isNullDate(this.selectedPresupuesto.fecemision)) {
            return AppSettings.EstadosPresupuesto.emitido;
        }
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/presupuesto', this.selectedIDPresupuesto]);
                        } else {
                            this.router.navigate(['/presitcarteleria', this.selectedIDPresupuesto, this.selectedItemID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        this.isLoading = false;
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
