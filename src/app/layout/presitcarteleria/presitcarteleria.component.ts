import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { PresupuestoItemCarteleria,
    PresupuestoItemCarteleriaService } from '../../shared';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
    selector: 'app-form',
    templateUrl: './presitcarteleria.component.html',
    styleUrls: ['./presitcarteleria.component.scss'],
    animations: [routerTransition()],
    providers: [PresupuestoItemCarteleriaService]
})

export class PresItCarteleriaComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Item de Cartelería';

    public error = false;
    public loading = false;
    public selectedID: number;
    public selectedIDpresupuesto: number;
    public selectedEntity: PresupuestoItemCarteleria;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private presitemService: PresupuestoItemCarteleriaService) {
        this.selectedEntity = new PresupuestoItemCarteleria();
    }

    public ngOnInit(): void {
        this.loading = false;
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.selectedIDpresupuesto = +params['idpresupuesto'];
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        if (this.selectedID > -1) {
            this.presitemService.getByID(this.selectedID).then(
                response => {
                    this.loading = false;
                    this.selectedEntity = response as PresupuestoItemCarteleria;
                }
            ).catch(e => this.handleError(e));
        }
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public volver() {
        this.router.navigate(['/presupuesto', this.selectedIDpresupuesto]);
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/presupuesto', this.selectedIDpresupuesto]);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    public eliminar() {
        console.log('eliminar');
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
