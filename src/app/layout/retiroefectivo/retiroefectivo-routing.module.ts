import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RetiroEfectivoComponent } from './retiroefectivo.component';

const routes: Routes = [
    { path: '', component: RetiroEfectivoComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RetiroEfectivoRoutingModule { }
