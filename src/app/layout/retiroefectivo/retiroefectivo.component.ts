import { Component, OnInit, ViewContainerRef  } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { DatePipe } from '@angular/common';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import {
    RetiroEfectivo,
    RetiroEfectivoService,
    NgbDateCustomParserFormatter,
    Usuario,
    UsuarioService,
    Totales,
    RegistroCajaService,
    TWUtils } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './retiroefectivo.component.html',
    styleUrls: ['./retiroefectivo.component.scss'],
    animations: [routerTransition()],
    providers: [
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        DatePipe,
        RegistroCajaService,
        RetiroEfectivoService,
        UsuarioService]
})

export class RetiroEfectivoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Retiro de efectivo';

    public selectedID: number;
    public selectedEntity: RetiroEfectivo;
    public totales: Totales;
    public dataUsuarios: Array<any> = Array<any>();

    public fechaRetiro: {};

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private datePipe: DatePipe,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private retiroEfectivoService: RetiroEfectivoService,
        private usuarioService: UsuarioService,
        private registroCajaService: RegistroCajaService) {
            this.isLoading = true;
            this.selectedEntity = new RetiroEfectivo();
            this.selectedEntity.fecha = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
            this.fechaRetiro = TWUtils.stringDateToJson(this.selectedEntity.fecha);
            this.totales = new Totales();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getData();
            this.getTotales();
        });
    }

    public getData() {
        const p0 = this.usuarioService.getList();
        Promise.all([
            p0
        ]).then(([result0]) => {
            this.dataUsuarios = result0;
            this.getEntityData()
            });
    }

    public getTotales() {
        this.registroCajaService.getTotales().then(
            response => {
                if (response) {
                    this.totales = response;
                } else {
                    alert('No se han podido obtener los datos de totales!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getEntityData(): any {
        if (this.selectedID > -1) {
            this.retiroEfectivoService.getByID(this.selectedID)
            .then(
                response => {
                    this.selectedEntity = response as RetiroEfectivo;
                    if (this.selectedEntity === undefined) {
                        this.selectedEntity = new RetiroEfectivo();
                        this.selectedEntity.fecha = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
                        this.fechaRetiro = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                        this.isLoading = false;
                        return;
                    }
                    if (this.selectedEntity.fecha !== '' && this.selectedEntity.fecha !== '0000-00-00') {
                        this.fechaRetiro = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                    } else {
                        this.selectedEntity.fecha = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
                        this.fechaRetiro = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                    }
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        this.selectedEntity.fecha = TWUtils.arrayDateToString(this.fechaRetiro);
        this.retiroEfectivoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El retiro de efectivo se guardó correctamente', true);
                } else {
                    this.showMessage('No se han podido guardar los datos del retiro!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.retiroEfectivoService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.retiroEfectivoService.ENTITY_DELETED) {
                    this.showMessage('El retiro de efectivo se ha eliminado correctamente', true);
                } else  if (+response === this.retiroEfectivoService.ERROR_FOREING_KEY) {
                    this.showMessage('El retiro de efectivo está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El retiro de efectivo no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/retiroefectivos']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/retiroefectivos']);
                        } else {
                            this.router.navigate(['/retiroefectivo', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
