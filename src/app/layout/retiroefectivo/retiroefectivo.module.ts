import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { RetiroEfectivoRoutingModule } from './retiroefectivo-routing.module';
import { RetiroEfectivoComponent } from './retiroefectivo.component';
import { PageHeaderModule } from './../../shared';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        RetiroEfectivoRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [RetiroEfectivoComponent]
})
export class RetiroEfectivoModule { }
