import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from './../../shared';

import { TipoAdicionalRoutingModule } from './tipoadicional-routing.module';
import { TipoAdicionalComponent } from './tipoadicional.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        TipoAdicionalRoutingModule,
        PageHeaderModule,
        FormsModule,
        NgbModule.forRoot(),
        ReactiveFormsModule
    ],
    declarations: [TipoAdicionalComponent]
})
export class TipoAdicionalModule { }
