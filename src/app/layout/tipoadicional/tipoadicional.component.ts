import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {TipoAdicional, TipoAdicionalService } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './tipoadicional.component.html',
    styleUrls: ['./tipoadicional.component.scss'],
    animations: [routerTransition()],
    providers: [TipoAdicionalService]
})

export class TipoAdicionalComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Tipo de Adicional';

    public selectedID: number;
    public selectedEntity: TipoAdicional;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private tipoAdicionalService: TipoAdicionalService) {
        this.selectedEntity = new TipoAdicional();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    public getEntityData(): any {
      if (this.selectedID > -1) {
          this.tipoAdicionalService.getByID(this.selectedID)
          .then(
              response => {
                  this.selectedEntity = response as TipoAdicional;
              })
          .catch(e => this.handleError(e));
      }
    }

    public onSave() {
        this.tipoAdicionalService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El tipo de adicional se guardó correctamente!!', true);
                } else {
                    this.showMessage('No se han podido obtener los datos del tipo de adicional!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.tipoAdicionalService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.tipoAdicionalService.ENTITY_DELETED) {
                    this.showMessage('El tipo de adicional se ha eliminado correctamente', true);
                } else  if (+response === this.tipoAdicionalService.ERROR_FOREING_KEY) {
                    this.showMessage('El tipo de adicional está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El tipo de adicional no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/tiposadicionales']);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/tiposadicionales']);
                        } else {
                            this.router.navigate(['/tipoadicional', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
