import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipoAdicionalComponent } from './tipoadicional.component';

const routes: Routes = [
    { path: '', component: TipoAdicionalComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TipoAdicionalRoutingModule { }
