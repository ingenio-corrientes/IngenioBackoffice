import {
    Component, OnInit, Input, Output,
    EventEmitter, SimpleChange, ViewContainerRef,
    OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    GastoItem, GastoItemService,
    FormaPagoService,
    BancoService,
    ChequeService,
    TWUtils,
    Gasto, GastoService} from '../../../../shared/';

@Component({
    selector: 'app-item-carga',
    templateUrl: './item-carga.component.html',
    styleUrls: ['./item-carga.component.scss'],
    providers: [
        DatePipe,
        GastoItemService,
        FormaPagoService,
        BancoService,
        ChequeService,
        GastoService
    ]
})

export class ItemCargaComponent implements OnInit, OnChanges {
    @Input() selectedGasto: Gasto;
    @Input() selectedItem: GastoItem = null;
    @Output() itemUpdated = new EventEmitter();

    modalMessage: string;
    modalTitle = 'Gasto';
    // variables de campos
    fechaItem: {};
    fecVencimientoItem: {};
    propio: boolean;
    gasto = new Gasto();

    // datoa estructurados
    selectedEntity: GastoItem;
    selectedIDGasto: number;

    // arreglos
    dataFormasPagos: Array<any> = Array<any>();
    dataBancos: Array<any> = Array<any>();
    dataEnCartera: Array<any> = Array<any>();
    dataGastoItems: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private datePipe: DatePipe,
        private gastoItemService: GastoItemService,
        private formaPagoService: FormaPagoService,
        private bancoService: BancoService,
        private chequeService: ChequeService,
        private gastoService: GastoService) {
            this.cancelar();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDGasto = params['id'];
            this.getData();
        });
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedGasto'] !== undefined) {
            if (changes['selectedGasto'].currentValue !== null) {
                this.selectedGasto = changes['selectedGasto'].currentValue;
            }
        }

        if (changes['selectedItem'] !== undefined) {
            if (changes['selectedItem'].currentValue !== null) {
                this.selectedEntity = changes['selectedItem'].currentValue;
                this.propio = (this.selectedEntity.propio === 1);
                if (this.selectedEntity.fecha !== '' && this.selectedEntity.fecha !== '0000-00-00') {
                    this.fechaItem = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                    this.fecVencimientoItem = TWUtils.stringDateToJson(this.selectedEntity.fecvencimiento);
                } else {
                    this.selectedEntity.fecha = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
                    this.fechaItem = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                    this.fecVencimientoItem = TWUtils.stringDateToJson(this.selectedEntity.fecvencimiento);
                }
                const idformapago = this.selectedEntity.idformapago;
                this.selectedEntity.idformapago = -1;
                this.selectedEntity.idformapago = idformapago;
            }
        }
    }

    public getData() {
        const p0 = this.formaPagoService.getList();
        const p1 = this.bancoService.getList();
        const p2 = this.chequeService.getEnCartera();
        const p3 = this.gastoService.getByID(this.selectedIDGasto);
        const p4  = this.gastoItemService.getListByIDGasto(this.selectedIDGasto);
        Promise.all([
            p0, p1, p2, p3, p4
        ]).then(([result0, result1, result2, result3, result4]) => {
            this.dataFormasPagos = result0;
            this.dataBancos = result1;
            this.dataEnCartera = result2;
            this.gasto = result3;
            this.dataGastoItems = result4;
        }).catch(e => this.handleError(e));
    }

    onFormaPagoChange() {
        if (this.isNew() && +this.selectedEntity.monto === 0) {
            let saldo = 0;
            this.dataGastoItems.forEach(item => {
                saldo += +item.monto
            });
            if (this.gasto.totaldeuda > 0) {
                saldo = +this.gasto.totaldeuda - saldo;
                this.selectedEntity.monto = +saldo;
            }
        }
    }

    onSubmit() {
        this.selectedEntity.idgasto = this.selectedIDGasto;
        if (!this.isCheque()) {
            this.selectedEntity.origendestino = '';
        }
        if (this.isBancario()) {
            this.selectedEntity.fecha = TWUtils.arrayDateToString(this.fechaItem);
            this.selectedEntity.fecvencimiento = TWUtils.arrayDateToString(this.fecVencimientoItem);
            this.selectedEntity.propio = (this.propio) ? 1 : 0;
        } else {
            this.selectedEntity.fecha = '';
            this.selectedEntity.fecvencimiento = '';
            this.selectedEntity.idbanco = -1;
            this.selectedEntity.numero = '';
        }

        this.gastoItemService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.updateItemList();
                    this.cancelar();
                    this.itemUpdated.emit(true);
                } else {
                    this.showMessage('No se han podido guardar los datos del item de gasto!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    updateItemList() {
        this.gastoItemService.getListByIDGasto(this.selectedIDGasto).then(
            response => {
                if (response) {
                    this.dataGastoItems = response;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isEmited(): boolean {
        const retval = (this.gasto !== undefined) ? !TWUtils.isNullDate(this.gasto.fecemision) : false;
        return retval;
    }

    isCero() {
        return +this.selectedEntity.monto === 0;
    }

    public eliminar() {
      this.gastoItemService.delete(this.selectedEntity)
      .then(
          response => {
            if (response) {
                this.dataGastoItems = this.dataGastoItems.filter(item =>
                    item.id !== this.selectedEntity.id);
                this.itemUpdated.emit(true);
                this.cancelar();
            } else {
                this.showMessage('No se han podido eliminar los datos del item de gasto!!', false);
            }
        }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new GastoItem();

        this.propio = true;

        this.selectedEntity.fecha = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        this.fechaItem = TWUtils.stringDateToJson(this.selectedEntity.fecha);

        this.selectedEntity.fecvencimiento = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        this.fecVencimientoItem = TWUtils.stringDateToJson(this.selectedEntity.fecvencimiento);
        this.itemUpdated.emit(true);
    }

    isNew() {
      return (this.selectedEntity.id === -1);
    }

    public isBancario() {
        const formaPago = this.dataFormasPagos.find(
            x =>
            +x.id === +this.selectedEntity.idformapago);
        const retval = (typeof formaPago !== 'undefined') ? +formaPago.bancario === 1 : false;
        return retval;
    }

    public isCheque() {
        const formaCobro = this.dataFormasPagos.find(
            x =>
            +x.id === +this.selectedEntity.idformapago);
        const retval = (typeof formaCobro !== 'undefined') ? +formaCobro.cheque === 1 : false;
        return retval;
    }

    isPropio() {
        return (this.propio && this.isCheque());
    }

    isEnCartera() {
        return (!this.propio && this.isCheque());
    }

    onPropios() {
        this.propio = !this.propio;
    }

    onChangeEnCartera() {
        if (!this.propio) {
            const chequeActual = this.dataEnCartera.find(enc => {
                return enc.id === this.selectedEntity.idcheque;
            })
            this.selectedEntity.idbanco = chequeActual.idbanco;
            this.selectedEntity.numero = chequeActual.numero;
            this.selectedEntity.monto = chequeActual.monto;
            this.selectedEntity.origendestino = chequeActual.tenedororiginal;
            this.selectedEntity.fecvencimiento = chequeActual.fechaVencimiento;
        }
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/gastos']);
                        } else {
                            this.router.navigate(['/gasto', this.selectedIDGasto]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
