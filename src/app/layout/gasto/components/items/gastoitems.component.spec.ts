import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { GastoItemsComponent } from './gastoitems.component';

describe('GastoItemsComponent', () => {
  let component: GastoItemsComponent;
  let fixture: ComponentFixture<GastoItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot()
    ],
      declarations: [ GastoItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastoItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
