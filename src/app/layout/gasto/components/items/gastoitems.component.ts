import { Component, OnInit, Input, SimpleChange, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR, Gasto } from '../../../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { GastoItem, GastoItemService } from '../../../../shared/';

@Component({
    selector: 'app-gastoitems',
    templateUrl: './gastoitems.component.html',
    styleUrls: ['./gastoitems.component.scss'],
    providers: [
        { provide: LOCALE_ID, useValue: 'es-AR' },
        CurrencyAR,
        GastoItemService]
})

export class GastoItemsComponent implements OnInit {
    @Input() selectedGasto: Gasto;

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            formapago: { title: 'Forma de Pago', filter: false, },
            monto: {
                title: 'Monto',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public data: Array<any> = Array<any>();
    public selectedItem: GastoItem = null;
    public selectedIDGasto: number;
    public modalMessage: string;
    public modalTitle = 'Gasto';

    isLoading = false;

    public constructor(public router: Router,
        private cp: CurrencyAR,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private gastoItemService: GastoItemService) {
        this.source = new LocalDataSource(this.data);
        this.isLoading = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDGasto = params['id'];
            this.getData();
        });
    }

    handleItemUpdated(user) {
        this.getData();
    }

    public getData() {
        if (this.selectedIDGasto !== -1) {
            this.gastoItemService.getListByIDGasto(this.selectedIDGasto).then(
                response => {
                    if (response) {
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                        this.isLoading = false;
                    } else {
                        this.showMessage('No se han podido cargar datos de formas de pagos!!', false);
                        this.isLoading = false;
                    }
                }
            ).catch(e => this.handleError(e));
        }
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'formapago', search: query, },
            { field: 'monto', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data as GastoItem;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            if (goBack) {
                                this.router.navigate(['/gastos']);
                            } else {
                                this.router.navigate(['/gasto', this.selectedIDGasto]);
                            }
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
