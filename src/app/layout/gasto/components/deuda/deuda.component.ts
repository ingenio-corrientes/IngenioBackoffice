import {
    Component, OnInit, ViewContainerRef,
    Input, Output, EventEmitter,
    SimpleChange
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { LOCALE_ID } from '@angular/core';

import {
    Gasto, GastoService,
    GastoDeuda, GastoDeudaService, TWUtils
} from '../../../../shared/';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-deuda',
    templateUrl: './deuda.component.html',
    styleUrls: ['./deuda.component.scss'],
    providers: [
        DatePipe,
        GastoService,
        GastoDeudaService,
        { provide: LOCALE_ID, useValue: 'es-AR' }]
})
export class DeudaComponent implements OnInit {
    @Input() selectedGasto: Gasto;
    @Input() selectedItem: GastoDeuda;
    @Output() itemUpdated = new EventEmitter();

    public modalTitle = 'Gasto';

    // variables de campos
    public subtotal;

    // datos estructurados
    public selectedEntity: GastoDeuda;
    private selectedIDGasto: number;
    public fecEmision: {};

    // arreglos

    isLoading = false;

    public constructor(public router: Router,
        private dp: DatePipe,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private gastoService: GastoService,
        private gastodeudaService: GastoDeudaService) {
        this.subtotal = 0.00;
        this.cancelar();
        this.isLoading = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDGasto = params['id'];
            this.getGetCobranzaData();
        });
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedGasto'] !== undefined) {
            if (changes['selectedGasto'].currentValue !== null) {
                this.selectedGasto = changes['selectedGasto'].currentValue;
                this.showDates();
            }
        }

        if (changes['selectedItem'] !== undefined) {
            if (changes['selectedItem'].currentValue !== null) {
                this.selectedEntity = changes['selectedItem'].currentValue;
                this.showDates();
            }
        }
    }

    showDates() {
        this.fecEmision = '';
        if (!TWUtils.isNullDate(this.selectedEntity.fecemision)) {
            this.fecEmision = this.dp.transform(this.selectedEntity.fecemision, 'dd-MM-yyyy');
        }
    }

    public getGetCobranzaData(): any {
        this.gastoService.getByID(this.selectedIDGasto).then(
            response => {
                this.selectedGasto = response as Gasto;
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    public isEmited() {
        return !TWUtils.isNullDate(this.selectedGasto.fecemision);
    }

    onSave() {
        if (this.selectedEntity === undefined) {
            return;
        }
        if (this.selectedEntity === null) {
            return;
        }
        this.selectedEntity.idgasto = this.selectedIDGasto;
        if (this.selectedEntity.saldo < this.selectedEntity.montoacancelar) {
            this.showMessage('El monto a cancelar no puede ser mayor al saldo!!', false);
            return;
        }
        this.gastodeudaService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                    this.cancelar();
                } else {
                    this.showMessage('No se ha podido guardar los datos de la deuda!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        if (this.selectedEntity === undefined) {
            return;
        }
        if (this.selectedEntity === null) {
            return;
        }
        this.gastodeudaService.delete(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        this.cancelar();
                    } else {
                        this.showMessage('No se ha podido eliminar los datos de la deuda!!', false);
                    }
                }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new GastoDeuda();
        this.showDates();
        this.itemUpdated.emit(true);
    }

    isNew() {
        return (this.selectedEntity !== null) ? (this.selectedEntity.id === -1) : (true);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
