import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GastoDataComponent } from './gasto-data.component';

describe('GastoDataComponent', () => {
  let component: GastoDataComponent;
  let fixture: ComponentFixture<GastoDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot()
    ],
      declarations: [ GastoDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastoDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
