import { Component, Input, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Gasto, GastoService,
    ProveedorService,
    TWUtils } from '../../../../shared';

@Component({
    selector: 'app-gastodata',
    templateUrl: './gasto-data.component.html',
    styleUrls: ['./gasto-data.component.scss'],
    providers: [
        GastoService,
        ProveedorService]
})

export class GastoDataComponent implements OnInit {
    @Input() selectedGasto: Gasto;
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Gasto';

    public dataProveedores: Array<any> = Array<any>();

    public selectedEntity: Gasto;
    public selectedID: number;
    public fechaGasto: {};

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private gastoService: GastoService,
        private proveedorService: ProveedorService) {
        this.selectedEntity = new Gasto();
        this.showDates();
        this.isLoading = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getData();
        });
    }

    public getData() {
        const p0 = this.getProveedores();
        Promise.all([
            p0
        ]).then(
            this.onSetEntityData()
            );
    }

    public getProveedores() {
        return this.proveedorService.getList().then(
            response => {
                if (response) {
                    this.dataProveedores = response;
                } else {
                    this.showMessage('No se han podido obtener datos de proveedores!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onSetEntityData(): any {
        if (this.selectedID > -1) {
            this.gastoService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Gasto;
                    this.fechaGasto = TWUtils.stringDateToJson(this.selectedEntity.fecha);
                    this.isLoading = false;
                })
            .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        this.selectedEntity.fecha = TWUtils.arrayDateToDateTimeString(this.fechaGasto);
        this.gastoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El recibo se guardó correctamente', false);
                } else {
                    this.showMessage('El recibo no se guardó!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    showDates() {
        this.fechaGasto = '';
        if (!TWUtils.isNullDate(this.selectedEntity.fecha)) {
            this.fechaGasto = this.selectedEntity.fecha;
        } else {
            this.fechaGasto = TWUtils.getTodayJson();
        }
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public isEmited(): boolean {
        return !TWUtils.isNullDate(this.selectedEntity.fecemision);
    }

    public eliminar() {
        this.gastoService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.gastoService.ENTITY_DELETED) {
                    this.showMessage('El gasto se ha eliminado correctamente', true);
                } else  if (+response === this.gastoService.ERROR_FOREING_KEY) {
                    this.showMessage('El gasto está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El gasto no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/gastos']);
                        } else {
                            this.router.navigate(['/gasto', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
