import { Component, OnInit, Input, SimpleChange, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';

import { Gasto, GastoDeuda, GastoDeudaService, GastoService, TWUtils } from '../../../../shared/';

@Component({
    selector: 'app-deudas',
    templateUrl: './deudas.component.html',
    styleUrls: ['./deudas.component.scss'],
    providers: [
        DatePipe,
        GastoDeudaService,
        GastoService,
        CurrencyPipe,
        { provide: LOCALE_ID, useValue: 'es-AR' }]
})

export class DeudasComponent implements OnInit, OnChanges {
    @Input() selectedGasto: Gasto;

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            tipocomprobante: { title: 'Tipo', filter: false, },
            numero: { title: 'Número', filter: false, },
            fecemision: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            saldo: {
                title: 'Saldo',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return this.cp.transform(value, 'ARS', true, '1.2-2');
                }
            },
            montoacancelar: {
                title: 'Monto a Cancelar',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return this.cp.transform(value, 'ARS', true, '1.2-2');
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered table-hover',
        },
    };
    source: LocalDataSource;

    public data: Array<any> = Array<any>();
    public selectedItem: GastoDeuda = null;
    public selectedIDDeuda: number;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private datePipe: DatePipe,
        private cp: CurrencyPipe,
        private gastoService: GastoService,
        private deudaService: GastoDeudaService) {
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDDeuda = params['id'];
            this.getData();
        });
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedGasto'].currentValue !== null) {
            this.selectedGasto = changes['selectedGasto'].currentValue;
        }
    }

    handleItemUpdated(user) {
        this.getData();
    }

    public getData() {
        if (this.selectedIDDeuda !== -1) {
            this.deudaService.getDeudasByIdGasto(this.selectedIDDeuda).then(
                response => {
                    if (response) {
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                    } else {
                        alert('No se pudo obtener los datos de deudas!!');
                    }
                }
            ).catch(e => this.handleError(e));
        }
    }

    public isEmited() {
        return !TWUtils.isNullDate(this.selectedGasto.fecemision);
    }

    onReloadDeudas() {
        this.deudaService.reloadDeudas(this.selectedIDDeuda).then(
            response => {
                if (response) {
                    this.getData();
                } else {
                    alert('No se pudo obtener los datos de deudas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'tipocomprobante', search: query, },
            { field: 'numero', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data;
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
