import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

// relative import
import { GastoRoutingModule } from './gasto-routing.module';
import { GastoComponent } from './gasto.component';
import {
  GastoDataComponent,
  GastoItemsComponent,
  ItemCargaComponent,
  DeudasComponent,
  DeudaComponent
} from './components/';

@NgModule({
    imports: [
        CommonModule,
        GastoRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      GastoComponent,
      GastoDataComponent,
      GastoItemsComponent,
      ItemCargaComponent,
      DeudasComponent,
      DeudaComponent
    ]
})
export class GastoModule { }
