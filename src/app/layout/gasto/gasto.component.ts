import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { CurrencyAR } from '../../shared/pipes/currency-ar';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import { Gasto, GastoService, GastoDeudaService, NgbDateCustomParserFormatter,
    FormaPagoService, AppSettings, GastoItemService } from '../../shared';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { GenGasto } from '../../shared/services/vouchers/gen-gasto';

@Component({
    selector: 'app-form',
    templateUrl: './gasto.component.html',
    styleUrls: ['./gasto.component.scss'],
    animations: [routerTransition()],
    providers: [
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        GastoService,
        GastoItemService,
        GastoDeudaService,
        FormaPagoService,
        CurrencyAR ]
})

export class GastoComponent implements OnInit {
    public modalTitle = 'Gasto';

    public error = false;
    public loading = false;
    public selectedID: number;
    public selectedEntity: Gasto;
    selectedGasto: Gasto;

    public itemsDeuda: Array<any> = Array<any>();
    public itemsFormaCobro: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private cp: CurrencyAR,
        private gastoService: GastoService,
        private gastoItemService: GastoItemService,
        private gastodeudaService: GastoDeudaService,
        private formaPagoService: FormaPagoService) {
        this.selectedEntity = new Gasto();
        this.selectedGasto = this.selectedEntity;
    }

    public ngOnInit(): void {
        this.loading = false;
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.onGetEntityData(false);
        });
    }

    public onGetEntityData(genGS: boolean): any {
        if (this.selectedID > -1) {
            this.gastoService.getByID(this.selectedID).then(
                response => {
                    this.loading = false;
                    this.selectedEntity = response as Gasto;
                    this.selectedGasto = this.selectedEntity;
                    if (genGS) {
                        this.getItemsData();
                    }
                }
            ).catch(e => this.handleError(e));
        }
    }

    public getItemsData() {
        const p0 = this.gastodeudaService.getDeudasByIdGasto(this.selectedID);
        const p1 = this.gastoItemService.getListByIDGasto(this.selectedID);
        Promise.all([
            p0, p1
        ]).then( ([result1, result2]) => {
            this.itemsDeuda = result1;
            this.itemsFormaCobro = result2;
            this.genVoucher();
            }
        );
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public registrar(content) {
        this.selectedEntity.dirty = 0;
        this.gastoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.selectedID = Number(response);
                    this.showMessage('El gasto se registró correctamente', false);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getEstado() {
        if (this.selectedID === -1) {
            return AppSettings.EstadosRecibo.nuevo;
        }
        if (!(+this.selectedEntity.monto > 0)) {
            return AppSettings.EstadosRecibo.borrador;
        }
    }

    public volver() {
        this.router.navigate(['/gastos']);
    }

    public emitir() {
        this.gastoService.emitir(this.selectedEntity).then(
            response => {
                let msg = 'ok';
                switch (+response) {
                    case this.gastoService.ESTADO_OK:
                    msg = 'La cobranza se emitió correctamente.';
                    this.onGetEntityData(true);
                    break;
                    case this.gastoService.ESTADO_SINDEUDA:
                    msg = 'La cobranza no posee deudas.';
                    break;
                    case this.gastoService.ESTADO_SINPAGOS:
                    msg = 'La cobranza no posee pagos';
                    break;
                    case this.gastoService.ESTADO_ERRORSALDO:
                    msg = 'La diferencia entre las deudas y los pagos es distinta de cero';
                    break;
                    default:
                    msg = response;
                    break;
                }
                this.showMessage(msg, false);
            }
        ).catch(e => this.handleError(e));
    }

    public genVoucher(): any {
        const presup = new GenGasto(this.cp);
        presup.genGasto(this.selectedEntity, this.itemsDeuda, this.itemsFormaCobro);
    }

    public eliminar() {
        console.log('eliminar');
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/gastos']);
                        } else {
                            this.router.navigate(['/gasto', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
