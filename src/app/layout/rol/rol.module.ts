import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { PaginationModule } from 'ng2-bootstrap';
// import { Ng2TableModule } from 'ng2-table';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { RolRoutingModule } from './rol-routing.module';
import { RolComponent } from './rol.component';
import {
  RolDataComponent,
  ItemsComponent,
  ItemCargaComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        RolRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
        // Ng2TableModule,
        // PaginationModule.forRoot()
    ],
    declarations: [
      RolComponent,
      RolDataComponent,
      ItemsComponent,
      ItemCargaComponent
    ]
})
export class RolModule { }
