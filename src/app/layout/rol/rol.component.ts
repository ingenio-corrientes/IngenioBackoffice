import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { Rol, RolService } from '../../shared';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
    selector: 'app-form',
    templateUrl: './rol.component.html',
    styleUrls: ['./rol.component.scss'],
    animations: [routerTransition()],
    providers: [
        RolService]
})

export class RolComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Rol';

    public error = false;
    public loading = false;
    public selectedID: number;
    public selectedEntity: Rol;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private rolService: RolService) {
        this.selectedEntity = new Rol();
    }

    public ngOnInit(): void {
        this.loading = false;
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        if (this.selectedID > -1) {
            this.rolService.getByID(this.selectedID).then(
                response => {
                    this.loading = false;
                    this.selectedEntity = response as Rol;
                }
            ).catch(e => this.handleError(e));
        }
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/roles']);
    }

    public registrar(content) {
        this.rolService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.selectedID = Number(response);
                    this.showMessage(content, 'El rol se registró correctamente', this.MESSAGE_STAYHERE);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(content, message, action) {
        this.modalMessage = message;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            if (action === this.MESSAGE_GOTO) {
                this.router.navigate(['/rol', this.selectedEntity.id]);
            }
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    public eliminar() {
        console.log('eliminar');
    }

    private handleError(error: any): Promise<any> {
        this.loading = false;
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
