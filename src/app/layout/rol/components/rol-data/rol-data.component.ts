import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    Rol, RolService,
    TWUtils } from '../../../../shared';

@Component({
    selector: 'app-roldata',
    templateUrl: './rol-data.component.html',
    styleUrls: ['./rol-data.component.scss'],
    providers: [
        RolService]
})

export class RolDataComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Rol';

    public error = false;
    public loading = false;
    submitted = false;

    public selectedEntity: Rol;
    public selectedID: number;
    public fechaGasto: {};

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private rolService: RolService) {
        this.selectedEntity = new Rol();
        this.error = false;
        this.loading = false;
    }

    public ngOnInit(): void {
        this.loading = false;
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.onSetEntityData();
        });
    }

    public onSetEntityData(): any {
        if (this.selectedID > -1) {
            this.rolService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Rol;
                    this.loading = false;
                }
            ).catch(e => this.handleError(e));
        } else {
            this.loading = false;
        }
    }

    public onSave() {
        this.rolService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.selectedID = Number(response);
                    this.showMessage('El rol se guardó correctamente', false);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public eliminar() {
        this.rolService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.rolService.ENTITY_DELETED) {
                    this.showMessage('El rol se ha eliminado correctamente', true);
                } else  if (+response === this.rolService.ERROR_FOREING_KEY) {
                    this.showMessage('El rol está siendo utilizado y no puede eliminarse.', false);
                } else {
                    this.showMessage('El rol no se ha podido eliminar!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/roles']);
                        } else {
                            this.router.navigate(['/rol', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
