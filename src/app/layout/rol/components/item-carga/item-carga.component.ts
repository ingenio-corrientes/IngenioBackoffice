import { Component, OnInit, Input, Output, OnChanges, EventEmitter, SimpleChange  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import {
    ModuloXRol, ModuloXRolService,
    TWUtils } from '../../../../shared/';

@Component({
    selector: 'app-item-carga',
    templateUrl: './item-carga.component.html',
    styleUrls: ['./item-carga.component.scss'],
    providers: [
        ModuloXRolService
    ]
})
export class ItemCargaComponent implements OnInit {
    @Input() selectedItem: ModuloXRol;
    @Output() itemUpdated = new EventEmitter();

    // datos estructurados
    public selectedEntity: ModuloXRol;
    private selectedIDRol: number;
    public ver: boolean;
    public editar: boolean;
    public eliminar: boolean;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private moduloXRolService: ModuloXRolService) {
        this.selectedEntity = new ModuloXRol();
        this.ver = false;
        this.editar = false;
        this.eliminar = false;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDRol = params['id'];
        });
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue;
            this.ver = this.selectedEntity.ver === 1;
            this.editar = this.selectedEntity.editar === 1;
            this.eliminar = this.selectedEntity.eliminar === 1;
        }
    }

    onSubmit() {
        this.selectedEntity.idrol = this.selectedIDRol;
        this.selectedEntity.ver = this.ver ? 1 : 0;
        this.selectedEntity.editar = this.editar ? 1 : 0;
        this.selectedEntity.eliminar = this.eliminar ? 1 : 0;

        this.moduloXRolService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                    this.itemUpdated.emit(true);
                } else {
                    alert('No se ha podido obtener los módulos del rol seleccionado!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new ModuloXRol();
    }

    isNew() {
      return (this.selectedEntity.id === -1);
    }

    isLoad() {
        return (this.selectedEntity.id > 0);
    }

    canVer() {
        return this.selectedEntity.ver === 1;
    }

    canEditar() {
        return this.selectedEntity.editar === 1;
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
