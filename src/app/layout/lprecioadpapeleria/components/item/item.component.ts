import { Component, OnInit, Input, Output, OnChanges, EventEmitter, SimpleChange, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    LPrecioAdicional, LPrecioAdicionalService,
    TipoAdicionalService,
    EventDataChange,
    AppSettings
} from '../../../../shared/';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    providers: [
        LPrecioAdicionalService,
        TipoAdicionalService]
})

export class ItemComponent implements OnInit, OnChanges {
    @Input() selectedItem: LPrecioAdicional;
    @Output() itemUpdated = new EventEmitter();
    public modalMessage: string;
    public modalTitle = 'LPrecioadpapeleria';

    // datos estructurados
    public selectedEntity: LPrecioAdicional;
    private selectedIDLista: number;

    // arreglos
    public dataTiposAdicionales: Array<any> = Array<any>();

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private itemsService: LPrecioAdicionalService,
        private tiposadicionalesService: TipoAdicionalService) {
        this.cancelar();
        this.isLoading = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDLista = params['id'];
            this.getData();
        });
    }
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedItem'].currentValue !== null) {
            this.selectedEntity = changes['selectedItem'].currentValue;
            this.isLoading = false;
        }
    }

    public getData() {
        this.tiposadicionalesService.getList().then(
            response => {
                if (response) {
                    this.dataTiposAdicionales = response;
                    this.isLoading = false;
                } else {
                    this.showMessage('No se has podido obtener los datos de sustratos!!', false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSave() {
        this.selectedEntity.idlistaprecio = this.selectedIDLista;
        this.itemsService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    const selectedItem = new EventDataChange();
                    selectedItem.eventType = AppSettings.EventType.update;
                    selectedItem.object = this.selectedEntity;
                    this.itemUpdated.emit(selectedItem);

                    this.selectedEntity = response;
                    this.cancelar();
                } else {
                    this.showMessage('No se ha podido guardar los datos del adicional de lista de precio!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.itemsService.delete(this.selectedEntity)
            .then(
                response => {
                    if (response) {
                        this.itemUpdated.emit(true);
                        this.cancelar();
                    } else {
                        this.showMessage('No se ha podido eliminar los datos del adicional de lista de precio!!', false);
                    }
                }).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new LPrecioAdicional();
        this.itemUpdated.emit(true);
    }

    isNew() {
        return (this.selectedEntity !== null) ? (this.selectedEntity.id === -1) : (true);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            if (goBack) {
                                this.router.navigate(['/lprecioadpapeleria']);
                            } else {
                                this.router.navigate(['/lprecioadpapeleria', this.selectedIDLista]);
                            }
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        return Promise.reject(error.message || error);
    }
}
