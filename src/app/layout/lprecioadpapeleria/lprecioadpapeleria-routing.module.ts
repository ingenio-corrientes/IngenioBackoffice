import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LPrecioAdPapeleriaComponent } from './lprecioadpapeleria.component';

const routes: Routes = [
    { path: '', component: LPrecioAdPapeleriaComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LPrecioAdPapeleriaRoutingModule { }
