import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LPrecioAdPapeleriaComponent } from './lprecioadpapeleria.component';
// import {
//   DataComponent,
//   ItemsComponent
// } from './components/';

import { PageHeaderModule } from './../../shared';

describe('LPrecioAdPapeleriaComponent', () => {
  let component: LPrecioAdPapeleriaComponent;
  let fixture: ComponentFixture<LPrecioAdPapeleriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        LPrecioAdPapeleriaComponent,
        // DataComponent,
        // ItemsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LPrecioAdPapeleriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
