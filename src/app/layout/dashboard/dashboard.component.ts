import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';

import { Resumen, ReporteService } from '../../shared';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()],
    providers: [
        ReporteService,
    ]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    resumenEntity: Resumen;

    constructor(public router: Router,
        private reporteService: ReporteService) {
        this.resumenEntity = new Resumen();
    }

    ngOnInit() {
        this.getData();
    }

    public getData(): any {
        this.reporteService.getList().then(
            response => {
                if (response) {
                    this.resumenEntity = response;
                }
                // else {
                //     alert('No se han podido cargar datos de gastos!!');
                // }
            }
        ).catch(e => this.handleError(e));
    }

    goResumenAProduccion() {
        this.router.navigate(['/presupuestos']);
    }

    goNoAprobados() {
        this.router.navigate(['/presupuestos']);
    }

    goCaja() {
        this.router.navigate(['/caja']);
    }

    goClienteCtaCte() {
        this.router.navigate(['/cuentascorrientes']);
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    public reclamosTotal() {
        console.log('reclamosTotal');
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
