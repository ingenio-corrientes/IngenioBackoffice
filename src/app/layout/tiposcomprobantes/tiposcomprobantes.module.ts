import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { TiposComprobantesRoutingModule } from './tiposcomprobantes-routing.module';
import { TiposComprobantesComponent } from './tiposcomprobantes.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        TiposComprobantesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [TiposComprobantesComponent]
})
export class TiposComprobantesModule { }
