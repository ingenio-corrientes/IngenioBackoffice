import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TiposComprobantesComponent } from './tiposcomprobantes.component';

const routes: Routes = [
    { path: '', component: TiposComprobantesComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TiposComprobantesRoutingModule { }
