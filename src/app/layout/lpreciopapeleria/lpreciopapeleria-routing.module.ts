import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LPrecioPapeleriaComponent } from './lpreciopapeleria.component';

const routes: Routes = [
    { path: '', component: LPrecioPapeleriaComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LPrecioPapeleriaRoutingModule { }
