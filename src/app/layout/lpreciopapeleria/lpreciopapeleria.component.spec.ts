import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LPrecioPapeleriaComponent } from './lpreciopapeleria.component';
// import {
//   DataComponent,
//   ItemsComponent
// } from './components/';

import { PageHeaderModule } from './../../shared';

describe('LPrecioPapeleriaComponent', () => {
  let component: LPrecioPapeleriaComponent;
  let fixture: ComponentFixture<LPrecioPapeleriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        LPrecioPapeleriaComponent,
        // DataComponent,
        // ItemsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LPrecioPapeleriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
