import { Component, OnInit, EventEmitter, Output, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR } from '../../../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    RegistroCaja, ChequeEnCartera,
    RegistroCajaService,
    TWUtils, AppSettings} from '../../../../shared';

@Component({
    selector: 'app-iniciodia',
    templateUrl: './iniciodia.component.html',
    styleUrls: ['./iniciodia.component.scss'],
    providers: [
        {provide: LOCALE_ID, useValue: 'es-AR'},
        CurrencyAR,
        RegistroCajaService,
        DatePipe]
})

export class InicioDiaComponent implements OnInit {
    @Output() dataUpdated = new EventEmitter();
    public modalTitle = 'Caja';

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            numero: { title: 'Número', filter: false, },
            banco: { title: 'Banco', filter: false, },
            fecemision: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            monto: {
                title: 'Monto',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public data: Array<any> = Array<any>();

    public selectedEntity: RegistroCaja;
    public selectedID: number;
    public diferencia = 0;
    public cantidad = 0;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private datePipe: DatePipe,
        private cp: CurrencyAR,
        private modalService: NgbModal,
        public datepipe: DatePipe,
        private registroCajaService: RegistroCajaService) {
        this.isLoading = true;
        this.selectedEntity = new RegistroCaja();
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.onSetEntityData();
        this.getChequesEnCartera();
    }

    public onSetEntityData(): any {
        this.registroCajaService.getRegistro().then(
            response => {
                this.selectedEntity = new RegistroCaja();
                if (response !== undefined) {
                    this.selectedEntity.efectivoinicio = Number(response.efectivoarqueo);
                    this.selectedEntity.efectivoanterior = Number(response.efectivoarqueo);
                }
                this.diferencia = (+this.selectedEntity.efectivoinicio) - (+this.selectedEntity.efectivoanterior);
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    public getChequesEnCartera() {
        this.registroCajaService.getChequesEnCartera().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.cantidad = this.data.length;
                } else {
                    alert('No se han podido obtener los datos de cheques en cartera!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onDataChange() {
        this.diferencia = (+this.selectedEntity.efectivoinicio) - (+this.selectedEntity.efectivoanterior);
    }

    public onSave() {
        this.selectedEntity.fecinicio = this.datepipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
        this.registroCajaService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.dataUpdated.emit(true);
                    this.showMessage('El inicio de día se realizó correctamente', false);
                } else {
                    this.showMessage('El inicio de día no se pudo realizar correctamente!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isOpenable() {
        return (!TWUtils.isNullDate(this.selectedEntity.fecinicio) && !TWUtils.isNullDate(this.selectedEntity.fecarqueo))
            || (TWUtils.isNullDate(this.selectedEntity.fecinicio) && TWUtils.isNullDate(this.selectedEntity.fecarqueo));
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/dashboard']);
                        } else {
                            this.router.navigate(['/caja']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
