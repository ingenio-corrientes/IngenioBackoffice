import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InicioDiaComponent } from './iniciodia.component';

describe('InicioDiaComponent', () => {
  let component: InicioDiaComponent;
  let fixture: ComponentFixture<InicioDiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot()
    ],
      declarations: [ InicioDiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioDiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
