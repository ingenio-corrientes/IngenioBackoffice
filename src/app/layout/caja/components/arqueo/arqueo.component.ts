import { Component, OnInit, EventEmitter, Output, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { LocalDataSource } from 'ng2-smart-table';
import { CurrencyAR } from '../../../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    RegistroCaja, Totales,
    RegistroCajaService,
    TWUtils} from '../../../../shared';

@Component({
    selector: 'app-arqueo',
    templateUrl: './arqueo.component.html',
    styleUrls: ['./arqueo.component.scss'],
    providers: [
        RegistroCajaService,
        CurrencyAR,
        DatePipe]
})

export class ArqueoComponent implements OnInit {
    @Output() dataUpdated = new EventEmitter();
    public modalTitle = 'Caja';

    settingscobros = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            forma: { title: 'Tipo', filter: false, },
            total: {
                title: 'Total',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_left"> ` + this.cp.transform(value) + ` </div>`;
                } }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    sourcecobros: LocalDataSource;

    settingspagos = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            forma: { title: 'Tipo', filter: false, },
            total: {
                title: 'Total',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_left"> ` + this.cp.transform(value) + ` </div>`;
                } }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    sourcepagos: LocalDataSource;

    public data: Array<any> = Array<any>();

    public dataPresupuestos: Array<any> = Array<any>();

    public selectedEntity = new RegistroCaja();
    public totales: Totales;
    public diferencia = 0;
    public cantidad = 0;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private datepipe: DatePipe,
        private cp: CurrencyAR,
        private registroCajaService: RegistroCajaService) {
        this.isLoading = true;
        this.sourcecobros = new LocalDataSource(this.data);
        this.sourcepagos = new LocalDataSource(this.data);
        this.totales = new Totales();
    }

    public ngOnInit(): void {
        this.onSetEntityData();
        this.getTotales();
        this.getFormasCobroPagos();
    }

    public onSetEntityData(): any {
        this.registroCajaService.getRegistro().then(
            response => {
                 this.selectedEntity = response as RegistroCaja;
                if (this.selectedEntity === undefined) {
                    this.selectedEntity = new RegistroCaja();
                }
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    public getTotales() {
        this.registroCajaService.getTotales().then(
            response => {
                if (response) {
                    this.totales = response;
                } else {
                    alert('No se han podido obtener los datos de totales!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getFormasCobroPagos() {
        this.registroCajaService.getFormasCobroPagos().then(
            response => {
                if (response) {
                    this.data = response;
                    const dataCobros = this.data.filter(data => +data.tipo === 0);
                    this.sourcecobros = new LocalDataSource(dataCobros);
                    const dataPagos = this.data.filter(data => +data.tipo === 1);
                    this.sourcepagos = new LocalDataSource(dataPagos);
                } else {
                    alert('No se han podido obtener los datos de cobros y pagos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onDataChange() {
        this.diferencia = (+this.selectedEntity.efectivoinicio) - (+this.selectedEntity.efectivoanterior);
    }

    public onSave() {
        this.selectedEntity.fecarqueo = this.datepipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
        this.selectedEntity.efectivoarqueo = (+this.selectedEntity.efectivoinicio)
             + (+this.totales.efectivo)
             - (+this.totales.totalretiros);
        this.registroCajaService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.dataUpdated.emit(true);
                    this.showMessage('El arqueo se realizó correctamente', false);
                } else {
                    this.showMessage('El arqueo no se pudo realizar correctamente!!', false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isArqueable() {
        const ini = TWUtils.isNullDate(this.selectedEntity.fecinicio);
        const arq = TWUtils.isNullDate(this.selectedEntity.fecarqueo);
        return (TWUtils.isNullDate(this.selectedEntity.fecarqueo) &&
            !TWUtils.isNullDate(this.selectedEntity.fecinicio));
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/dashboard']);
                        } else {
                            this.router.navigate(['/caja']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
