import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR } from '../../../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { RegistroCajaService } from '../../../../shared/';

@Component({
    selector: 'app-retiros',
    templateUrl: './retiros.component.html',
    styleUrls: ['./retiros.component.scss'],
    providers: [
        { provide: LOCALE_ID, useValue: 'es-AR' },
        CurrencyAR, RegistroCajaService]
})

export class RetirosComponent implements OnInit {
    public modalTitle = 'RetiroEfectivo';
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            responsable: { title: 'Responsable', filter: false, },
            concepto: { title: 'Concepto', filter: false, },
            monto: {
                title: 'Monto',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;

    public data: Array<any> = Array<any>();

    isLoading = false;

    public constructor(public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private cp: CurrencyAR,
        private route: ActivatedRoute,
        private retirosService: RegistroCajaService) {
        this.isLoading = true;
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.getData();
        });
    }

    public getData() {
        this.retirosService.getRetirosEfectivos().then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.isLoading = false;
                } else {
                    this.showMessage('No se han podido cargar datos de retiros de efectivo!!', false);
                    this.isLoading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/retiroefectivo', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            if (goBack) {
                                this.router.navigate(['/dashboard']);
                            } else {
                                this.router.navigate(['/caja']);
                            }
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
