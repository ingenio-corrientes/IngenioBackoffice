import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageHeaderModule } from './../../shared';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { CajaRoutingModule } from './caja-routing.module';
import { CajaComponent } from './caja.component';
import {
  InicioDiaComponent,
  ArqueoComponent,
  RetirosComponent
} from './components/';

@NgModule({
    imports: [
        CommonModule,
        CajaRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      CajaComponent,
      InicioDiaComponent,
      ArqueoComponent,
      RetirosComponent
    ]
})
export class CajaModule { }
