import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { RegistroCaja, RegistroCajaService, TWUtils } from '../../shared';

import 'rxjs/add/observable/forkJoin';

@Component({
    selector: 'app-form',
    templateUrl: './caja.component.html',
    styleUrls: ['./caja.component.scss'],
    animations: [routerTransition()],
    providers: [
        RegistroCajaService]
})

export class CajaComponent implements OnInit {
    public modalTitle = 'Caja';

    public selectedID: number;
    public selectedEntity: RegistroCaja;

    tabActivo: string;

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private viewContainer: ViewContainerRef,
        private modal: ModalDialogService,
        private cajaService: RegistroCajaService) {
        this.tabActivo = '0',
        this.isLoading = true;
        this.selectedEntity = new RegistroCaja();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        this.cajaService.getRegistro().then(
            response => {
                this.selectedEntity = response as RegistroCaja;
                if (this.selectedEntity === undefined) {
                    this.selectedEntity = new RegistroCaja();
                }
                this.tabActivo = (this.isArqueable()) ? '1' : '0';
                this.isLoading = false;
            }
        ).catch(e => this.handleError(e));
    }

    handleDataUpdated(data) {
      this.onGetEntityData();
    }

    public isArqueable() {
        const ini = TWUtils.isNullDate(this.selectedEntity.fecinicio);
        const arq = TWUtils.isNullDate(this.selectedEntity.fecarqueo);
        const retVal = (TWUtils.isNullDate(this.selectedEntity.fecarqueo) &&
            !TWUtils.isNullDate(this.selectedEntity.fecinicio));
        return retVal;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        // if (goBack) {
                        //     this.router.navigate(['/compras']);
                        // } else {
                        //     this.router.navigate(['/compra', this.selectedID]);
                        // }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
