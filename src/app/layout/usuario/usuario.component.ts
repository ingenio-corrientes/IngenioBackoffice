import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    ModuloXRolService,
    Usuario, ResetPass,
    UsuarioService, Rol,
    RolService, TWUtils } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './usuario.component.html',
    styleUrls: ['./usuario.component.scss'],
    animations: [routerTransition()],
    providers: [
        UsuarioService,
        RolService,
        ModuloXRolService]
})

export class UsuarioComponent implements OnInit {
    readonly IDMODULO = 2;
    readonly MESSAGE_STAYHERE = 'listaprecio';
    readonly MESSAGE_GOTO = 'listasprecios';
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Usuario';

    public error = false;
    public loading = false;
    submitted = false;
    public selectedID: number;
    public selectedEntity: Usuario;
    public dataUsuarios: Array<any> = Array<any>();
    public dataRoles: Array<any> = Array<any>();
    loggedId: number;

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private usuarioService: UsuarioService,
        private rolService: RolService,
        private moduloXRolService: ModuloXRolService) {
        this.selectedEntity = new Usuario();
        this.isLoading = true;
    }

    public ngOnInit(): void {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedId = +jsonData.id;
        this.evalPermisos();
        this.route.params.subscribe(params => {
            this.loading = true;
            this.selectedID = +params['id'];
            this.getEntityData();
            this.getClienteTipo();
            this.loading = false;
        });
    }

    evalPermisos() {
        this.moduloXRolService.getListByIDUsuarioIDModulo(this.loggedId, this.IDMODULO)
            .then(response => {
                if (response) {
                    if (+response.ver !== 1) {
                        this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    }
                    this.isLoading = false;
                } else {
                    this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    public getEntityData(): any {
        if (this.selectedID > -1) {
            this.usuarioService.getByID(this.selectedID)
            .then(
                response => {
                    this.loading = false;
                    this.selectedEntity = response as Usuario;
                })
            .catch(e => this.handleError(e));
        }
    }

    public getClienteTipo(): any {
        this.rolService.getList()
        .then(
            response => {
                this.dataRoles = response as Rol[];
            })
        .catch(e => this.handleError(e));
    }

    public onSave() {
        this.usuarioService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.showMessage('El usuario se guardó correctamente', true, false);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    resetPass() {
        const reset = new ResetPass();
        reset.id = this.selectedEntity.id;
        this.usuarioService.resetPassword(reset).then(
            response => {
                if (response) {
                    this.loading = false;
                    this.showMessage('La contraseña se reseteó.', true, false);
                } else {
                    this.error = true;
                    this.loading = false;
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.usuarioService.delete(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El usuario se eliminó correctamente!!', true, false);
                } else {
                    alert('No se han podido eliminar el usuario!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }
    public volver() {
        this.router.navigate(['/usuarios']);
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                            return;
                        }
                        if (goBack) {
                            this.router.navigate(['/usuarios']);
                        } else {
                            this.router.navigate(['/usuario', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
