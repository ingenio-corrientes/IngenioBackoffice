import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { TiposAdicionalesRoutingModule } from './tiposadicionales-routing.module';
import { TiposAdicionalesComponent } from './tiposadicionales.component';

@NgModule({
    imports: [
        FormsModule,
        Ng2SmartTableModule,
        CommonModule,
        TiposAdicionalesRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule
    ],
    declarations: [TiposAdicionalesComponent]
})
export class TiposAdicionalesModule { }
