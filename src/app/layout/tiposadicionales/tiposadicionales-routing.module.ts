import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TiposAdicionalesComponent } from './tiposadicionales.component';

const routes: Routes = [
    { path: '', component: TiposAdicionalesComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TiposAdicionalesRoutingModule { }
