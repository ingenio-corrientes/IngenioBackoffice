import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LPrecioPapeleriaIntervalosComponent } from './lpreciopapeleriaintervalos.component';
// import {
//   DataComponent,
//   ItemsComponent
// } from './components/';

import { PageHeaderModule } from './../../shared';

describe('LPrecioPapeleriaIntervalosComponent', () => {
  let component: LPrecioPapeleriaIntervalosComponent;
  let fixture: ComponentFixture<LPrecioPapeleriaIntervalosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PageHeaderModule
    ],
      declarations: [
        LPrecioPapeleriaIntervalosComponent,
        // DataComponent,
        // ItemsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LPrecioPapeleriaIntervalosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
