import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// relative import
import { LPrecioPapeleriaIntervalosRoutingModule } from './lpreciopapeleriaintervalos-routing.module';
import { LPrecioPapeleriaIntervalosComponent } from './lpreciopapeleriaintervalos.component';
import {
  ItemComponent
} from './components/';

import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [
        CommonModule,
        LPrecioPapeleriaIntervalosRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [
      LPrecioPapeleriaIntervalosComponent,
      ItemComponent
    ]
})
export class LPrecioPapeleriaIntervalosModule { }
