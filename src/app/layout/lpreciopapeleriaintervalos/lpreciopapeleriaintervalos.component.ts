import { Component, OnInit, Input, SimpleChange, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { CurrencyPipe, PercentPipe } from '@angular/common';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { LPrecioPapeleriaIntervalo, LPrecioPapeleriaIntervaloService } from '../../shared/';

@Component({
    selector: 'app-data',
    templateUrl: './lpreciopapeleriaintervalos.component.html',
    styleUrls: ['./lpreciopapeleriaintervalos.component.scss'],
    animations: [routerTransition()],
    providers: [
        LPrecioPapeleriaIntervaloService,
        CurrencyPipe, PercentPipe]
})

export class LPrecioPapeleriaIntervalosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            descripcion: { title: 'Descripción', filter: false, },
            maximo: { title: 'Máximo', filter: false, },
            porcentaje: {
                title: 'Porcentaje', filter: false,
                valuePrepareFunction: (value) => {
                    return this.pp.transform(value / 100);
                }
            },
            precio: {
                title: 'Precio', filter: false,
                valuePrepareFunction: (value) => {
                    return this.cp.transform(value, 'USD', true, '1.2-2');
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered table-hover',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'LPrecioPapeleriaIntervalos';

    public data: Array<any> = Array<any>();
    public selectedItem: LPrecioPapeleriaIntervalo = null;
    public selectedIDListaPapel: number;
    public selectedIDLista: number;

    isLoading = false;

    public constructor(
        public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private cp: CurrencyPipe,
        private pp: PercentPipe,
        private itemService: LPrecioPapeleriaIntervaloService) {
        this.isLoading = true;
        this.source = new LocalDataSource(this.data);
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDLista = params['id'];
            this.selectedIDListaPapel = params['idpapel'];
            this.getData();
        });
    }

    handleItemUpdated(user) {
        this.getData();
    }

    public getData() {
        if (+this.selectedIDListaPapel !== -1) {
            this.itemService.getListByIDLista(this.selectedIDListaPapel).then(
                response => {
                    if (response) {
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                        this.isLoading = false;
                    } else {
                        this.showMessage('No se pudo obtener los datos de la lista!!', false);
                        this.isLoading = false;
                    }
                }
            ).catch(e => this.handleError(e));
        }
    }

    public volver() {
        this.router.navigate(['/lpreciopapeleria', this.selectedIDLista]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'descripcion', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedItem = event.data;
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
                closeButtonClass: 'close theme-icon-close'
            },
            data: {
                text: message
            },
            actionButtons: [
                {
                    text: 'Aceptar',
                    buttonClass: 'btn btn-success',
                    onAction: () => new Promise((resolve: any) => {
                        setTimeout(() => {
                            if (goBack) {
                                this.router.navigate(['/lpreciopapeleria']);
                            } else {
                                this.router.navigate(['/lpreciopapeleria', this.selectedIDLista]);
                            }
                            resolve();
                        }, 20);
                    })
                }]
        })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
