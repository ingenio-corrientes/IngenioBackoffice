import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LPrecioPapeleriaIntervalosComponent } from './lpreciopapeleriaintervalos.component';

const routes: Routes = [
    { path: '', component: LPrecioPapeleriaIntervalosComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LPrecioPapeleriaIntervalosRoutingModule { }
