import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PageHeaderModule } from './../../shared';

import { RecibosRoutingModule } from './recibos-routing.module';
import { RecibosComponent } from './recibos.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RecibosRoutingModule,
        PageHeaderModule,
        ReactiveFormsModule,
        Ng2SmartTableModule
    ],
    declarations: [RecibosComponent]
})
export class RecibosModule { }
