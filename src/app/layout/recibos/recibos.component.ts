import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR, TWUtils } from '../../shared';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Recibo, ReciboService } from '../../shared';
import { TranslateDefaultParser } from '@ngx-translate/core';

@Component({
    selector: 'app-tables',
    templateUrl: './recibos.component.html',
    styleUrls: ['./recibos.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        CurrencyAR,
        ReciboService,
    ]
})

export class RecibosComponent implements OnInit {
    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            id: { title: 'Número', filter: false, },
            razonsocial: { title: 'Cliente', filter: false, },
            idpresupuesto: { title: 'Presupuesto', filter: false, },
            fecha: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            monto: {
                title: 'Monto',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return `<div class="cell_right"> ` + this.cp.transform(value) + ` </div>`;
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        },
    };
    source: LocalDataSource;
    public modalTitle = 'Recibos';

    public dataEstados: Array<any> = Array<any>();
    public selectedEntity: Recibo;
    public data: Array<any> = Array<any>();
    public recibos: number;
    public monto: string;

    fecinicio: number;
    fecfin: number;

    isLoading = false;
    showCancelados = true;

    public constructor(public router: Router,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private route: ActivatedRoute,
        private datePipe: DatePipe,
        private cp: CurrencyAR,
        private reciboService: ReciboService) {
            this.isLoading = true;
            this.source = new LocalDataSource(this.data);
            this.recibos = 0;
            this.monto = '';
            this.showCancelados = true;
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.fecinicio = +params['fecinicio'];
            this.fecfin = +params['fecfin'];
            if (this.fecinicio && this.fecfin) {
                this.getFilteredList();
                this.isLoading = false;
            } else {
                this.getList();
                this.isLoading = false;
            }
        });
    }

    protected getFilteredList() {
        this.reciboService.getFilteredByDate(this.fecinicio, this.fecfin).then(
            response => {
                if (response) {
                    this.data = response;
                    this.source = new LocalDataSource(this.data);
                    this.source.setSort([{ field: 'fecha', direction: 'asc' }]);
                } else {
                    this.showMessage('No se han podido cargar datos de recibos!!', false);
                }
                this.calcularResumen();
            }
        ).catch(e => this.handleError(e));
    }

    public getList() {
        this.reciboService.getList().then(
            response => {
                if (response) {
                    this.data = response;
                    this.onCheck();
                    /// this.source = new LocalDataSource(this.data);
                    this.source.setSort([{ field: 'fecha', direction: 'asc' }]);
                } else {
                    this.showMessage('No se han podido cargar datos de recibos!!', false);
                }
                this.calcularResumen();
            }
        ).catch(e => this.handleError(e));
    }

    nuevo() {
        this.router.navigate(['/recibo', -1]);
    }

    onCheck() {
        this.showCancelados = !this.showCancelados;
        if (this.showCancelados) {
            this.source = new LocalDataSource(this.data);
        } else {
            const dataFilt = this.data.filter(item => {
                const emitido = TWUtils.isNullDate(item.fecemision);
                const conSaldo = +item.saldopresupuesto > 0;
                return (emitido || conSaldo);
                });
            this.source = new LocalDataSource(dataFilt);
        }
    }

    private calcularResumen() {
        this.source.getFilteredAndSorted().then(data => {
            let mont = 0;
            let cant = 0;
            data.forEach(element => {
                mont += +element.monto;
                cant++;
            });
            this.recibos = cant;
            this.monto = this.cp.transform(mont);
        });
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            this.calcularResumen();
            return;
        }
        this.source.setFilter([
            { field: 'id', search: query, },
            { field: 'razonsocial', search: query, },
            { field: 'idpresupuesto', search: query, },
            { field: 'fecha', search: query, },
            { field: 'monto', search: query, }
        ], false);
        this.calcularResumen();
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
        this.router.navigate(['/recibo', event.data.id]);
    }

    showMessage(message: string, goBack: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (goBack) {
                            this.router.navigate(['/recibos']);
                        } else {
                            this.router.navigate(['/recibos']);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('Error: ' + error, false);
        this.isLoading = false;
        return Promise.reject(error.message || error);
    }
}
