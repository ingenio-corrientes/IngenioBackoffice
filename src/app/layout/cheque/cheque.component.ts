import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { CurrencyAR } from '../../shared/pipes/currency-ar';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import {
    ModuloXRolService,
    Cheque,
    ChequeService,
    TWUtils } from '../../shared';

@Component({
    selector: 'app-form',
    templateUrl: './cheque.component.html',
    styleUrls: ['./cheque.component.scss'],
    animations: [routerTransition()],
    providers: [
        DatePipe,
        CurrencyAR,
        ChequeService,
        ModuloXRolService]
})

export class ChequeComponent implements OnInit {
    readonly IDMODULO = 4;
    public modalTitle = 'Cheque';

    public selectedID: number;
    public selectedEntity: Cheque;
    public depositado: boolean;
    public cobrado: boolean;
    fecEmision: string;
    fecVencimiento: string;
    monto: string;
    loggedId: number;

    isLoading = false;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private dp: DatePipe,
        private cp: CurrencyAR,
        private modal: ModalDialogService,
        private viewContainer: ViewContainerRef,
        private chequeService: ChequeService,
        private moduloXRolService: ModuloXRolService) {
        this.isLoading = true;
        this.selectedEntity = new Cheque();
        this.depositado = false;
        this.cobrado = false;
        const today: Date = new Date();
        today.setMonth(today.getMonth() - 1);
        this.fecEmision = TWUtils.stringDateToJson(this.dp.transform(today, 'yyyy-MM-dd'));
        this.fecVencimiento = TWUtils.stringDateToJson(this.dp.transform(new Date(), 'yyyy-MM-dd'));
        this.monto = '';
    }

    public ngOnInit(): void {
        const jsonData = JSON.parse(localStorage.getItem('userLog'));
        this.loggedId = +jsonData.id;
        this.evalPermisos();
        this.route.params.subscribe(params => {
            this.selectedID = +params['id'];
            this.getEntityData();
        });
    }

    evalPermisos() {
        this.moduloXRolService.getListByIDUsuarioIDModulo(this.loggedId, this.IDMODULO)
            .then(response => {
                if (response) {
                    if (+response.ver !== 1) {
                        this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    }
                    this.isLoading = false;
                } else {
                    this.showMessage('No está autorizado para acceder a este módulo!', false, true);
                    this.isLoading = false;
                }
            })
        .catch(e => this.handleError(e));
    }

    public getEntityData(): any {
        if (this.selectedID > -1) {
            this.chequeService.getByID(this.selectedID)
                .then(
                    response => {
                        this.selectedEntity = response as Cheque;
                        this.depositado = this.selectedEntity.depositado === 1;
                        this.cobrado = this.selectedEntity.cobrado === 1;
                        this.fecEmision = this.dp.transform(this.selectedEntity.fecemision, 'dd-MM-yyyy');
                        this.fecVencimiento = this.dp.transform(this.selectedEntity.fecvencimiento, 'dd-MM-yyyy');
                        this.monto = this.cp.transform(this.selectedEntity.monto);
                        this.isLoading = false;
                    })
                .catch(e => this.handleError(e));
        } else {
            this.isLoading = false;
        }
    }

    public onSave() {
        this.selectedEntity.depositado = this.depositado ? 1 : 0;
        this.selectedEntity.cobrado = this.cobrado ? 1 : 0;
        this.chequeService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = Number(response);
                    this.showMessage('El cheque se guardó correctamente', true, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public eliminar() {
        this.chequeService.delete(this.selectedEntity).then(
            response => {
                if (+response === this.chequeService.ENTITY_DELETED) {
                    this.showMessage('El cheque se ha eliminado correctamente', true, false);
                } else  if (+response === this.chequeService.ERROR_FOREING_KEY) {
                    this.showMessage('El cheque está siendo utilizado y no puede eliminarse.', false, false);
                } else {
                    this.showMessage('El cheque no se ha podido eliminar!', false, false);
                }
            }
        ).catch(e => this.handleError(e));
    }

    public isNew() {
        return (this.selectedID === -1);
    }

    public isOrigenRecibo() {
        return +this.selectedEntity.origen === this.chequeService.ORIGEN_RECIBO;
    }

    public isOrigenGasto() {
        return +this.selectedEntity.origen === this.chequeService.ORIGEN_GASTO;
    }

    public volver() {
        this.router.navigate(['/cheques']);
    }

    showMessage(message: string, goBack: boolean, notAuth: boolean) {
        this.modal.openDialog(this.viewContainer, {
            title: this.modalTitle,
            childComponent: SimpleModalComponent,
            settings: {
              closeButtonClass: 'close theme-icon-close'
            },
            data: {
              text: message
            },
            actionButtons: [
                {
                  text: 'Aceptar',
                  buttonClass: 'btn btn-success',
                  onAction: () => new Promise((resolve: any) => {
                    setTimeout(() => {
                        if (notAuth) {
                            this.router.navigate(['/dashboard']);
                            return;
                        }
                        if (goBack) {
                            this.router.navigate(['/cheques']);
                        } else {
                            this.router.navigate(['/cheque', this.selectedID]);
                        }
                        resolve();
                    }, 20);
                  })
                }]
          })
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
