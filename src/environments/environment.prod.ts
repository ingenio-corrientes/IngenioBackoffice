export const environment = {
  production: true,
  apiUrl: 'https://ingeniocorrientes.com.ar/admin/api/',
  imagestock: 'https://ingeniocorrientes.com.ar/admin/' + 'imgs/',
  DEFAULT_PASS: 'IngenioPass',
  version: '0.1.0'
};
